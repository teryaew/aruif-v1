'use strict';

const reactDocs = require('react-docgen');
const fs = require('fs');
const path = require('path');
const doctrine = require('doctrine');
const camelCase = require('uppercamelcase');
const decamelize = require('decamelize');

/**
 * Преобразует описание методов в формат, подходящий для документации
 * @param {Array} methods Массив методов, полученный из react-docgen
 * @returns {Array}
 */
function prepareMethods(methods) {
    methods = methods || [];
    return methods
        .filter(m => !!m.docblock)
        .map(m => Object.assign({}, m, {
            docblock: doctrine.parse(m.docblock)
        }))
        .filter(m => m.docblock.tags && m.docblock.tags.some(tag => tag.title === 'public'));
}

const TYPE_MAP = {
    array: 'Array',
    bool: 'Boolean',
    func: 'Function',
    number: 'Number',
    object: 'Object',
    string: 'String',
    node: 'Node',
    element: 'Element'
};

/**
 * @typedef {Object} PropDescription
 * @property {String} name Имя свойства
 * @property {Object} type Тип свойства
 * @property {String} default Значение по умолчанию
 * @property {Boolean} required Является ли свойство обязательным
 * @property {String} description Описание свойства из jsdoc-а
 */

/**
 * @typedef {Object} EnumDescription
 * @property {String} name Имя Enum-а
 * @property {Array.<String>} values Значения Enum-а
 */

/**
 * @typedef {Object} CustomTypeDescription
 * @property {String} name Имя типа
 * @property {Array.<PropDescription>} props Свойтсва типа
 */

/**
 * Создает описание типа из формата react-docgen
 * @param {Object} type Объект с описанием типа из react-docgen
 * @param {String} propName Имя свойства
 * @param {Array.<CustomTypeDescription>} customTypes Массив типов
 * @param {Array.<EnumDescription>} enums Массив enum-ов
 * @returns {String|{typeName: String}}
 */
function getType(type, propName, customTypes, enums) {
    if (!type) {
        return 'Unknown';
    }
    switch (type.name) {
        case 'array':
        case 'bool':
        case 'func':
        case 'number':
        case 'object':
        case 'string':
        case 'node':
        case 'element':
            return TYPE_MAP[type.name];
        case 'instanceOf':
            return type.value;
        case 'oneOfType':
        case 'union':
            return {
                typeName: 'union',
                types: type.value.map(getType)
            };
        case 'arrayOf':
            return {
                typeName: 'array',
                innerType: getType(type.value, propName, customTypes, enums)
            };
        case 'shape':
            propName = camelCase(propName) + 'Type';
            Object.keys(type.value).forEach(key => {
                type.value[key].type = {
                    name: type.value[key].name,
                    value: type.value[key].value
                };
            });
            customTypes.push({
                name: propName,
                props: prepareProps(type.value, customTypes, enums).props
            });

            return { typeName: 'shape', name: propName };
        case 'enum':
            propName = camelCase(propName) + 'Enum';
            enums.push({
                name: propName,
                values: type.value.map(v => v.value)
            });
            return { typeName: 'enum', name: propName };
        case 'custom':
            return type.raw;
        default:
            return type.name || type.value;
    }
}

/**
 * Преобразуем объект с информацией о свойствах из формата react-docgen в формат, пригодный для создания документации
 * @param {Object} [props] Объект с информациях о свойствах компонента, полученный из react-docgen
 * @param {Array.<CustomTypeDescription>} [customTypes] Массив типов, используемых в компоненте
 * @param {Array.<EnumDescription>} [enums] Массив enum-ов, используемых в компоненте
 * @returns {{customTypes: Array.<CustomTypeDescription>, enums: Array.<EnumDescription>, props: Array.<PropDescription>}}
 */
function prepareProps(props, customTypes, enums) {
    props = props || {};
    customTypes = customTypes || [];
    enums = enums || [];
    const result = Object.keys(props)
        .map(name => {
            const prop = props[name];

            return {
                name: name,
                type: getType(prop.type, name, customTypes, enums),
                default: prop.defaultValue ? prop.defaultValue.value : undefined,
                required: prop.required,
                description: prop.description || ''
            };
        });

    return {
        customTypes: customTypes,
        enums: enums,
        props: result
    };
}

function getDocForFile(filePath, componentName) {
    const src = fs.readFileSync(filePath, 'utf8');

    const componentInfo = reactDocs.parse(src);
    const description = doctrine.parse(componentInfo.description);

    if (description.tags && description.tags.length > 0) {
        var tag = description.tags.find(tag => tag.title === 'extends');
        if (tag) {
            componentInfo.extends = {
                name: tag.name,
                source: decamelize(tag.name, '-')
            };
        }
    }

    componentInfo.name = camelCase(componentName);
    componentInfo.description = description.description;
    componentInfo.examples = description.tags.filter(tag => tag.title === 'example').map(tag => tag.description);
    componentInfo.source = componentName;
    componentInfo.methods = prepareMethods(componentInfo.methods);
    const preparedProps = prepareProps(componentInfo.props);
    componentInfo.props = preparedProps.props;
    componentInfo.enums = preparedProps.enums;
    componentInfo.types = preparedProps.customTypes.reverse();

    return componentInfo;
}

function getComponents(src, ignore) {
    return fs.readdirSync(src)
        .filter(item => {
            if (ignore.indexOf(item) !== -1) {
                return false;
            }

            return (
                fs.statSync(path.resolve(src, item)).isDirectory() &&
                fs.existsSync(path.resolve(src, item, item + '.jsx'))
            );
        });
}

module.exports = {
    getDocForFile: getDocForFile,
    getComponents: getComponents
};
