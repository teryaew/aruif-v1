const manageTranslations = require('react-intl-translations-manager').default;

manageTranslations({
    translationsDirectory: 'locales/',
    messagesDirectory: 'locales/messages/',
    languages: ['en', 'ru']
});
