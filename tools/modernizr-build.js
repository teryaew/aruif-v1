const fs = require('fs');
const modernizr = require('modernizr');
const path = require('path');
const config = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '.modernizrrc')));

function wrapOutput(output) {
    return `
;(function(window) {
if (window) {
  ${output}
  module.exports = window.Modernizr;
} else {
  module.exports = {};
}
})(typeof window !== 'undefined' ? window : false);`;
}

modernizr.build(config, function (output) {
    const filePath = path.join(__dirname, 'modernizr.js');
    const features = output.split('\n')[1];

    fs.readFile(filePath, 'utf-8', function (error, data) {
        if (error && error.code !== 'ENOENT') {
            throw error;
        }

        if (!data || features !== data.split('\n')[4]) {
            fs.writeFile(filePath, wrapOutput(output), function () {
                console.log(`Modernizr build saved at \`${filePath}\``);
            });
        }
    });
});
