/* eslint-disable no-new-func */
/* jshint evil: true */

/**
 * Возвращает имена аргументов функции. Спасибо, Девид Волш.
 * https://davidwalsh.name/javascript-arguments
 */
function getArgs(func) {
    // First match everything inside the function argument parens.
    var args = func.toString().match(/function\s.*?\(([^)]*)\)/)[1];

    // Split the arguments string into an array comma delimited.
    return args.split(',').map(function(arg) {
        // Ensure no inline comments are parsed and trim the whitespace.
        return arg.replace(/\/\*.*\*\//, '').trim();
    }).filter(function(arg) {
        // Ensure no undefined values are added.
        return arg;
    });
}

function initializeBenchmakSuite() {
    if (window.__bs) {
        return;
    }

    window.__bs = {};

    window.__bs.setup = function (suiteName) {
        Object.keys(window.__bs[suiteName]).forEach(depName => {
            window[depName] = window.__bs[suiteName][depName];
        });
    };

    window.__bs.teardown = function (suiteName) {
        Object.keys(window.__bs[suiteName]).forEach(depName => delete window[depName]);
    };
}

/**
 * Возвращает настройку для набора тестов `benchmark.js`.
 * Настраивает набор так, что перед запуском набора тестов в глобальную
 * область видимости экспортируются переменные из замыкания, переданной
 * функции. После выполнения тестового набора очищает глобальный контекст.
 *
 * @param {String} suiteName Название тестового набора
 * @param {Function} scope Функция-замыкания тестового набора
 * @param {Array} Список зависимостей
 */
export default function benchmarkSuite(suiteName, scope, deps) {
    deps = Array.from(deps);

    initializeBenchmakSuite();

    window.__bs[suiteName] = getArgs(scope).reduce((result, depName, depIndex) => {
        result[depName] = deps[depIndex];
        return result;
    }, {});

    return {
        setup: new Function(`
            window.__bs.setup('__SUITE_NAME__');

            this.element = document.createElement('div');
            this.element.setAttribute('id', 'react-app');
            document.body.appendChild(this.element);
        `.replace('__SUITE_NAME__', suiteName)),

        teardown: new Function(`
            ReactDOM.unmountComponentAtNode(this.element);
            this.element.parentNode.removeChild(this.element);
            delete this.element;

            window.__bs.teardown('__SUITE_NAME__');
        `.replace('__SUITE_NAME__', suiteName))
    };
}
