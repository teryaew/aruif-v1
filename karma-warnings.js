/* eslint no-console: 0 */
/**
 * Throws error on react warnings.
 * Used only in tests.
 */

console.error = function (message) {
    throw new Error(message);
};
