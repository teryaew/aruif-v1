import { render, cleanUp } from '../test-utils';

import AppTitle from './app-title';

describe('app-title', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let appTitle = render(<AppTitle>AppTitle-test</AppTitle>);

        expect(appTitle.node).to.exist;
        expect(appTitle.node).to.have.text('AppTitle-test');
    });
});
