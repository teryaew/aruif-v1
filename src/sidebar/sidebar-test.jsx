import { render, cleanUp } from '../test-utils';

import Sidebar from './sidebar';

describe('sidebar component', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let sidebar = render(<Sidebar visible={ true }>defaultText</Sidebar>);

        expect(sidebar.node).to.exist;
        expect(sidebar.node.querySelector('.sidebar__content')).to.have.text('defaultText');
    });

    it('should render cross icon by default', () => {
        let sidebar = render(<Sidebar visible={ true }>defaultText</Sidebar>);
        let closeIcon = sidebar.node.querySelector('.sidebar__closer');

        expect(closeIcon).to.exist;
    });

    it('shouldn`t render cross icon with special param', () => {
        let sidebar = render(<Sidebar visible={ true } hasCloser={ false }>defaultText</Sidebar>);
        let closeIcon = sidebar.node.querySelector('.sidebar__closer');

        expect(closeIcon).to.not.exist;
    });


    it('should call `onCloserClick` callback after cross icon was clicked', () => {
        let onClick = chai.spy();
        let sidebar = render(
            <Sidebar
                visible={ true }
                hasCloser={ true }
                onCloserClick={ onClick }
            >
                defaultText
            </Sidebar>
        );
        let closeIcon = sidebar.node.querySelector('.sidebar__closer');

        closeIcon.click();

        expect(onClick).to.have.been.called.once;
    });
});
