import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import Icon from '../icon/icon';
import PopupContainerProvider from '../popup-container-provider/popup-container-provider';

import { deprecated } from '../lib/prop-types';
import performance from '../performance';

import cn from '../cn';
require('./sidebar.css');

/**
 * Компонент боковой панели aka холодильник.
 */
@cn('sidebar')
@performance()
export default class Sidebar extends React.Component {
    static propTypes = {
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Дочерние компоненты */
        children: Type.node,
        /** Используйте `hasCloser`. Признак для отрисовки элемента закрытия */
        showCloser: deprecated(Type.bool, 'Use `hasCloser` instead'),
        /** Признак для отрисовки элемента закрытия */
        hasCloser: Type.bool,
        /** Признак появления холодильника */
        visible: Type.bool.isRequired,
        /** Обработчик клика на элемент закрытия */
        onCloserClick: Type.func
    };

    static defaultProps = {
        hasCloser: true
    };

    componentDidMount() {
        this.setBodyClass(this.props.visible);
    }

    componentWillReceiveProps(nextProps) {
        this.setBodyClass(nextProps.visible);
    }

    componentWillUnmount() {
        this.setBodyClass(false);
    }

    /**
     * Изменяет класс для body. Нужен для управления скроллом
     * основного экрана при показе холодильника.
     *
     * @param {Boolean} visible Признак видимости сайдбара.
     */
    setBodyClass(visible) {
        document.body.classList[visible ? 'add' : 'remove']('sidebar-visible');
    }

    @autobind
    handleCloserClick() {
        const { onCloserClick } = this.props;

        if (onCloserClick) {
            onCloserClick();
        }
    }

    render(cn) {
        const { hasCloser, showCloser, children, visible } = this.props;

        return (
            <PopupContainerProvider className={ cn({ visible }) }>
                <div>
                    {
                        (hasCloser || showCloser) &&
                        <div className={ cn('closer') } onClick={ this.handleCloserClick }>
                            <Icon
                                tool='close'
                                size='xl'
                            />
                        </div>
                    }
                    <div className={ cn('content') }>
                        { children }
                    </div>
                </div>
            </PopupContainerProvider>
        );
    }
}
