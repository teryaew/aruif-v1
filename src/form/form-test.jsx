import { render, cleanUp, simulate } from '../test-utils';

import Form from './form';

describe('form', function () {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let form = render(<Form>Form-example</Form>);

        expect(form.node).to.exist;
        expect(form.node).to.have.text('Form-example');
        expect(form.node).to.have.class('form');
    });

    it('should render footer at the end of a form', () => {
        let form = render(<Form footer={ <div>footer</div> }>Form-content</Form>);
        let contentNode = form.node.querySelector('.form__footer');

        expect(contentNode).to.have.text('footer');
    });

    it('should call `onSubmit` callback after form was submitted', () => {
        let onSubmit = chai.spy();
        let form = render(<Form onSubmit={ onSubmit } />);

        simulate(form.node, 'submit');

        expect(onSubmit).to.have.been.called.once;
    });
});
