import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./form.css');

/**
 * Компонент формы
 */
@cn('form')
@performance()
class Form extends FeatherComponent {
    static propTypes = {
        /** Выравнивание поля в форме // TODO @teryaew: think about it */
        align: Type.oneOf(['left', 'center', 'right', 'justify']),
        /** Способ кодирования данных формы при их отправке */
        enctype: Type.oneOf(['application/x-www-form-urlencoded', 'multipart/form-data', 'text/plain']),
        /** Адрес отправки данных на сервер */
        action: Type.string,
        /** Метод запроса */
        method: Type.oneOf(['post', 'get']),
        /** Тип формы */
        view: Type.oneOf(['line', 'normal']),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Заголовок для формы */
        header: Type.node,
        /** Футер для формы */
        footer: Type.node,
        /** Управление встроенным в браузер механизмом валидации формы */
        noValidate: Type.bool,
        /** Управление автозаполнением формы */
        autocomplete: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Идентификатор компонента в DOM */
        id: Type.string,
        /** Имя компонента в DOM */
        name: Type.string,
        /** Обработчик отправки формы */
        onSubmit: Type.func
    };

    static defaultProps = {
        action: '/',
        enctype: 'application/x-www-form-urlencoded',
        method: 'post',
        size: 'm',
        noValidate: false,
        autocomplete: true
    };

    render(cn) {
        return (
            <form
                className={ cn({
                    align: this.props.align,
                    size: this.props.size,
                    view: this.props.view
                }) }
                action={ this.props.action }
                autoComplete={ this.props.autocomplete === false ? 'off' : 'on' }
                encType={ this.props.enctype }
                id={ this.props.id }
                method={ this.props.method }
                name={ this.props.name }
                noValidate={ this.props.noValidate }
                onSubmit={ this.handleSubmit }
            >
                {
                    this.props.header &&
                    <div className={ cn('header') }>
                        { this.props.header }
                    </div>
                }
                <div className={ cn('content') }>
                    { this.props.children }
                </div>
                {
                    this.props.footer &&
                    <div className={ cn('footer') }>
                        { this.props.footer }
                    </div>
                }
            </form>
        );
    }

    @autobind
    handleSubmit(e) {
        e.preventDefault();

        if (this.props.onSubmit) {
            this.props.onSubmit();
        }
    }
}

export default Form;
