import FeatherComponent from '../feather/feather';
import Input from '../input/input';

import performance from '../performance';

import cn from '../cn';

/**
 * Компонент поля ввода почты
 * @extends Input
 */
@cn('email-input', Input)
@performance()
class EmailInput extends FeatherComponent {
    root;

    render(cn, Input) {
        return (
            <Input
                { ...this.props }
                type='email'
                ref={ (root) => { this.root = root; } }
                noValidate={ true }
                className={ cn }
            />
        );
    }

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */
    focus() {
        this.root.focus();
    }

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */
    blur() {
        this.root.blur();
    }

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */
    scrollTo() {
        this.root.scrollTo();
    }

}

export default EmailInput;
