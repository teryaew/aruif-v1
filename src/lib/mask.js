import InputMask from 'inputmask-core';

// Стандартный плейсхолдер буквы равный по ширине цифровому символу.
const PLACEHOLDER_CHAR = '\u2007';

/**
 * Класс маски. Позволяет форматировать строку по заданной маске.
 *
 * @class
 * @param {String} mask Маска в формате: https://github.com/insin/inputmask-core
 */
class Mask {
    /**
     * Длина маски.
     *
     * @public
     * @type {Number}
     */
    length;

    /**
     * Индекс первого редактируемого символа.
     *
     * @public
     * @type {Number}
     */
    firstEditableIndex;

    /**
     * Индекс последнего редактируемого символа.
     *
     * @public
     * @type {Number}
     */
    lastEditableIndex;

    /**
     * @type {InputMask.Pattern}
     */
    pattern;

    constructor(mask) {
        this.pattern = new InputMask.Pattern(mask, null, PLACEHOLDER_CHAR);

        this.length = this.pattern.length;
        this.firstEditableIndex = this.pattern.firstEditableIndex;
        this.lastEditableIndex = this.pattern.lastEditableIndex;
    }

    /**
     * Проверяет является ли символ в заданном индексе редактируемым.
     *
     * @param {Number} index Индекс символа.
     * @returns {Boolean}
     */
    isEditableIndex(index) {
        return this.pattern.isEditableIndex(index);
    }

    /**
     * Форматирует значение введенное в поле ввода по маске.
     *
     * @param {String} value Неформатированное значение из поля ввода.
     * @returns {String}
     */
    format(value) {
        let formattedValue = '';

        let cleanValue = value.replace(/\s+/g, '');
        let cleanValueLength = cleanValue.length;
        let cleanValueIndex = 0;
        let cleanValueChar;

        let patternIndex = 0;
        let patternLength = this.pattern.length;

        while (patternIndex < patternLength && cleanValueIndex < cleanValueLength) {
            if (!this.pattern.isEditableIndex(patternIndex)) {
                formattedValue += this.pattern.pattern[patternIndex];
                patternIndex++;
            } else {
                while ((cleanValueChar = cleanValue.charAt(cleanValueIndex)) !== '') { // eslint-disable-line no-cond-assign
                    if (this.pattern.isValidAtIndex(cleanValueChar, patternIndex)) {
                        formattedValue += this.pattern.transform(cleanValueChar, patternIndex);
                        patternIndex++;
                        cleanValueIndex++;
                        break;
                    } else {
                        cleanValueIndex++;
                    }
                }
            }
        }

        return formattedValue;
    }
}

export default Mask;
