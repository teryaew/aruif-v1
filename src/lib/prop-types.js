import moment from 'moment';

function createChainableTypeChecker(validate) {
    function checkType(isRequired, props, propName, componentName, location) {
        componentName = componentName || '';
        if (props[propName] === null || props[propName] === undefined) {
            if (isRequired) {
                return new Error(
                    `Required prop \`${propName}\` was not specified in \`${componentName}\`.`
                );
            }
            return null;
        }

        return validate(props, propName, componentName, location);
    }

    let chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
}

function propTypeIsMoment(props, propName, componentName) {
    if (!moment.isMoment(props[propName])) {
        return new Error(
            `Invalid prop \`${propName}\` supplied to \`${componentName}\`. 
            Expected valid moment.js object, ${typeof props[propName]} given.`
        );
    }
    return null;
}

function propTypeIsHtmlElement(props, propName, componentName) {
    if (!(props[propName]) instanceof (typeof HTMLElement === 'undefined' ? {} : HTMLElement)) {
        return new Error(
            `Invalid prop \`${propName}\` supplied to \`${componentName}\`.
            Expected valid HTMLElement object, ${typeof props[propName]} given.`
        );
    }
    return null;
}

export function deprecated(propType, message) {
    let warned = false;
    return function (...args) {
        const [props, propName, componentName] = args;
        const prop = props[propName];
        if (prop !== undefined && prop !== null && !warned) {
            warned = true;
            if (process.env.NODE_ENV !== 'production') {
                /* eslint no-console: 0 */
                console.warn(`Property '${propName}' of '${componentName}' is deprecated. ${message}`);
            }
        }
        return propType.call(this, ...args);
    };
}

export function deprecatedType(oldType, newType, message) {
    let warned = false;

    return function (...args) {
        const [, propName, componentName] = args;
        const oldResult = oldType.call(this, ...args);
        const newResult = newType.call(this, ...args);
        if (process.env.NODE_ENV !== 'production' && !oldResult && !warned && newResult) {
            warned = true;
            /* eslint no-console: 0 */
            console.warn(`Given type of '${propName}' of '${componentName}' is deprecated. ${message}`);
        }
        return newResult;
    };
}

export const Moment = createChainableTypeChecker(propTypeIsMoment);
export const HtmlElement = createChainableTypeChecker(propTypeIsHtmlElement);
