import moment from 'moment';

const DAYS_IN_WEEK = 7;

/**
 * Нормализирует дату, возвращает Date в независтимости от входных данных.
 *
 * @param {Moment|Date|Number} date Дата для нормализации.
 * @returns {Date}
 */
export function normalizeDate(date) {
    return moment.isMoment(date)
        ? date.toDate()
        : new Date(date);
}

/**
 * Возвращает "правильный" индекс дня недели, 0 - пн, 1 - вт и так далее.
 *
 * @param {Date} date Дата, из которой нужно получить день недели.
 * @returns {Number}
 */
export function getRussianWeekDay(date) {
    let sunday = 0;
    let foreignWeekDayIndex = date.getDay();

    return foreignWeekDayIndex === sunday
        ? DAYS_IN_WEEK - 1
        : foreignWeekDayIndex - 1;
}

const PARSE_TOKENS = [
    { type: 'date', regex: /^\d{2}/, formatRegex: /^DD/ },
    { type: 'date', regex: /^\d{1,2}/, formatRegex: /^D/ },
    { type: 'month', regex: /^\d{2}/, formatRegex: /^MM/ },
    { type: 'month', regex: /^\d{1,2}/, formatRegex: /^M/ },
    { type: 'year', regex: /^\d{4}/, formatRegex: /^YYYY/ }
];

const TOKEN_LIMITS = {
    date: { min: 1, max: 31 },
    month: { min: 1, max: 12 },
    year: { min: 1, max: Number.MAX_SAFE_INTEGER }
};

/**
 * @typedef {Object} FormatParserToken
 * @property {String} type Тип токена.
 * @property {RegExp} formatRegex Для всех типов кроме 'delimiter'. Регулярное выражение, соответствующее формату
 * @property {RegExp} regex Для всех типов кроме 'delimiter'. Регулярное выражение для проверки соответствия формату
 * @property {String} value Только для типа 'delimiter'. Символ разделитель.
 */

const PARSER_CACHE = {};

/**
 * Разбирает строку с форматом даты.
 *
 * @param {String} format формат даты для разбора.
 * @returns {Array<FormatParserToken>}
 */
function parseFormat(format) {
    if (PARSER_CACHE[format]) {
        return PARSER_CACHE[format];
    }

    let parser = [];
    let processingFormat = format;

    while (processingFormat.length > 0) {
        /* eslint no-loop-func: 0 */
        let matchedToken = PARSE_TOKENS.find(t => processingFormat.match(t.formatRegex));
        if (matchedToken) {
            parser.push(matchedToken);
            processingFormat = processingFormat.replace(matchedToken.formatRegex, '');
        } else {
            parser.push({ type: 'delimiter', value: processingFormat[0] });
            processingFormat = processingFormat.substring(1);
        }
    }

    PARSER_CACHE[format] = parser;
    return parser;
}

/**
 * Разбирает дату из строки по заданному формату.
 * Допустимые элементы формата:
 * D - дата, в формате 1-31
 * DD - дата, в формате 01-31,
 * M - месяц, в формате 1-12,
 * MM - месяц, в формате 01-12
 * YYYY - год
 * В качестве разделителей между элементами даты могут выступать любые символы, не являющиеся частью формата.
 *
 * @param {String} input Входная строка для разбора.
 * @param {String} [format='DD.MM.YYYY'] Формат, который будет использоваться для разбора.
 * @param {Boolean} [strict=true] Запрещать ли значения, выходящие за пределы логических ограничений месяцев/дней.
 * В случае если strict=false 22 месяц будет интерпретироваться как год и 10 месяцев.
 * @returns {Date}
 */
export function parse(input, format = 'DD.MM.YYYY', strict = true) {
    let parsedFormat = parseFormat(format);
    let parsedResult = {};
    for (let i = 0; i < parsedFormat.length; i++) {
        let token = parsedFormat[i];
        if (token.type === 'delimiter') {
            if (input[0] !== token.value) {
                return new Date('invalid');
            }
            input = input.substring(1);
            continue;
        }

        let match = input.match(token.regex);
        if (!match) {
            return new Date('invalid');
        }
        let matchedValue = parseInt(match[0], 10);
        let limits = TOKEN_LIMITS[token.type];
        if (strict && (matchedValue > limits.max || matchedValue < limits.min)) {
            return new Date('invalid');
        }
        parsedResult[token.type] = matchedValue;
        input = input.replace(token.regex, '');
    }

    return new Date(parsedResult.year, parsedResult.month - 1, parsedResult.date || 1);
}
