/**
 * Check that mouse was outside element.
 *
 * @param {Object} event - MouseEvent
 * @param {React.Element} element - Element to check bounds
 * @returns {Boolean}
 */
export function isEventOusideBounds(event, element) {
    let rect = element.getBoundingClientRect();
    return ((event.pageX < rect.left || event.pageX > rect.right) ||
            (event.pageY < rect.top || event.pageY > rect.bottom));
}

/**
 * Check that mouse was outside element.
 *
 * @param {Object} event - MouseEvent
 * @param {React.Element} element - Element to check bounds
 * @returns {Boolean}
 */
export function isEventOutsideClientBounds(event, element) {
    let rect = element.getBoundingClientRect();
    return event.clientX < rect.left || event.clientX > rect.right
        || event.clientY < rect.top || event.clientY > rect.bottom;
}
