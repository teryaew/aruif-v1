import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import AppContent from '../app-content/app-content';
import AppTitle from '../app-title/app-title';
import FeatherComponent from '../feather/feather';
import Heading from '../heading/heading';
import Link from '../link/link';
import Page from '../page/page';

import cn from '../cn';
require('./error-page.css');

/**
 * Компонент страницы ошибки.
 * Как правило является корневым компонентом страницы.
 * Используется вместо компонента Page.
 *
 * ```javascript
 * import ErrorPage from 'arui-feather/src/error-page/error-page';
 * import Header from 'arui-feather/src/header/header';
 *
 * <ErrorPage
 *      returnUrl='/login'
 *      header={ <Header /> }
 * />
 * ```
 */
@cn('error-page')
class ErrorPage extends FeatherComponent {
    static propTypes = {
        /** Заголовок ошибки */
        title: Type.oneOfType([Type.node, Type.string]),
        /** Сообщение ошибки */
        text: Type.oneOfType([Type.node, Type.string]),
        /** Шапка страницы */
        header: Type.node,
        /** href для ссылки 'Вернуться в интернет-банк' */
        returnUrl: Type.string,
        /** Альтернативный текст для ссылки 'Вернуться в интернет-банк' */
        returnTitle: Type.oneOfType([Type.node, Type.string])
    };

    static defaultProps = {
        title: (
            <FormattedMessage
                id='errorPage.title'
                defaultMessage='An error has occurred'
            />
        ),
        text: (
            <FormattedMessage
                id='errorPage.text'
                defaultMessage='Please try again after some time.'
            />
        ),
        returnTitle: (
            <FormattedMessage
                id='errorPage.returnTitle'
                defaultMessage='Return to Internet-Bank'
            />
        )
    };

    render(cn) {
        return (
            <I18n locale={ this.props.locale }>
                <Page
                    header={ this.props.header }
                    className={ cn }
                >
                    <AppTitle>
                        <Heading>{ this.props.title }</Heading>
                    </AppTitle>
                    <AppContent>
                        <Heading size='m'>{ this.props.text }</Heading>
                        { this.props.returnUrl &&
                            <Link
                                className={ cn('return-link') }
                                size='l'
                                text={ this.props.returnTitle }
                                url={ this.props.returnUrl }
                            />
                        }
                    </AppContent>
                </Page>
            </I18n>
        );
    }
}

export default ErrorPage;
