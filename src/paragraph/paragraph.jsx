import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./paragraph.css');

/**
 * Компонент параграфа текста.
 */
@cn('paragraph')
@performance()
class Paragraph extends FeatherComponent {
    static propTypes = {
        /** Тип параграфа */
        view: Type.oneOf(['lead', 'normal']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        return (
            <p className={ cn({ view: this.props.view }) }>
                { this.props.children }
            </p>
        );
    }
}

export default Paragraph;
