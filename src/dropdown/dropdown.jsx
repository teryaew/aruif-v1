import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import Button from '../button/button';
import FeatherComponent from '../feather/feather';
import Link from '../link/link';
import Popup from '../popup/popup';

import performance from '../performance';

import cn from '../cn';

/**
 * Компонент "выпадашка": ссылка или кнопка. По клику показывается Popup.
 */
@cn('dropdown')
@performance()
class Dropdown extends FeatherComponent {
    static propTypes = {
        /** Локализация компонента */
        locale: Type.string,
        /** Тип компонента */
        switcherType: Type.oneOf(['link', 'button']),
        /** Текст кнопки компонента */
        switcherText: Type.node,
        /** Компонент [Popup](../popup/) */
        popupContent: Type.node,
        /** Свойства для компонента [Popup](../popup/) */
        popupProps: Type.object,
        /** Управление возможностью отображать попап при наведении курсора */
        mode: Type.oneOf(['hover', 'normal']),
        /** Управление возможностью открытия попапа */
        disabled: Type.bool,
        /** Управление состоянием открыт/закрыт попапа */
        opened: Type.bool,
        /** Только для switcherType='button'. Тип переключателя для кнопки, 'check' */
        togglable: Type.oneOf(['button', 'check']),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик клика по кнопке компонента */
        onSwitcherClick: Type.func,
        /** Обработчик события наведения курсора на кнопку компонента */
        onSwitcherMouseEnter: Type.func,
        /** Обработчик события снятия курсора с кнопки компонента */
        onSwitcherMouseLeave: Type.func,
        /** Обработчик события наведения курсора на попап */
        onPopupMouseEnter: Type.func,
        /** Обработчик события снятия курсора с попапа */
        onPopupMouseLeave: Type.func,
        /** Обработчик события клика попапа за пределами попапа */
        onPopupClickOutside: Type.func
    };

    static defaultProps = {
        switcherType: 'link',
        switcherText: (
            <FormattedMessage
                id='dropdown.switcherText'
                defaultMessage='Switcher'
            />
        ),
        disabled: false,
        size: 'm'
    };

    state = {
        opened: false,
        switcherHovered: false,
        popupHovered: false
    };

    popup;
    switcher;

    componentDidMount() {
        this.popup.setTarget(this.switcher.getNode());
    }

    render(cn) {
        return (
            <I18n locale={ this.props.locale }>
                <div
                    className={ cn({
                        switcher: this.props.switcher
                    }) }
                >
                    { this.renderSwitcher(cn) }
                    { this.renderPopup(cn) }
                </div>
            </I18n>
        );
    }

    renderSwitcher(cn) {
        let content = this.props.children || this.props.switcherText;
        let opened = this.props.opened !== undefined
            ? this.props.opened
            : this.state.opened;

        return this.props.switcherType === 'button'
            ? this.renderSwitcherButton(cn, content, opened)
            : this.renderSwitcherLink(cn, content);
    }

    renderSwitcherButton(cn, content, opened) {
        return (
            <Button
                className={ cn('switcher') }
                size={ this.props.size }
                ref={ (switcher) => { this.switcher = switcher; } }
                disabled = { this.props.disabled }
                togglable={ this.props.togglable }
                checked={ this.props.togglable === 'check' && opened }
                onClick={ !this.props.disabled ? this.handleSwitcherClick : undefined }
                onMouseEnter={ this.handleSwitcherMouseEnter }
                onMouseLeave={ this.handleSwitcherMouseLeave }
            >
                { content }
            </Button>
        );
    }

    renderSwitcherLink(cn, content) {
        return (
            <Link
                className={ cn('switcher') }
                size={ this.props.size }
                ref={ (switcher) => { this.switcher = switcher; } }
                pseudo={ true }
                onClick={ !this.props.disabled ? this.handleSwitcherClick : undefined }
                onMouseEnter={ this.handleSwitcherMouseEnter }
                onMouseLeave={ this.handleSwitcherMouseLeave }
                text={ content }
            />
        );
    }

    renderPopup(cn) {
        let opened = this.props.opened !== undefined
            ? this.props.opened
            : this.state.opened;

        let popupProps = {
            className: cn('popup'),
            size: this.props.size,
            autoclosable: true,
            ...this.props.popupProps
        };

        return (
            <Popup
                { ...popupProps }
                ref={ (popup) => { this.popup = popup; } }
                visible={
                    !this.props.disabled && opened ||
                    this.props.mode === 'hover' && (
                        this.state.switcherHovered ||
                        this.state.popupHovered
                    )
                }
                target='anchor'
                onMouseEnter={ this.handlePopupMouseEnter }
                onMouseLeave={ this.handlePopupMouseLeave }
                onClickOutside={ this.handlePopupClickOutside }
            >
                { this.props.popupContent }
            </Popup>
        );
    }

    @autobind
    handleSwitcherClick() {
        let newOpenedStatusValue = this.props.opened !== undefined
            ? !this.props.opened
            : !this.state.opened;

        this.setState({
            opened: newOpenedStatusValue
        });

        if (this.props.onSwitcherClick) {
            this.props.onSwitcherClick(newOpenedStatusValue);
        }
    }

    @autobind
    handleSwitcherMouseEnter() {
        this.setState({ switcherHovered: true });

        if (this.props.onSwitcherMouseEnter) {
            this.props.onSwitcherMouseEnter();
        }
    }

    @autobind
    handleSwitcherMouseLeave() {
        this.setState({ switcherHovered: false });

        if (this.props.onSwitcherMouseLeave) {
            this.props.onSwitcherMouseLeave();
        }
    }

    @autobind
    handlePopupMouseEnter() {
        this.setState({ popupHovered: true });

        if (this.props.onPopupMouseEnter) {
            this.props.onPopupMouseEnter();
        }
    }

    @autobind
    handlePopupMouseLeave() {
        this.setState({ popupHovered: false });

        if (this.props.onPopupMouseLeave) {
            this.props.onPopupMouseLeave();
        }
    }

    @autobind
    handlePopupClickOutside() {
        this.setState({ opened: false });

        if (this.props.onPopupClickOutside) {
            this.props.onPopupClickOutside();
        }
    }
}

export default Dropdown;
