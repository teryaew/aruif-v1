import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';

/**
 * Компонент позволяющий слушать изменения размера родительского элемента.
 * Для использования разместите его в элементе об изменении размера, которого
 * вы хотите знать и добавьте внешний обработчик `onResize`.
 *
 * Важно! Элемент, размер которого вы хотите измерять, должен обладать
 * css свойством `position: relative;`.
 */
class ResizeSensor extends FeatherComponent {
    static propTypes = {
        /** Callback на изменение размера родителя */
        onResize: Type.func
    };

    iframe;

    render() {
        let iframeStyle = {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            background: 'transparent',
            border: 'none',
            zIndex: -1
        };

        return (
            <iframe
                ref={ (iframe) => { this.iframe = iframe; } }
                style={ iframeStyle }
                tabIndex='-1'
            />
        );
    }

    componentDidMount() {
        this.iframe.contentWindow.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        this.iframe.contentWindow.removeEventListener('resize', this.handleResize);
    }

    @autobind
    handleResize() {
        if (this.props.onResize) {
            this.props.onResize();
        }
    }
}

export default ResizeSensor;
