import { render, cleanUp } from '../test-utils';

import Spin from './spin';

describe('spin', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let spin = render(<Spin />);

        expect(spin.node).to.exist;
    });

    it('should set class "spin_visible" when visible=true', () => {
        let spin = render(<Spin visible={ true } />);

        expect(spin.node).to.have.class('spin_visible');
    });
});
