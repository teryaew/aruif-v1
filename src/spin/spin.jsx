import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./spin.css');

/**
 * Компонент показывающий крутящееся кольцо загрузки.
 */
@cn('spin')
@performance()
class Spin extends FeatherComponent {
    static propTypes = {
        /** Управление видимостью компонента */
        visible: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        visible: false,
        size: 'm'
    };

    render(cn) {
        return (
            <span
                className={ cn({
                    size: this.props.size,
                    visible: this.props.visible
                }) }
            />
        );
    }
}

export default Spin;
