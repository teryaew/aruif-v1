import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';
import startOfDay from 'date-fns/start_of_day';
import formatDate from 'date-fns/format';
import isDateValid from 'date-fns/is_valid';

import { parse as parseDate } from '../lib/date-utils';
import Calendar from '../calendar/calendar';
import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';
import Input from '../input/input';
import Mq from '../mq/mq';
import Popup from '../popup/popup';

import { keyboardCode } from '../lib/keyboard';
import Modernizr from '../../tools/modernizr';
import performance from '../performance';

import cn from '../cn';
require('./calendar-input.css');

const CUSTOM_DATE_FORMAT = 'DD.MM.YYYY';
const NATIVE_DATE_FORMAT = 'YYYY-MM-DD';

const IS_BROWSER = typeof window !== 'undefined';
const SUPPORTS_INPUT_TYPE_DATE = IS_BROWSER && Modernizr.inputtypes.date;

/**
 * Компонент поля ввода даты.
 */
@cn('calendar-input', Input, Popup)
@performance(true)
class CalendarInput extends FeatherComponent {
    static propTypes = {
        /** Содержимое поля ввода, указанное по умолчанию */
        value: Type.string,
        /** Отображение попапа с ошибкой в момент когда фокус находится в поле ввода */
        error: Type.node,
        /** Свойства компонента [Calendar](../calendar/) */
        calendar: Type.object,
        /** Управление возможностью раскрытия календаря */
        opened: Type.bool,
        /** Управление возможностью компонента занимать всю ширину родителя */
        width: Type.oneOf(['default', 'available']),
        /** Направления, в которые может открываться попап компонента */
        directions: Type.arrayOf(Type.oneOf([
            'anchor', 'top-left', 'top-center', 'top-right', 'left-top', 'left-center', 'left-bottom', 'right-top',
            'right-center', 'right-bottom', 'bottom-left', 'bottom-center', 'bottom-right'
        ])),
        /** Управление возможностью изменения значения компонента */
        disabled: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Последовательность перехода между контролами при нажатии на Tab */
        tabIndex: Type.number,
        /** Показывать иконку календаря в инпуте */
        withIcon: Type.bool,
        /** Подсказка в текстовом поле */
        placeholder: Type.string,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик установки фокуса на компонент */
        onFocus: Type.func,
        /** Обработчик снятия фокуса с компонента */
        onBlur: Type.func,
        /** Обработчик установки фокуса на поле ввода */
        onInputFocus: Type.func,
        /** Обработчик снятия фокуса с поля ввода */
        onInputBlur: Type.func,
        /** Обработчик ввода даты в текстовом поле */
        onInputChange: Type.func,
        /** Обработчик выбора даты в календаре */
        onCalendarChange: Type.func,
        /** Обрабочик изменения даты в календаре */
        onChange: Type.func,
        /** Обработчик события нажатия на клавишу в момент, когда фокус находится на компоненте */
        onKeyDown: Type.func,
        /** Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится в календаре */
        onCalendarKeyDown: Type.func,
        /** Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на текстовом поле */
        onInputKeyDown: Type.func
    };

    static defaultProps = {
        withIcon: true,
        directions: ['bottom-left', 'bottom-right', 'top-left', 'top-right'],
        placeholder: '00.00.0000'
    };

    state = {
        isNativeInputEnabled: false,
        isInputFocused: false,
        isCalendarFocused: false,
        opened: false,
        value: '',
        month: this.calculateMonth(this.props.value)
    };

    timeoutId;

    calendar;
    calendarPopup;
    customCalendarTarget;
    nativeCalendarTarget;

    componentDidMount() {
        if (this.calendarPopup) {
            let element;

            if (this.nativeCalendarTarget) {
                element = this.nativeCalendarTarget;
            } else if (this.customCalendarTarget) {
                element = this.customCalendarTarget.getNode();
            }

            if (element) {
                this.calendarPopup.setTarget(element);
            }
        }
    }

    componentWillUnmount() {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
            this.timeoutId = null;
        }
    }

    render(cn, Input, Popup) {
        let value = this.props.value !== undefined
            ? this.props.value
            : this.state.value;

        let commonProps = {
            disabled: this.props.disabled,
            tabIndex: this.props.tabIndex,
            noValidate: true
        };

        return (
            <span className={ cn({ width: this.props.width }) }>
                <Input
                    ref={ (customCalendarTarget) => {
                        this.customCalendarTarget = customCalendarTarget;
                    } }
                    { ...commonProps }
                    className={ cn('custom-field') }
                    disabledAttr={ this.state.isNativeInputEnabled }
                    error={ this.props.error }
                    showErrorPopup={ this.state.isNativeInputEnabled }
                    focused={ this.state.isInputFocused || this.state.isCalendarFocused }
                    mask='11.11.1111'
                    size={ this.props.size }
                    type='text'
                    placeholder={ this.props.placeholder }
                    value={ value }
                    width={ this.props.width }
                    onBlur={ this.handleCustomInputBlur }
                    onChange={ this.handleCustomInputChange }
                    onFocus={ this.handleCustomInputFocus }
                    onKeyDown={ this.handleInputKeyDown }
                    icon={ this.props.withIcon &&
                        <Icon
                            size={ this.props.size }
                            tool='calendar'
                            onClick={ this.handleIconClick }
                        />
                    }
                />
                <Mq
                    query='--small-only'
                    touch={ true }
                    onMatchChange={ this.handleMqMatchChange }
                >
                    { SUPPORTS_INPUT_TYPE_DATE &&
                        <input
                            ref={ (nativeCalendarTarget) => {
                                this.nativeCalendarTarget = nativeCalendarTarget;
                            } }
                            { ...commonProps }
                            className={ cn('native-field') }
                            type='date'
                            value={ this.formatDateIntoNative(value) }
                            onBlur={ this.handleNativeInputBlur }
                            onChange={ this.handleNativeInputChange }
                            onFocus={ this.handleNativeInputFocus }
                        />
                    }
                </Mq>
                { this.renderPopup(value, Popup) }
            </span>
        );
    }

    renderPopup(value, Popup) {
        let opened = this.props.opened !== undefined
            ? this.props.opened
            : this.state.opened;

        return (
            <Popup
                ref={ (calendarPopup) => { this.calendarPopup = calendarPopup; } }
                autoclosable={ true }
                visible={ opened }
                directions={ this.props.directions }
                onClickOutside={ this.handleClickOutside }
            >
                <Calendar
                    ref={ (calendar) => { this.calendar = calendar; } }
                    month={ this.state.month }
                    { ...this.props.calendar }
                    error={ this.props.error }
                    value={ this.parseDate(value, CUSTOM_DATE_FORMAT) }
                    onBlur={ this.handleCalendarBlur }
                    onFocus={ this.handleCalendarFocus }
                    onKeyDown={ this.handleCalendarKeyDown }
                    onValueChange={ this.handleCalendarChange }
                    onMonthChange={ this.handleCalendarMonthChange }
                />
            </Popup>
        );
    }

    /**
     * Устанавливает фокус на поле ввода, открывает календарь.
     *
     * @public
     */
    focus() {
        let targetRef = this.nativeCalendarTarget || this.customCalendarTarget;

        targetRef.focus();
    }

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */
    blur() {
        let targetRef = this.nativeCalendarTarget || this.customCalendarTarget;

        targetRef.blur();
    }

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */
    scrollTo() {
        this.customCalendarTarget.scrollTo();
    }

    changeFocused(focusedState, event) {
        let newState = {
            isInputFocused: this.state.isInputFocused,
            isCalendarFocused: this.state.isCalendarFocused,
            ...focusedState
        };

        let newFocused = newState.isInputFocused || newState.isCalendarFocused;

        this.setState(focusedState);

        let newOpened = false;

        if (newFocused) {
            if (this.props.onFocus) {
                this.props.onFocus(event);
            }

            newOpened = true;
        } else {
            if (this.props.onBlur) {
                this.props.onBlur(event);
            }
        }

        if (!this.state.isNativeInputEnabled) {
            if (this.timeoutId) {
                clearTimeout(this.timeoutId);
            }
            this.timeoutId = setTimeout(() => {
                let value = this.props.value !== undefined
                    ? this.props.value
                    : this.state.value;

                let newMonth = this.state.opened !== newOpened
                    ? this.calculateMonth(value)
                    : this.state.month;

                this.setState({
                    opened: newOpened,
                    month: newMonth
                });

                this.timeoutId = null;
            }, 0);
        }
    }

    formatDateIntoCustom(value) {
        let date = this.parseDate(value, NATIVE_DATE_FORMAT);

        if (date) {
            return formatDate(date, CUSTOM_DATE_FORMAT);
        }
        return value;
    }

    /**
     * NB: В нативном календаре нельзя менять формат даты. Приемлем только YYYY-MM-DD формат.
     * https://www.w3.org/TR/html-markup/input.date.html#input.date.attrs.value
     * https://tools.ietf.org/html/rfc3339#section-5.6
     *
     * @param {String} [value] Дата
     *
     * @returns {String} Отформатированная в YYYY-MM-DD дата или необработанное значение (например: '')
     */
    formatDateIntoNative(value) {
        let date = this.parseDate(value, CUSTOM_DATE_FORMAT);

        if (date) {
            return formatDate(date, NATIVE_DATE_FORMAT);
        }
        return value;
    }

    @autobind
    handleCalendarChange(value, formatted, isTriggeredByKeyboard) {
        if (!isTriggeredByKeyboard) {
            this.setState({
                opened: false
            });
        }

        this.setState({ value: formatted });

        if (this.props.onCalendarChange) {
            this.props.onCalendarChange(formatted);
        }

        if (this.props.onChange) {
            this.props.onChange(formatted, value);
        }
    }

    @autobind
    handleCalendarMonthChange(month) {
        this.setState({
            month
        });
    }

    @autobind
    handleCalendarFocus(event) {
        this.changeFocused({ isCalendarFocused: true }, event);
    }

    @autobind
    handleCalendarBlur(event) {
        this.changeFocused({ isCalendarFocused: false }, event);
    }

    @autobind
    handleCalendarKeyDown(event) {
        switch (event.which) {
            case keyboardCode.ESCAPE:
                event.preventDefault();
                this.customCalendarTarget.focus();
                break;
            case keyboardCode.ENTER:
            case keyboardCode.SPACE:
                event.preventDefault();
                this.setState({
                    opened: false
                });
                break;
        }

        if (this.props.onCalendarKeyDown) {
            this.props.onCalendarKeyDown(event);
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    }

    @autobind
    handleIconClick() {
        this.customCalendarTarget.focus();
    }

    @autobind
    handleCustomInputChange(value) {
        this.setState({ value });

        if (this.props.onInputChange) {
            this.props.onInputChange(value);
        }

        if (this.props.onChange) {
            this.props.onChange(value, this.parseDate(value, CUSTOM_DATE_FORMAT));
        }
    }

    @autobind
    handleNativeInputChange(event) {
        let value = this.formatDateIntoCustom(event.target.value);

        // Детектим нажатие `сlear` в нативном календаре
        if (this.state.value === value) {
            value = '';
        }

        this.setState({ value });

        if (this.props.onInputChange) {
            this.props.onInputChange(value);
        }

        if (this.props.onChange) {
            this.props.onChange(value, this.parseDate(value, CUSTOM_DATE_FORMAT));
        }
    }

    @autobind
    handleCustomInputFocus(event) {
        this.changeFocused({ isInputFocused: true }, event);

        if (this.props.onInputFocus) {
            this.props.onInputFocus(event);
        }
    }

    @autobind
    handleNativeInputFocus(event) {
        // Копируем пришедший из аргументов SyntheticEvent для дальнейшего редактирования
        let resultEvent = Object.assign({}, event,
            // Трансформируем нативную YYYY-MM-DD дату в кастомный формат на вывод в коллбэках
            { target: { value: this.formatDateIntoCustom(event.target.value) }
        });

        this.changeFocused({ isInputFocused: true }, resultEvent);

        if (this.props.onInputFocus) {
            this.props.onInputFocus(resultEvent);
        }
    }

    @autobind
    handleCustomInputBlur(event) {
        this.changeFocused({ isInputFocused: false }, event);

        if (this.props.onInputBlur) {
            this.props.onInputBlur(event);
        }
    }

    @autobind
    handleNativeInputBlur(event) {
        // Копируем пришедший из аргументов SyntheticEvent для дальнейшего редактирования
        let resultEvent = Object.assign({}, event,
            // Трансформируем нативную YYYY-MM-DD дату в кастомный формат на вывод в коллбэках
            { target: { value: this.formatDateIntoCustom(event.target.value) }
        });

        this.changeFocused({ isInputFocused: false }, resultEvent);

        if (this.props.onInputBlur) {
            this.props.onInputBlur(resultEvent);
        }
    }

    @autobind
    handleInputKeyDown(event) {
        switch (event.which) {
            case keyboardCode.DOWN_ARROW: {
                event.preventDefault();

                let value = this.props.value !== undefined
                    ? this.props.value
                    : this.state.value;

                this.setState({
                    opened: true,
                    month: this.calculateMonth(value)
                });
                this.calendar.focus();

                break;
            }
            case keyboardCode.ESCAPE: {
                event.preventDefault();

                this.setState({
                    opened: false
                });

                break;
            }
        }

        if (this.props.onInputKeyDown) {
            this.props.onInputKeyDown(event);
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    }

    @autobind
    handleMqMatchChange(isMatched) {
        this.setState({
            isNativeInputEnabled: isMatched && SUPPORTS_INPUT_TYPE_DATE
        });
    }

    parseDate(value, format) {
        let result = null;
        let valueTrimmed = value.replace(/~+$/, '');

        // Проверяем, чтобы пользователь ввел полную строку даты без пробелов.
        if (valueTrimmed.length === format.length && !valueTrimmed.match(/\s/)) {
            let valueDate = parseDate(valueTrimmed, format);
            if (isDateValid(valueDate)) {
                result = valueDate.valueOf();
            }
        }

        return result;
    }

    calculateMonth(value) {
        let newValue = (value && this.parseDate(value, CUSTOM_DATE_FORMAT)) || Date.now();

        return startOfDay(newValue).valueOf();
    }
}

export default CalendarInput;
