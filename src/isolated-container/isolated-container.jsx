import FeatherComponent from '../feather/feather';

/**
 * Изолирует своих детей от изменений props-ов, и context-а.
 * Используется для визуализации элементов в кастомных контейнерах,
 * о которых React не должен ничего знать.
 */
class IsolatedContainer extends FeatherComponent {
    /**
     * @type {HTMLElement}
     */
    element;

    shouldComponentUpdate() {
        return false;
    }

    render() {
        return <div ref={ (element) => { this.element = element; } } />;
    }

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */
    getNode() {
        return this.element;
    }
}

export default IsolatedContainer;
