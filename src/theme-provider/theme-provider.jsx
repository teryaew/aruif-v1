import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';

import cn from '../cn';

/**
 * Компонент задающий тему для своих дочерних компонентов.
 * Важно! Может содержать в себе строго один дочерний компонент.
 *
 * @example
 * ```javascript
 * import ThemeProvider from 'arui-feather/src/theme-provider/theme-provider';
 * import Page from 'arui-feather/src/page/page';
 * import Heading from 'arui-feather/src/heading/heading';
 *
 * <ThemeProvider theme="alfa-on-color">
 *    <Page>
 *       <Heading>Заголовок страницы</Heading>
 *       <div style={{ background: "white" }}>
 *           <ThemeProvider theme="alfa-on-white">
 *               Врезка белого цвета на странице...
 *           </ThemeProvider>
 *       </div>
 *    </Page>
 * </ThemeProvider>
 * ```
 */
@cn('theme-provider')
class ThemeProvider extends FeatherComponent {
    static propTypes = {
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-colored', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static contextTypes = {
        theme: Type.string
    };

    static childContextTypes = {
        theme: Type.string
    };

    getChildContext() {
        return {
            theme: this.props.theme
        };
    }

    render() {
        if (this.props.children && this.props.children.length > 1) {
            throw new Error('You can provide only one child element to <ThemeProvider />');
        }

        return this.props.children && React.cloneElement(this.props.children);
    }
}

export default ThemeProvider;
