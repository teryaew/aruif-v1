import { render, cleanUp } from '../test-utils';

import InlineError from './inline-error';

describe('inline-error', () => {
    afterEach(cleanUp);

    it('renders without problems', () => {
        let error = render(<InlineError>Error content</InlineError>);

        expect(error.node).to.exist;
        expect(error.node).to.have.text('Error content');
    });
});
