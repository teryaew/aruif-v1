import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./inline-error.css');

/**
 * Компонент ошибки. Используется в calendar-input
 */
@cn('inline-error')
@performance()
class InlineError extends FeatherComponent {
    static propTypes = {
        /** Дополнительный класс */
        className: Type.any,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white'])
    };

    static defaultProps = {
        size: 'm'
    };

    render(cn) {
        return (
            <div className={ cn({ size: this.props.size }) }>
                { this.props.children }
            </div>
        );
    }
}

export default InlineError;
