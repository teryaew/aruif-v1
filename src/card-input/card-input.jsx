import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Input from '../input/input';

import performance from '../performance';

import cn from '../cn';

/**
 * Компонент поля ввода номера карты по маске
 *
 * @class
 * @extends Input
 */
@cn('card-input', Input)
@performance()
class CardInput extends FeatherComponent {
    static propTypes = {
        /** Подсказка в текстовом поле */
        placeholder: Type.string
    };

    static defaultProps = {
        placeholder: '0000 0000 0000 0000 00'
    };

    root;

    render(cn, Input) {
        return (
            <Input
                { ...this.props }
                type='tel'
                ref={ (root) => { this.root = root; } }
                mask='1111 1111 1111 1111 11'
                noValidate={ true }
                className={ cn }
            />
        );
    }

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */
    focus() {
        this.root.focus();
    }

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */
    blur() {
        this.root.blur();
    }

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */
    scrollTo() {
        this.root.scrollTo();
    }
}

export default CardInput;
