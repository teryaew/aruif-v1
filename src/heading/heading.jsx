import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./heading.css');

const HEADING_LEVEL = {
    xl: 1,
    l: 2,
    m: 3,
    s: 4
};

/**
 * Компонент заголовка.
 */
@cn('heading')
@performance()
class Heading extends FeatherComponent {
    static propTypes = {
        /** Выравнивание поля в форме // TODO @teryaew: think about it */
        align: Type.oneOf(['left', 'center', 'right', 'justify']),
        /** Дополнительный класс */
        className: Type.any,
        /** Размер, определяющий какой тег заголовка будет использоваться */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white'])
    };

    static defaultProps = {
        size: 'xl'
    };

    render(cn) {
        let { align, size } = this.props;
        let headingProps = {
            className: cn({
                align,
                size
            })
        };

        return React.createElement('h' + HEADING_LEVEL[size],
            headingProps,
            this.props.children
        );
    }
}

export default Heading;
