import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./logo.css');

/**
 * Компонент логотипа. Может отбражаться как в виде знака, так и в виде знак + написание.
 */
@cn('logo')
@performance()
class Logo extends FeatherComponent {
    static propTypes = {
        /** Тип логотипа */
        view: Type.oneOf(['default', 'full']),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        size: 'm'
    };

    render(cn) {
        return (
            <span className={ cn({ size: this.props.size }) }>
                <span className={ cn('icon') } />
                { this.props.view === 'full' &&
                    <span className={ cn('text') } />
                }
            </span>
        );
    }
}

export default Logo;
