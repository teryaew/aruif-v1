import { render, cleanUp } from '../test-utils';

import Logo from './logo';

describe('logo', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let logo = render(<Logo />);
        let logoIcon = logo.node.childNodes[0];

        expect(logo.node).to.exist;
        expect(logoIcon).to.have.class('logo__icon');
    });

    it('should render picture with text when view="full"', () => {
        let logo = render(<Logo view='full' />);
        let logoText = logo.node.childNodes[1];

        expect(logoText).to.have.class('logo__text');
    });
});
