import { render, cleanUp } from '../test-utils';

import AppContent from './app-content';

describe('app-content', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let appContent = render(<AppContent>AppContent-test</AppContent>);

        expect(appContent.node).to.exist;
        expect(appContent.node).to.have.text('AppContent-test');
    });
});
