import { render, cleanUp } from '../test-utils';

import User from './user';

describe('user', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let user = render(<User url='#' text='user' />);

        expect(user.node).to.exist;
        expect(user.node).to.have.class('user');
    });

    it('should call `onClick` callback after user was clicked', () => {
        let onClick = chai.spy();
        let user = render(<User onClick={ onClick } text='user' />);

        user.node.click();

        expect(onClick).to.have.been.called.once;
    });
});
