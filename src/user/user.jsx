import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';
import Link from '../link/link';

import performance from '../performance';

import cn from '../cn';

/**
 * Компонент доступа к пользовательскому профилю: cодержит имя пользователя и кнопку "Выйти".
 */
@cn('user')
@performance()
class User extends FeatherComponent {
    static propTypes = {
        /** href ссылки с именем пользователя */
        url: Type.string,
        /** Имя пользователя */
        text: Type.string,
        /** Иконка пользователя */
        icon: Type.node,
        /** Обработчик клика по пользователю */
        onClick: Type.func,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        url: '#'
    };

    render(cn) {
        let icon = this.props.icon !== undefined
            ? this.props.icon
            : <Icon className={ cn('icon') } size='s' user='body' />;

        return (
            <Link
                className={ cn() }
                icon={ icon }
                text={ this.props.text }
                url={ this.props.url }
                onClick={ this.handleClick }
            />
        );
    }

    @autobind
    handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }
}

export default User;
