import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Popup from '../popup/popup';

import createFragment from 'react-addons-create-fragment';
import performance from '../performance';

import cn from '../cn';
require('./radio-group.css');

/**
 * Компонент группы радио-кнопок.
 *
 * @example
 * ```
 * import 'Radio' from 'arui-feather/src/radio/radio';
 * import 'RadioGroup' from 'arui-feather/src/radio-group/radio-group';
 *
 * // Вертикальная группа радио кнопок
 * <RadioGroup>
 *    <Radio text="Кнопка раз" />
 *    <Radio text="Кнопка два" />
 *    <Radio text="Кнопка три" />
 * </RadioGroup>
 *
 * // Горизонтальная группа радио кнопок, состоящая из обычных кнопок
 * <RadioGroup type="button">
 *    <Radio type="button" text="Кнопка раз" />
 *    <Radio type="button" text="Кнопка два" />
 *    <Radio type="button" text="Кнопка три" />
 * </RadioGroup>
 *
 * // Горизонтальная группа радио кнопок
 * <RadioGroup type="line">
 *    <Radio text="Кнопка раз" />
 *    <Radio text="Кнопка два" />
 *    <Radio text="Кнопка три" />
 * </RadioGroup>
 * ```
 */
@cn('radio-group')
@performance()
class RadioGroup extends FeatherComponent {
    static propTypes = {
        /** Тип группы кнопок */
        type: Type.oneOf(['normal', 'button', 'line']),
        /** Значение выбранной радио-кнопки */
        value: Type.string,
        /** Отображение попапа с ошибкой в момент когда фокус находится на компоненте */
        error: Type.node,
        /** Расположение попапа с ошибкой (в порядке приоритета) относительно точки открытия */
        errorDirections: Type.arrayOf(Type.string),
        /** Управление шириной группы кнопок для типа 'button'. При значении 'available' растягивает группу на ширину родителя */
        width: Type.oneOf(['default', 'available']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик фокуса радиогруппы */
        onFocus: Type.func,
        /** Обработчик снятия фокуса с радиогруппы */
        onBlur: Type.func,
        /** Обработчик изменения значения 'checked' одного из дочерних радио-кнопок */
        onChange: Type.func
    };

    static defaultProps = {
        errorDirections: ['right-center', 'right-top', 'right-bottom', 'bottom-left']
    };

    state = {
        value: '',
        focused: false
    };

    root;

    componentDidMount() {
        this.ensureErrorPopupTarget();
    }

    componentDidUpdate() {
        this.ensureErrorPopupTarget();
    }

    render(cn) {
        let children = this.props.children
            ? this.props.children.length
                ? this.props.children
                : [this.props.children]
            : null;

        let radioGroupParts = {};
        let props;
        if (this.props.type === 'button') {
            props = { width: this.props.width };
        }

        if (children) {
            this.radios = [];

            let value = this.props.value !== undefined
                ? this.props.value
                : this.state.value;

            React.Children.forEach(children, (radio, index) => {
                radio = React.cloneElement(radio, {
                    ref: radio => this.radios.push(radio),
                    error: radio.props.error !== undefined
                        ? radio.props.error : Boolean(this.props.error),
                    checked: radio.props.checked !== undefined
                        ? radio.props.checked : (value === radio.props.value),
                    onChange: radio.props.onChange !== undefined
                        ? radio.props.onChange : this.handleRadioChange,
                    ...props
                });

                radioGroupParts[`radio-${index}`] =
                    (this.props.type !== 'button' && this.props.type !== 'line')
                        ? <div>{ radio }</div>
                        : radio;
            });
        }
        return (
            <span
                role='group'
                className={ cn({
                    type: this.props.type,
                    invalid: !!this.props.error,
                    ...props
                }) + ' control-group' }
                tabIndex='-1'
                onFocus={ this.handleFocus }
                onBlur={ this.handleBlur }
                ref={ (root) => { this.root = root; } }
            >
                { createFragment(radioGroupParts) }
                { this.renderErrorPopup() }
            </span>
        );
    }

    renderErrorPopup() {
        return (
            (this.props.error && this.state.focused) &&
                <Popup
                    directions={ this.props.errorDirections }
                    ref={ popup => { this.errorPopup = popup; } }
                    type='tooltip'
                    mainOffset={ 13 }
                    visible={ true }
                    invalid={ true }
                >
                    { this.props.error }
                </Popup>
        );
    }

    ensureErrorPopupTarget() {
        if (this.props.error && this.state.focused) {
            this.errorPopup.setTarget(this.root);
        }
    }

    /**
     * Устанавливает фокус на первую радиокнопку в группе.
     *
     * @public
     */
    focus() {
        if (this.radios && this.radios[0]) {
            this.radios[0].focus();
        }
    }

    /**
     * Убирает фокус с группы радио-кнопок.
     *
     * @public
     */
    blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    }

    @autobind
    handleRadioChange(value) {
        this.setState({ value });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    @autobind
    handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    }

    @autobind
    handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }
}

export default RadioGroup;
