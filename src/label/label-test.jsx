import { render, cleanUp } from '../test-utils';

import Label from './label';

describe('label', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let label = render(<Label>Label-test</Label>);

        expect(label.node).to.exist;
    });

    it('should render with size="m" by default', () => {
        let label = render(<Label />);

        expect(label.node).to.have.class('label_size_m');
    });
});
