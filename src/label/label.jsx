import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';

require('./label.css');

/**
 * Компонента лейбла.
 */
@cn('label')
@performance()
class Label extends FeatherComponent {
    static propTypes = {
        /** Отображение как второстепенная информация */
        minor: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['xs', 's', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Управление возможностью рендерить компонент в одну сроку */
        isNoWrap: Type.bool
    };

    static defaultProps = {
        isNoWrap: false,
        minor: false,
        size: 'm'
    };

    render(cn) {
        return (
            <span
                className={ cn({
                    size: this.props.size,
                    'no-wrap': this.props.isNoWrap,
                    minor: this.props.minor
                }) }
            >
                <span className={ cn('inner') }>
                    { this.props.children }
                </span>
            </span>
        );
    }
}

export default Label;
