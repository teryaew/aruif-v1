import benchmarkSuite from '../tools/benchmark-suite';

import { render } from './test-utils';
import bemCn from 'bem-cn-fast';
import cn from './cn';
import FeatherComponent from './feather/feather';

class NoCnTest extends FeatherComponent {
    render() {
        return <div className='cn-benchmark-test' />;
    }
}

const cnTest = bemCn('cn-benchmark-test');

class ClosureCnTest extends FeatherComponent {
    render() {
        return <div className={ cnTest } />;
    }
}

const cnTestCall = bemCn('cn-benchmark-test');

class ClosureCnCallTest extends FeatherComponent {
    render() {
        return <div className={ cnTestCall() } />;
    }
}

@cn('cn-benchmark-test')
class CnTest extends FeatherComponent {
    render(cn) {
        return <div className={ cn() } />;
    }
}

@cn('cn-benchmark-test')
class CnToStringTest extends FeatherComponent {
    render(cn) {
        return <div className={ cn } />;
    }
}

@cn('cn-benchmark-test')
class CnTestWithMods extends FeatherComponent {
    render(cn) {
        return <div className={ cn({ mod1: true, mod2: 'visible' }) } />;
    }
}

// Необходимо запускать тест производительности так, чтобы сохранить имена
// используюмых для теста зависимостей и экспортировать их в глобальный контекст.
// Так как `benchmark.js` перекомпилирует функции тестирования через `new Function()`
// и они теряют контекст замыкания.
//
// Для это используется функция замыкания, которая заставляет `webpack` сохранить имена
// зависимостей и утилита `benchmarkSuite`, которая экспортирует зависимости перед запуском
// теста в глобальный контекст, а после выполнения теста удаляет зависимости из него.
(function scope(render, NoCnTest, ClosureCnTest, ClosureCnCallTest, CnTest, CnToStringTest, CnTestWithMods) {
    suite('cn', function () {
        benchmark('render without `cn`', function () {
            render(<NoCnTest />, { container: this.element });
        });

        benchmark('render with `cn` in closure', function () {
            render(<ClosureCnTest />, { container: this.element });
        });

        benchmark('render with `cn` in closure call', function () {
            render(<ClosureCnCallTest />, { container: this.element });
        });

        benchmark('render with `cn` call', function () {
            render(<CnTest />, { container: this.element });
        });

        benchmark('render with `cn.toString()`', function () {
            render(<CnToStringTest />, { container: this.element });
        });

        benchmark('render with `cn` call with mods', function () {
            render(<CnTestWithMods />, { container: this.element });
        });
    }, benchmarkSuite('cn', scope, arguments));
}(render, NoCnTest, ClosureCnTest, ClosureCnCallTest, CnTest, CnToStringTest, CnTestWithMods));
