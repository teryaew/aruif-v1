import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import { deprecated } from '../lib/prop-types';

import cn from '../cn';
require('./form-field.css');

/**
 * Компонент поля формы: cодержит заголовок контрола и сам контрол.
 * Контрол должен быть передан дочерним компонентов.
 *
 * @example
 * ```javascript
 * import FormField from 'arui-feather/src/form-field/form-field';
 * import Input from 'arui-feather/src/input/input';
 *
 * <FormField label="Текстовое поле">
 *     <Input />
 * </FormField>
 * ```
 *
 * Компонент может использоваться для отображения заголовков слева от блока.
 * Используется совместно с компонентом `AppContent`.
 *
 * ```javascript
 * import AppContent from 'arui-feather/src/app-content/app-content';
 * import FormField from 'arui-feather/src/form-field/form-field';
 * import Label from 'arui-feather/src/label/label';
 *
 * <AppContent>
 *    <FormField view="line" label={ <Label>Заголовок блока</Label> }>
 *       Содержимое блока
 *    </FormField>
 *    <FormField view="line" label={ <Label>Заголовок блока</Label> }>
 *       Содержимое блока
 *    </FormField>
 * </AppContent>
 * ```
 */
@cn('form-field')
@performance()
class FormField extends FeatherComponent {
    static propTypes = {
        /** Выравнивание поля в форме // TODO @teryaew: think about it */
        align: Type.oneOf(['left', 'center', 'right', 'justify']),
        /** DEPRECATED! Заголовок для контрола */
        label: deprecated(Type.node),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Расположение элемента label: 'line' */
        view: Type.string,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        align: 'left',
        size: 'm'
    };

    render(cn) {
        return (
            <div
                className={ cn({
                    align: this.props.align,
                    size: this.props.size,
                    view: this.props.view
                }) }
            >
                <div className={ cn('content') }>
                    { this.props.children }
                </div>
            </div>
        );
    }
}

export default FormField;
