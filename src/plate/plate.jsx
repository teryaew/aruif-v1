import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';

import { deprecated } from '../lib/prop-types';
import performance from '../performance';

import cn from '../cn';
require('./plate.css');

/**
 * Компонент плашки.
 */
@cn('plate')
@performance()
class Plate extends FeatherComponent {
    static propTypes = {
        /** Управление наличием закрывающего крестика */
        hasCloser: Type.bool,
        /** Используйте `hasCloser`. Управляет наличием закрывающего крестика */
        hasClear: deprecated(Type.bool, 'Use `hasCloser instead`'),
        /** Плоская тема */
        isFlat: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик клика по плашке */
        onClick: Type.func,
        /** Обработчик клика по крестику */
        onCloserClick: Type.func,
        /** Используйте `onCloserClick`. Обработчик клика по крестику */
        onCloseClick: deprecated(Type.func, 'Use `onCloserClick` instead')
    };

    static defaultProps = {
        hasCloser: false
    };

    state = {
        isHidden: false
    };

    render(cn) {
        let hasCloser = this.props.hasCloser || this.props.hasClear;
        return (
            <span
                className={ cn({
                    'has-clear': hasCloser,
                    hidden: hasCloser && this.state.isHidden,
                    flat: this.props.isFlat
                }) }
                onClick={ this.handleClick }
            >
                { this.props.children }
                { hasCloser &&
                    <div className={ cn('clear') } >
                        <Icon
                            theme='alfa-on-white'
                            onClick={ this.handleCloseClick }
                            tool='close'
                        />
                    </div>
                }
            </span>
        );
    }

    @autobind
    handleClick() {
        if (this.props.onClick) {
            this.props.onClick();
        }
    }

    @autobind
    handleCloseClick() {
        this.setState({
            isHidden: true
        });

        let onCloserClick = this.props.onCloserClick || this.props.onCloseClick;

        if (onCloserClick) {
            onCloserClick();
        }
    }
}

export default Plate;
