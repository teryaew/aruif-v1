import React from 'react';

/**
 * Базовый абстрактный компонент ARUI Feather.
 * Все компоненты наследуются от него.
 *
 * @class
 * @abstract
 */
class FeatherComponent extends React.Component {}

export default FeatherComponent;
