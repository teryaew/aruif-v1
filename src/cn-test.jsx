import { render, cleanUp } from './test-utils';
import FeatherComponent from './feather/feather';
import cn from './cn';

describe('cn', function () {
    afterEach(cleanUp);

    it('should render decorated class with base css-class `cn-test`', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        let cnTest = render(<CnTestComponent />);
        expect(cnTest.node).to.have.class('cn-test');
    });

    it('should override css-class on extended class', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        @cn('cn-test-extended')
        class CnTestExtendedComponent extends CnTestComponent {}

        let cnTest = render(<CnTestExtendedComponent />);
        expect(cnTest.node).to.have.class('cn-test-extended');
    });

    it('should keep css-class on extended class without `cn` reuse', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        class CnTestExtendedComponent extends CnTestComponent {}

        let cnTest = render(<CnTestExtendedComponent />);
        expect(cnTest.node).to.have.class('cn-test');
    });

    it('should render decorated class with element `test-element` with css-class `cn-test__test-element`', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return (
                    <div className={ cn }>
                        <div className={ cn('test-element') } />
                    </div>
                );
            }
        }

        let cnTest = render(<CnTestComponent />);
        let cnTestElementNode = cnTest.node.querySelector('.cn-test__test-element');
        expect(cnTestElementNode).to.exist;
    });

    it('should render decorated class with modificator `mod` with css-class `cn-test_mod`', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn({ mod: this.props.mod }) } />;
            }
        }

        let cnTest = render(<CnTestComponent mod={ true } />);
        expect(cnTest.node).to.have.class('cn-test_mod');
    });

    it('should render decorated class with modificator `mod`=`some` with css-class `cn-test_mod_some`', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn({ mod: this.props.mod }) } />;
            }
        }

        let cnTest = render(<CnTestComponent mod='some' />);
        expect(cnTest.node).to.have.class('cn-test_mod_some');
    });

    it(
        'should render decorated class with element `test-element` with element mod ' +
        'with css-class `cn-test__test-element_mod`',
        function () {
            @cn('cn-test')
            class CnTestComponent extends FeatherComponent {
                render(cn) {
                    return (
                        <div className={ cn }>
                            <div className={ cn('test-element', { mod: true }) } />
                        </div>
                    );
                }
            }

            let cnTest = render(<CnTestComponent />);
            let cnTestElementNode = cnTest.node.querySelector('.cn-test__test-element_mod');
            expect(cnTestElementNode).to.exist;
        }
    );

    it('should render decorated class with additional css-class passed through `className` prop', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        let cnTest = render(<CnTestComponent className='additional-class' />);
        expect(cnTest.node).to.have.class('additional-class');
    });

    it('should render decorated class with default `theme`=`alfa-on-color`', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        let cnTest = render(<CnTestComponent />);
        expect(cnTest.node).to.have.class('cn-test_theme_alfa-on-color');
    });

    it('should render decorated class with `theme`=`alfa-on-white` passed through `theme` prop', function () {
        @cn('cn-test')
        class CnTestComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        let cnTest = render(<CnTestComponent theme='alfa-on-white' />);
        expect(cnTest.node).to.have.class('cn-test_theme_alfa-on-white');
    });

    it('should inject component', function () {
        @cn('cn-injected-test')
        class InjectedComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        @cn('cn-test', InjectedComponent)
        class CnTestComponent extends FeatherComponent {
            render(cn, InjectedComponent) {
                return <InjectedComponent className={ cn } />;
            }
        }

        let cnTest = render(<CnTestComponent />);
        expect(cnTest.node).to.have.class('cn-test');
        expect(cnTest.node).to.have.class('cn-injected-test');
    });

    it('should override injected component on extended class', function () {
        @cn('cn-injected-test')
        class InjectedComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        @cn('cn-injected-override-test')
        class InjectedOverrideComponent extends FeatherComponent {
            render(cn) {
                return <div className={ cn } />;
            }
        }

        @cn('cn-test', InjectedComponent)
        class CnTestComponent extends FeatherComponent {
            render(cn, InjectedComponent) {
                return <InjectedComponent className={ cn } />;
            }
        }

        @cn('cn-test-extended', InjectedOverrideComponent)
        class CnTestExtendedComponent extends CnTestComponent {}

        let cnTest = render(<CnTestExtendedComponent />);
        expect(cnTest.node).to.have.class('cn-test-extended');
        expect(cnTest.node).to.have.class('cn-injected-override-test');
    });
});
