import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';
import uniqueId from 'lodash.uniqueid';

import Icon from '../icon/icon';
import Mq from '../mq/mq';
import PopupContainerProvider from '../popup-container-provider/popup-container-provider';
import RenderInContainer from '../render-in-container/render-in-container';
import ThemeProvider from '../theme-provider/theme-provider';

import cn from '../cn';
import { isEventOutsideClientBounds } from '../lib/window';
import { keyboardCode } from '../lib/keyboard';
import performance from '../performance';

require('./modal.css');

const MODAL_OPENED_BODY_CLASS = 'body_modal_opened';

const openedModalsPool = {};
let savedScrollPosition = 0;

/**
 * Обновляет стили body. Нужно в первую очередь для сохранения позиции скрола при отображении/скрытии модального окна
 */
function updateBodyStyles() {
    const isModalOpened = document.body.classList.contains(MODAL_OPENED_BODY_CLASS);
    const shouldModalBeOpened = Object.values(openedModalsPool).some(v => v);

    if (isModalOpened && !shouldModalBeOpened) { // yep, i know about toggle, but i need to update some js-computed styles
        document.body.style.top = '';
        document.body.classList.remove(MODAL_OPENED_BODY_CLASS);
        document.body.scrollTop = savedScrollPosition;
    } else if (!isModalOpened && shouldModalBeOpened) {
        savedScrollPosition = document.body.scrollTop;
        document.body.style.top = `-${document.body.scrollTop}px`;
        document.body.classList.add(MODAL_OPENED_BODY_CLASS);
    }
}

@cn('modal')
@performance()
export default class Modal extends React.Component {
    static propTypes = {
        /** Управление наличием закрывающего крестика */
        hasCloser: Type.bool,
        /** Управление видимостью компонента */
        visible: Type.bool.isRequired,
        /** Управление возможностью автозакрытия компонента */
        autoclosable: Type.bool,
        /** Управляет высотой модального окна */
        height: Type.oneOf(['auto', 'available']),
        /** Обработчик закрытия компонента */
        onClose: Type.func.isRequired,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        hasCloser: false,
        visible: true,
        autoclosable: true,
        height: 'auto'
    };

    state = {
        small: false
    };

    id = uniqueId('modal');
    innerDomElement = null;

    componentDidMount() {
        openedModalsPool[this.id] = this.props.visible;
        updateBodyStyles();
        window.addEventListener('keydown', this.handleKeyDown);
        window.addEventListener('click', this.handleWindowClick);
    }

    componentWillReceiveProps(nextProps) {
        openedModalsPool[this.id] = nextProps.visible;
        updateBodyStyles();
    }

    componentWillUnmount() {
        delete openedModalsPool[this.id];
        updateBodyStyles();
        window.removeEventListener('keydown', this.handleKeyDown);
        window.removeEventListener('click', this.handleWindowClick);
    }

    render(cn) {
        return (
            <RenderInContainer
                className={ cn('wrapper', {
                    hidden: !this.props.visible,
                    height: this.props.height,
                    'on-white': !this.state.small
                }) }
            >
                <ThemeProvider theme={ this.state.small ? 'alfa-on-color' : 'alfa-on-white' }>
                    <PopupContainerProvider
                        className={ cn() }
                    >
                        <div ref={ elem => { this.innerDomElement = elem; } }>
                            <Mq query='--small-only' onMatchChange={ this.handleMqMatchChange } children={ null } />
                            { this.props.children }
                            { this.props.hasCloser &&
                                <Icon
                                    className={ cn('closer') }
                                    onClick={ this.handleCloserClick }
                                    tool='close'
                                />
                            }
                        </div>
                    </PopupContainerProvider>
                </ThemeProvider>
            </RenderInContainer>
        );
    }

    @autobind
    handleCloserClick() {
        this.props.onClose();
    }

    @autobind
    handleMqMatchChange(isSmall) {
        this.setState({
            small: isSmall
        });
    }

    @autobind
    handleKeyDown(event) {
        switch (event.which) {
            case keyboardCode.ESCAPE:
                event.preventDefault();
                this.props.onClose();
                break;
        }
    }

    @autobind
    handleWindowClick(event) {
        if (this.props.autoclosable && isEventOutsideClientBounds(event, this.innerDomElement)) {
            this.props.onClose();
        }
    }
}
