import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import Dropdown from '../dropdown/dropdown';
import FeatherComponent from '../feather/feather';
import Link from '../link/link';

import performance from '../performance';

import cn from '../cn';
require('./menu-item.css');

/**
 * Компонент элемента меню. Как правило, используется совместно с Menu.
 */
@cn('menu-item')
@performance()
class MenuItem extends FeatherComponent {
    static propTypes = {
        /** Тип элемента меню */
        type: Type.oneOf(['link', 'dropdown', 'block']),
        /** Тип ссылки, для компонента с type='link' */
        view: Type.oneOf(['default', 'link', 'pseudo', 'text']),
        /** href ссылки, для компонента с type='link' */
        url: Type.string,
        /** target для ссылки */
        target: Type.oneOf(['_self', '_blank', '_parent', '_top']),
        /** Уникальное значение элемента. Для использования в Menu */
        value: Type.oneOfType([
            Type.string,
            Type.number
        ]),
        /** Попап для компонента с type='dropdown' */
        popup: Type.node,
        /** Управление возможностью выбирать данный компонент */
        disabled: Type.bool,
        /** Управление состоянием выбран/не выбран компонента */
        checked: Type.bool,
        /** Управление видимостью компонента */
        hidden: Type.bool,
        /** Управление визуальным выделением компонента */
        hovered: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Только для type='link', обработчик клика по компоненту */
        onClick: Type.func,
        /** Обработчик фокуса компонента */
        onFocus: Type.func,
        /** Обработчик снятия фокуса компонента */
        onBlur: Type.func,
        /** Обработчик события наведения курсора на элемент меню */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с элемента меню */
        onMouseLeave: Type.func
    };

    static defaultProps = {
        type: 'link'
    };

    state = {
        hovered: false,
        focused: false
    };

    control;

    render(cn) {
        let content = this.props.children || this.props.value;
        let itemElement;
        let menuItemProps = null;

        switch (this.props.type) {
            case 'dropdown':
                itemElement = (
                    <Dropdown
                        ref={ (control) => { this.control = control; } }
                        className={ cn('control') + ' ' + cn('dropdown') }
                        size={ this.props.size }
                        opened={ this.state.hovered }
                        switcherType='link'
                        switcherText={ content }
                        popupContent={ this.props.popup }
                        popupProps={ {
                            directions: ['top-left'],
                            height: 'available',
                            target: 'anchor'
                        } }
                        mode='hover'
                        onSwitcherClick={ this.handleClick }
                        onSwitcherMouseEnter={ this.handleMouseEnter }
                        onSwitcherMouseLeave={ this.handleMouseLeave }
                    />
                );
                break;
            case 'block':
                menuItemProps = {
                    ref: (control) => { this.control = control; },
                    onClick: this.handleClick,
                    onMouseEnter: this.handleMouseEnter,
                    onMouseLeave: this.handleMouseLeave
                };
                itemElement = <span className={ cn('control') }>{ content }</span>;

                break;
            case 'link':
            default:
                itemElement = (
                    <Link
                        ref={ (control) => { this.control = control; } }
                        className={ cn('control') + ' ' + cn('link') }
                        size={ this.props.size }
                        pseudo={ this.props.view === 'pseudo' }
                        underlined={ false }
                        disabled={ this.props.disabled }
                        text={ content }
                        url={ this.props.url }
                        target={ this.props.target }
                        onClick={ this.handleClick }
                        onFocus={ this.handleFocus }
                        onBlur={ this.handleBlur }
                        onMouseEnter={ this.handleMouseEnter }
                        onMouseLeave={ this.handleMouseLeave }
                    />
                );
                break;
        }

        return (
            <div
                className={ cn({
                    disabled: this.props.disabled,
                    checked: this.props.checked,
                    hidden: this.props.hidden,
                    type: this.props.type,
                    hovered: this.props.hovered === undefined ? this.state.hovered : this.props.hovered,
                    focused: this.state.focused
                }) }
                { ...menuItemProps }
            >
                { itemElement }
            </div>
        );
    }

    /**
     * Устанавливает фокус на элементе меню.
     *
     * @public
     */
    focus() {
        this.control.focus();
    }

    /**
     * Убирает фокус с элемента меню.
     *
     * @public
     */
    blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    }

    @autobind
    handleClick(e) {
        if (this.props.disabled) {
            e.preventDefault();
            return;
        }

        if (this.props.onClick) {
            this.props.onClick(e);
        }
    }

    @autobind
    handleFocus(e) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(e);
        }
    }

    @autobind
    handleBlur(e) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    }

    @autobind
    handleMouseEnter(e) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(e);
        }
    }

    @autobind
    handleMouseLeave(e) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(e);
        }
    }
}

export default MenuItem;
