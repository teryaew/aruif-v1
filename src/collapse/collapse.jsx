import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import FeatherComponent from '../feather/feather';
import Link from '../link/link';
import ResizeSensor from '../resize-sensor/resize-sensor';

import performance from '../performance';

import cn from '../cn';
require('./collapse.css');

/**
 * Компонент "подката": позволяет спрятать кусок текста за ссылку "Еще...".
 */
@cn('collapse')
@performance()
class Collapse extends FeatherComponent {
    static propTypes = {
        /** Управление состоянием expand/collapse компонента */
        isExpanded: Type.bool,
        /** Текст ссылки в expand состоянии */
        collapsedLabel: Type.string,
        /** Текст ссылки в collapse состоянии */
        expandedLabel: Type.string,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик смены состояния expand/collapse */
        onExpandedChange: Type.func
    };

    static defaultProps = {
        expandedLabel: (
            <FormattedMessage
                id='collapse.expandedLabel'
                defaultMessage='Collapse'
            />
        ),
        collapsedLabel: (
            <FormattedMessage
                id='collapse.collapsedLabel'
                defaultMessage='Expand'
            />
        )
    };

    state = {
        isExpanded: false
    };

    content;
    contentCase;

    componentDidMount() {
        this.updateContentHeight();
    }

    componentDidUpdate() {
        this.updateContentHeight();
    }

    render(cn) {
        let expanded = this.props.isExpanded !== undefined
            ? this.props.isExpanded
            : this.state.isExpanded;

        return (
            <I18n locale={ this.props.locale }>
                <div
                    className={ cn({ expanded }) }
                >
                    <div
                        ref={ (content) => { this.content = content; } }
                        className={ cn('content') }
                    >
                        <div ref={ (contentCase) => { this.contentCase = contentCase; } }>
                            { this.props.children }
                            <ResizeSensor onResize={ this.updateContentHeight } />
                        </div>
                        <Link
                            className={ cn('link') }
                            pseudo={ true }
                            onClick={ this.handleExpandedChange }
                            text={ expanded
                                ? this.props.expandedLabel
                                : this.props.collapsedLabel
                            }
                        />
                    </div>
                </div>
            </I18n>
        );
    }

    @autobind
    handleExpandedChange() {
        let newExpandedValue = this.props.isExpanded !== undefined
            ? !this.props.isExpanded
            : !this.state.isExpanded;

        this.setState({
            isExpanded: newExpandedValue
        });

        if (this.props.onExpandedChange) {
            this.props.onExpandedChange(newExpandedValue);
        }
    }

    @autobind
    updateContentHeight() {
        let expanded = this.props.isExpanded !== undefined
            ? this.props.isExpanded
            : this.state.isExpanded;

        let contentHeight;

        if (expanded) {
            contentHeight = this.contentCase.offsetHeight;
        } else {
            contentHeight = 0;
        }

        if (this.content) {
            this.content.style.height = `${contentHeight}px`;
        }
    }
}

export default Collapse;
