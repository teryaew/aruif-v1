import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import MaskedInput from '../masked-input/masked-input';

import performance from '../performance';
import { scrollTo } from '../lib/scroll-to';
import { SCROLL_TO_CORRECTION } from '../vars';

import cn from '../cn';
// require('./input_mode_link.css');
require('./input_theme_alfa-on-color.css');
require('./input_theme_alfa-on-white.css');
require('./input.css');

// Оптимальное количество символов для дефолтного инпута в соответствии с min-width поля и размерами шрифта
const OPTIMAL_SIZE = 9;

/**
 * Компонент текстового поля ввода.
 */
@cn('input', MaskedInput)
@performance()
class Input extends FeatherComponent {
    static propTypes = {
        /** Лейбл для поля */
        label: Type.node,
        /** Подсказка под полем */
        tip: Type.node,
        /** Тип поля */
        type: Type.oneOf(['card', 'email', 'file', 'hidden', 'money', 'password', 'tel', 'text']),
        /** Управление возможностью компонента занимать всю ширину родителя */
        width: Type.oneOf(['default', 'available']),
        /** Управление автозаполнением компонента */
        autocomplete: Type.bool,
        /** Управление возможностью изменения атрибута компонента, установка соответствующего класса-модификатора для оформления */
        disabled: Type.bool,
        /** Управление возможностью изменения атрибута компонента (без установки класса-модификатора для оформления) */
        disabledAttr: Type.bool,
        /** Управление возможностью изменения класса-модификатора компонента */
        focused: Type.bool,
        /** Максимальное число символов */
        maxLength: Type.number,
        /** Иконка компонента */
        icon: Type.node,
        /** Управление наличием крестика, сбрасывающего значение 'value' */
        clear: Type.bool,
        /** Уникальный id блока */
        id: Type.string,
        /** Уникальное имя блока */
        name: Type.string,
        /** Содержимое поля ввода, указанное по умолчанию */
        value: Type.string,
        /** Последовательность перехода между контролами при нажатии на Tab */
        tabIndex: Type.number,
        /** Подсказка в текстовом поле */
        placeholder: Type.string,
        /** Определяет маску для ввода значений. [Шаблон маски](https://github.com/insin/inputmask-core#pattern) */
        mask: Type.string,
        /** Стандартное ствойство HTMLInputElement 'pattern'. Может быть использовано для показа корректной клавиатуры на мобильных устройствах. */
        pattern: Type.string,
        /** Управление встроенной проверкой данных введённых пользователем в поле на корректность */
        noValidate: Type.bool,
        /** Добавление дополнительных элементов к инпуту слева */
        leftAddons: Type.node,
        /** Добавление дополнительных элементов к инпуту справа */
        rightAddons: Type.node,
        /** Отображение ошибки */
        error: Type.node,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl', 'xxl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик изменения значения 'value' */
        onChange: Type.func,
        /** Обработчик фокуса поля */
        onFocus: Type.func,
        /** Обработчик клика по полю */
        onClick: Type.func,
        /** Обработчик снятия фокуса с поля */
        onBlur: Type.func,
        /** Обработчик клика по крестику сбрасываещему значение 'value' */
        onClearClick: Type.func,
        /** Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте */
        onKeyDown: Type.func,
        /** Обработчик события отжатия на клавишу клавиатуры в момент, когда фокус находится на компоненте */
        onKeyUp: Type.func,
        /** Обработчик события вставки текста в поле */
        onPaste: Type.func,
        /** Обработчик, вызываемый перед началом ввода в маскированное поле */
        onProcessMaskInputEvent: Type.func
    };

    static defaultProps = {
        noValidate: false,
        size: 'm',
        type: 'text'
    };

    state = {
        focused: false,
        value: ''
    };

    root;
    control;

    render(cn, MaskedInput) {
        let hasAddons = !!this.props.rightAddons || !!this.props.leftAddons;

        let content = this.renderContent(cn, MaskedInput);
        if (hasAddons) {
            content = <span className={ cn('addons-layout') }>{ content }</span>;
        }

        let focused = this.getFocusedState();

        return (
            <span
                className={ cn({
                    type: this.props.type,
                    disabled: this.props.disabled,
                    focused,
                    width: this.props.width,
                    size: this.props.size,
                    'has-label': !!this.props.label,
                    'has-value': !!this.props.value || !!this.state.value,
                    'has-icon': !!this.props.icon,
                    'has-clear': !!this.props.clear,
                    'has-addons': hasAddons,
                    invalid: !!this.props.error
                }) }
                ref={ (root) => { this.root = root; } }
            >
                { content }
                {
                    this.props.tip &&
                        <span className={ cn('tip') }>
                            { this.props.tip }
                        </span>
                }
            </span>
        );
    }

    renderContent(cn, MaskedInput) {
        return ([
            this.props.leftAddons &&
                <span className={ cn('addons', { left: true }) } key='left-addons'>
                    { this.props.leftAddons }
                </span>,
            this.renderInput(cn, MaskedInput),
            this.props.rightAddons &&
                <span className={ cn('addons', { right: true }) } key='right-addons'>
                    { this.props.rightAddons }
                </span>
        ]);
    }

    renderInput(cn, MaskedInput) {
        let value = this.props.value !== undefined
            ? this.props.value
            : this.state.value;

        let isMaskedInput = this.props.mask !== undefined;

        // Нормализация минимальной ширины инпута между браузерами
        let size = this.props.maxLength || OPTIMAL_SIZE;

        let inputProps = {
            className: cn('control'),
            type: this.props.type,
            noValidate: this.props.noValidate,
            autoComplete: this.props.autocomplete === false ? 'off' : 'on',
            disabled: this.props.disabled || this.props.disabledAttr,
            maxLength: this.props.maxLength,
            id: this.props.id,
            name: this.props.name,
            value,
            tabIndex: this.props.tabIndex,
            placeholder: this.props.placeholder,
            pattern: this.props.pattern,
            ref: (control) => { this.control = control; },
            size,
            onChange: this.handleChange,
            onFocus: this.handleFocus,
            onClick: this.handleClick,
            onBlur: this.handleBlur,
            onKeyDown: this.handleKeyDown,
            onKeyUp: this.handleKeyUp,
            onPaste: this.handlePaste
        };

        return (
            <span
                className={ cn('box') }
                key='box'
            >
                {
                    (this.props.error || !!this.props.label) &&
                    <span className={ cn('label') }>{ this.props.error || this.props.label }</span>
                }
                {
                    !isMaskedInput
                        ? <input { ...inputProps } />
                        : <MaskedInput
                            { ...inputProps }
                            mask={ this.props.mask }
                            onProcessInputEvent={ this.props.onProcessMaskInputEvent }
                        />
                }
                {
                    this.props.clear &&
                    <span
                        className={ cn('clear', { visible: !!value }) }
                        onClick={ this.handleClearClick }
                    />
                }
                {
                    this.props.icon &&
                    <span className={ cn('icon') }>
                        { this.props.icon }
                    </span>
                }
            </span>
        );
    }

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */
    getNode() {
        return this.root;
    }

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */
    focus() {
        this.control.focus();
    }

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */
    blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    }

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */
    scrollTo() {
        scrollTo({
            targetY: this.root.getBoundingClientRect().top + window.pageYOffset - SCROLL_TO_CORRECTION
        });
    }

    /**
     * Устанавливает начальное и конечное положение выделения текста в элементе.
     *
     * @public
     * @param {Number} [start=0] Индекс первого выделенного символа.
     * @param {Number} [end=value.length] Индекс символа после последнего выделенного символа.
     */
    setSelectionRange(start = 0, end = this.control.value.length) {
        this.control.setSelectionRange(start, end);
    }

    /**
     * Возвращает ссылку на инстанс контрола.
     * Для полей ввода с маской ссылку на объект `MaskedInput`.
     *
     * @public
     * @returns {React.Component}
     */
    getControl() {
        return this.control;
    }

    changeValue(value) {
        this.setState({ value });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    @autobind
    handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    }

    @autobind
    handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    @autobind
    handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    @autobind
    handleChange(event) {
        this.changeValue(event.target.value);
    }

    @autobind
    handleClearClick() {
        this.changeValue('');

        if (this.props.onClearClick) {
            this.props.onClearClick();
        }

        this.focus();
    }

    @autobind
    handleKeyDown(event) {
        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    }

    @autobind
    handleKeyUp(event) {
        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }
    }

    @autobind
    handlePaste(event) {
        if (this.props.onPaste) {
            this.props.onPaste(event);
        }
    }

    getFocusedState() {
        return this.props.focused !== undefined ? this.props.focused : this.state.focused;
    }
}

export default Input;
