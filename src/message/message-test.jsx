import { render, cleanUp } from '../test-utils';

import Message from './message';

describe('message', () => {
    afterEach(cleanUp);

    it('should render text message by default', () => {
        let message = render(<Message>Message-example</Message>);

        expect(message.node).to.exist;
        expect(message.node).to.have.text('Message-example');
        expect(message.node).to.have.class('message');
    });
});
