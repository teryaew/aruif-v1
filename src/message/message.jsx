import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Popup from '../popup/popup';

import performance from '../performance';

import cn from '../cn';
require('./message.css');

/**
 * Компонент сообщения.
 */
@cn('message')
@performance()
class Message extends FeatherComponent {
    static propTypes = {
        /** Тип компонента */
        type: Type.oneOf(['text', 'popup']),
        /** Управление видимостью компонента */
        visible: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        type: 'text'
    };

    popup;
    popupTarget;

    componentDidMount() {
        if (this.popup && this.popupTarget) {
            this.popup.setTarget(this.popupTarget);
        }
    }

    render(cn) {
        return this.props.type === 'text'
            ? this.renderTextMessage(cn)
            : this.renderPopupMessage(cn);
    }

    renderTextMessage(cn) {
        return (
            <div
                className={ cn({
                    type: 'text',
                    showed: this.props.visible
                }) + ' message__control' }
            >
                { this.props.children }
            </div>
        );
    }

    renderPopupMessage(cn) {
        return (
            <div>
                <div
                    className={ cn({ type: 'popup' }) }
                    ref={ (popupTarget) => { this.popupTarget = popupTarget; } }
                >
                    <Popup
                        className={ cn('control') }
                        ref={ (popup) => { this.popup = popup; } }
                        visible={ this.props.visible }
                    >
                        { this.props.children }
                    </Popup>
                </div>
            </div>
        );
    }
}

export default Message;
