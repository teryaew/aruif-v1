import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import Button from '../button/button';
import FeatherComponent from '../feather/feather';

import performance from '../performance';
import { scrollTo } from '../lib/scroll-to';
import { SCROLL_TO_CORRECTION } from '../vars';

import cn from '../cn';
require('./radio_theme_alfa-on-color.css');
require('./radio_theme_alfa-on-white.css');
require('./radio.css');

/**
 * Компонент радио-кнопки.
 */
@cn('radio')
@performance()
class Radio extends FeatherComponent {
    static propTypes = {
        /** Тип */
        type: Type.oneOf(['normal', 'button']),
        /** Управление состоянием вкл/выкл компонента */
        checked: Type.bool,
        /** Управление возможностью изменения состояние 'checked' компонента */
        disabled: Type.bool,
        /** Уникальное имя радио-кнопки */
        name: Type.string,
        /** Значение радио-кнопки, которое будет отправлено на сервер, если она выбрана */
        value: Type.string,
        /** Текст подписи к радио-кнопке */
        text: Type.node,
        /** Управление шириной кнопки для типа 'button'. При значении 'available' растягивает кнопку на ширину родителя */
        width: Type.oneOf(['default', 'available']),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Отображение в состоянии ошибки */
        error: Type.bool,
        /** Последовательность перехода между контролами при нажатии на Tab */
        tabIndex: Type.number,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик изменения значения 'checked' компонента, принимает на вход isChecked и value компонента */
        onChange: Type.func,
        /** Обработчик фокуса комнонента */
        onFocus: Type.func,
        /** Обработчик снятия фокуса с компонента */
        onBlur: Type.func,
        /** Обработчик события наведения курсора на радио-кнопку */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с радио-кнопки */
        onMouseLeave: Type.func
    };

    static defaultProps = {
        size: 'm',
        tabIndex: 0
    };

    state = {
        focused: false,
        hovered: false,
        checked: false
    };

    label;
    control;

    render(cn) {
        let checked = this.props.checked !== undefined
            ? this.props.checked
            : this.state.checked;

        return (
            <label
                className={ cn({
                    size: this.props.size,
                    disabled: this.props.disabled,
                    checked,
                    focused: this.state.focused,
                    hovered: this.state.hovered,
                    invalid: !!this.props.error,
                    width: this.props.type === 'button' ? this.props.width : null
                }) }
                tabIndex={ this.props.tabIndex }
                onFocus={ this.handleFocus }
                onBlur={ this.handleBlur }
                onMouseEnter={ this.handleMouseEnter }
                onMouseLeave={ this.handleMouseLeave }
                ref={ (label) => { this.label = label; } }
            >
                {
                    this.props.type === 'button'
                    ? this.renderButtonCheckBox(cn, checked)
                    : this.renderNormalCheckBox(cn, checked)
                }
            </label>
        );
    }

    renderNormalCheckBox(cn, checked) {
        return (
            <div>
                <span className={ cn('box') }>
                    <input
                        checked={ checked }
                        disabled={ this.props.disabled }
                        name={ this.props.name }
                        value={ this.props.value }
                        autoComplete='off'
                        tabIndex='-1'
                        type='radio'
                        className={ cn('control') }
                        ref={ (control) => { this.control = control; } }
                        onChange={ this.handleChange }
                    />
                </span>
                { this.props.text &&
                    <span
                        className={ cn('text') }
                        role='presentation'
                    >
                        { this.props.text }
                    </span>
                }
            </div>
        );
    }

    renderButtonCheckBox(cn, checked) {
        return (
            <div>
                <Button
                    togglable='check'
                    checked={ checked }
                    disabled={ this.props.disabled }
                    size={ this.props.size }
                    width={ this.props.width }
                    focused={ this.state.focused }
                    hovered={ this.state.hovered }
                    tabIndex={ this.props.tabIndex }
                    view={ checked ? 'action' : undefined }
                    onClick={ this.handleChange }
                >
                    { this.props.icon }
                    {
                        this.props.text
                            ? this.props.text
                            : ''
                    }
                </Button>
                <input
                    checked={ checked }
                    disabled={ this.props.disabled }
                    name={ this.props.name }
                    value={ this.props.value }
                    autoComplete='off'
                    tabIndex='-1'
                    type='radio'
                    className={ cn('control') }
                    ref={ (control) => { this.control = control; } }
                    onChange={ this.handleChange }
                />
            </div>
        );
    }

    /**
     * Устанавливает фокус на радио-кнопку.
     *
     * @public
     */
    focus() {
        this.control.focus();
    }

    /**
     * Убирает фокус с радио-кнопки.
     *
     * @public
     */
    blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    }

    /**
     * Скроллит страницу до радио-кнопки.
     *
     * @public
     */
    scrollTo() {
        let elementRect = this.label.getBoundingClientRect();

        scrollTo({
            targetY: elementRect.top + window.pageYOffset - SCROLL_TO_CORRECTION
        });
    }

    @autobind
    handleChange() {
        if (!this.props.disabled) {
            let nextCheckedValue = !(
                this.props.checked !== undefined
                    ? this.props.checked
                    : this.state.checked
            );

            this.setState({ checked: nextCheckedValue });

            if (this.props.onChange) {
                this.props.onChange(this.props.value, nextCheckedValue);
            }
        }
    }

    @autobind
    handleFocus(event) {
        if (!this.props.disabled) {
            this.setState({ focused: true });
        }

        event.target.value = this.props.value;

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    }

    @autobind
    handleBlur(event) {
        if (!this.props.disabled) {
            this.setState({ focused: false });
        }

        event.target.value = this.props.value;

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    @autobind
    handleMouseEnter(event) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    }

    @autobind
    handleMouseLeave(event) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    }
}

export default Radio;
