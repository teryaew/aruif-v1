import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

let sharedContainer = null;

/**
 * Тестовая обертка.
 *
 * @typedef {Object} TestWrapper
 * @property {React.Component} instance Ссылка на экземпляр React компонента.
 * @property {Node} node Корневой HTML узел компонента.
 * @property {Node} container HTML узел контейнера, в котором отрендерен компонент.
 */

/**
 * Рендерит компонент в настоящий DOM, возвращает тестовую обертку.
 *
 * @param {Object} jsx JSX для рендера.
 * @param {Object} [options] Опции для рендера.
 * @param {String} [options.css] Стили для рендер контейнера.
 * @param {Node} [options.container] Контейнер, в который нарендерить компонент.
 * @returns {TestWrapper}
 */
export function render(jsx, options = {}) {
    let container = options.container;

    if (!container) {
        if (!sharedContainer) {
            sharedContainer = document.createElement('div');
            document.body.appendChild(sharedContainer);
        }
        container = sharedContainer;
    }

    if (options.css) {
        container.setAttribute('style', options.css);
    }

    let instance = ReactDOM.render(jsx, container);

    return {
        instance,
        node: ReactDOM.findDOMNode(instance),
        container
    };
}

/**
 * Очищает содержимое DOM после тестов.
 */
export function cleanUp() {
    if (sharedContainer) {
        sharedContainer = null;
    }

    while (document.body.childNodes.length > 0) {
        document.body.removeChild(document.body.firstChild);
    }
}

/**
 * Симулирует событие на HTML узле.
 *
 * @param {Node} node HTML узел, на котором необходимо сгенерить событие.
 * @param {String} event Тип события.
 * @param {*} [data] Данные для прокисрования в событие.
 */
export function simulate(node, event, data) {
    TestUtils.Simulate[event](node, data);
}
