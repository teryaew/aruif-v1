import { render, cleanUp } from '../test-utils';

import AppMenu from './app-menu';

describe('app-menu', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let appMenu = render(<AppMenu>AppMenu-test</AppMenu>);

        expect(appMenu.node).to.exist;
        expect(appMenu.node).to.have.text('AppMenu-test');
    });
});
