import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('../app/app.css');

/**
 * Компонент меню страницы.
 * Обычно используется совместно с компонентом `Page`.
 *
 * @example
 * ```javascript
 * import Page from 'arui-feather/src/page/page';
 * import Header from 'arui-feather/src/header/header';
 * import Footer from 'arui-feather/src/footer/footer';
 *
 * import AppTitle from 'arui-feather/src/app-title/app-title';
 * import AppMenu from 'arui-feather/src/app-menu/app-menu';
 * import AppContent from 'arui-feather/src/app-content/app-content';
 *
 * import Heading from 'arui-feather/src/heading/heading';
 * import Menu from 'arui-feather/src/menu/menu';
 * import Paragraph from 'arui-feather/src/paragraph/paragraph';
 *
 * <Page header={ <Header /> } footer={ <Footer /> }>
 *     <AppTitle>
 *         <Heading>Заголовок страницы</Heading>
 *     </AppTitle>
 *     <AppMenu>
 *         <Menu />
 *     </AppMenu>
 *     <AppContent>
 *         <Paragraph>Контент страницы...</Paragraph>
 *     </AppContent>
 * </Page>
 * ```
 */
@cn('app')
@performance()
class AppMenu extends FeatherComponent {
    static propTypes = {
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        return (
            <div className={ cn('menu') }>
                <div className={ cn('menu-case') }>
                    <div className={ cn('menu-content') }>
                        { this.props.children }
                    </div>
                </div>
            </div>
        );
    }
}

export default AppMenu;
