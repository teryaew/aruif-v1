import createFragment from 'react-addons-create-fragment';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./checkbox-group.css');

/**
 * Компонент группы чекбоксов.
 *
 * @example
 * ```
 * import 'CheckBox' from 'arui-feather/src/checkbox/checkbox';
 * import 'CheckBoxGroup' from 'arui-feather/src/checkbox-group/checkbox-group';
 *
 * // Вертикальная группа чекбоксов
 * <CheckBoxGroup>
 *    <CheckBox text="Раз" />
 *    <CheckBox text="Два" />
 *    <CheckBox text="Три" />
 * </CheckBoxGroup>
 *
 * // Горизонтальная группа чекбоксов, состоящая из обычных кнопок
 * <CheckBoxGroup type="button">
 *    <CheckBox type="button" text="Раз" />
 *    <CheckBox type="button" text="Два" />
 *    <CheckBox type="button" text="Три" />
 * </CheckBoxGroup>
 *
 * // Горизонтальная группа чекбоксов
 * <CheckBoxGroup type="line">
 *    <CheckBox text="Раз" />
 *    <CheckBox text="Два" />
 *    <CheckBox text="Три" />
 * </CheckBoxGroup>
 * ```
 */
@cn('checkbox-group')
@performance()
class CheckBoxGroup extends FeatherComponent {
    static propTypes = {
        /** Тип компонента */
        type: Type.oneOf(['normal', 'button', 'line']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        let children = this.props.children
            ? this.props.children.length
                ? this.props.children
                : [this.props.children]
            : null;

        let checkboxGroupParts = {};

        if (children) {
            children.forEach((checkbox, index) => {
                checkboxGroupParts[`checkbox-${index}`] =
                    (this.props.type !== 'button' && this.props.type !== 'line')
                        ? <div>{ checkbox }</div>
                        : checkbox;
            });
        }

        return (
            <span
                role='group'
                className={ cn({
                    type: this.props.type
                }) + ' control-group' }
            >
                { createFragment(checkboxGroupParts) }
            </span>
        );
    }
}

export default CheckBoxGroup;
