import { render, cleanUp } from '../test-utils';

import Copyright from './copyright';

describe('copyright', function () {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let copyright = render(<Copyright />);

        expect(copyright.node).to.exist;
        expect(copyright.node).to.have.class('copyright');
    });
});
