import { PropTypes as Type } from 'react';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';

import Link from '../link/link';
import FeatherComponent from '../feather/feather';

import performance from '../performance';

import cn from '../cn';
require('./copyright.css');

/**
 * Компонент копирайта: отображает данные о лицензии Альфа-Банка.
 */
@cn('copyright')
@performance()
class Copyright extends FeatherComponent {
    static propTypes = {
        /** Дополнительный класс */
        className: Type.any,
        /** Локализация компонента */
        locale: Type.string,
        /** Отображение годов */
        showYears: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white'])
    };

    static defaultProps = {
        locale: 'en',
        showYears: false
    };

    render(cn) {
        return (
            <div className={ cn }>
                { this.props.children || this.renderDefaultCopyright(cn) }
            </div>
        );
    }

    renderDefaultCopyright(cn) {
        return (
            <span>
                {
                    this.props.showYears &&
                    <span className={ cn('years') }>
                        © 2001—2017
                        <span>&nbsp;</span>
                        <Link
                            size='xs'
                            url='https://alfabank.ru/'
                            text={
                                <FormattedMessage
                                    id='copyright.linkText'
                                    defaultMessage='Alfa-Bank'
                                />
                            }
                        />
                        <span className={ cn('dot') }>.&nbsp;</span>
                        <br />
                    </span>
                }
                <FormattedHTMLMessage
                    id='copyright.title'
                    defaultMessage={
                        'General license of the Bank of Russia No. 3124'
                    }
                />
            </span>
        );
    }
}

export default Copyright;
