import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';
import Link from '../link/link';

import cn from '../cn';
require('./social.css');

/**
 * Компонент отображающий "иконостас" социальных сетей.
 * По-умолчанию отображает следующие социальные сети: Vk, Facebook, Twitter.
 */
@cn('social')
class Social extends FeatherComponent {
    static propTypes = {
        /** Список социальных сетей */
        networks: Type.arrayOf(Type.shape({
            name: Type.string,
            url: Type.string
        })),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        networks: [
            { name: 'vk', url: 'https://vk.com/alfabank' },
            { name: 'facebook', url: 'https://facebook.com/alfabank' },
            { name: 'twitter', url: 'https://twitter.com/alfabank' }
        ]
    };

    render(cn) {
        return (
            <I18n locale={ this.props.locale }>
                <div className={ cn }>
                    <div className={ cn('list') }>
                        {
                            this.props.networks.map(network => (
                                <Link className={ cn('item') } url={ network.url } key={ network.name }>
                                    <Icon
                                        className={ cn('icon') }
                                        network={ network.name }
                                    />
                                </Link>
                            ))
                        }
                    </div>
                    <div className={ cn('info') }>
                        <Link
                            size='s'
                            url='//alfabank.ru/internet/socmedia'
                            text={
                                <FormattedMessage
                                    id='social.info'
                                    defaultMessage='Social media'
                                />
                            }
                        />
                    </div>
                </div>
            </I18n>
        );
    }
}

export default Social;
