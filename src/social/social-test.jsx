import { render, cleanUp } from '../test-utils';

import Social from './social';

const NETWORK = {
    name: 'vk',
    url: 'https://vk.com/alfabank'
};

describe('social', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let social = render(<Social />);

        expect(social.node).to.exist;
    });

    it('should render three social links by default', () => {
        let social = render(<Social />);
        let socialListNode = social.node.querySelectorAll('.social__item');

        expect(socialListNode.length).to.equal(3);
    });

    it('should render one social link', () => {
        let social = render(<Social networks={ [NETWORK] } />);
        let socialListNode = social.node.querySelectorAll('.social__item');

        expect(socialListNode.length).to.equal(1);
    });

    it('should render Link with url & network Icon inside social__item', () => {
        let social = render(<Social networks={ [NETWORK] } />);
        let socialItem = social.node.querySelector('.social__item');
        let socialIcon = socialItem.firstElementChild;

        expect(socialItem.tagName).to.equal('A');
        expect(socialItem).to.have.attr('href', NETWORK.url);
        expect(socialIcon).to.have.class('icon');
        expect(socialIcon).to.have.class(`icon_network_${NETWORK.name}`);
    });
});
