import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import Copyright from '../copyright/copyright';
import FeatherComponent from '../feather/feather';
import Social from '../social/social';

import performance from '../performance';

import cn from '../cn';
require('./footer.css');

/**
 * Компонент подвала сайта.
 * Обычно используется совместно с компонентом `Page`.
 *
 * @example
 * ```javascript
 * import Page from 'arui-feather/src/page/page';
 * import Header from 'arui-feather/src/header/header';
 * import Footer from 'arui-feather/src/footer/footer';
 *
 * <Page header={ <Header /> } footer={ <Footer /> }>
 *     Контент страницы...
 * </Page>
 * ```
 */
@cn('footer')
@performance()
class Footer extends FeatherComponent {
    static propTypes = {
        /** Локализация компонента */
        locale: Type.string,
        /** Меню в подвале */
        menu: Type.node,
        /** Дополнительный текст */
        additional: Type.node,
        /** Содержимое блока соц. сетей */
        social: Type.node,
        /** Отображение блока соц. сетей */
        showSocial: Type.bool,
        /** Содержимое блока копирайта */
        copyright: Type.node,
        /** Отображение годов в копирайте */
        showYears: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    static defaultProps = {
        additional: (
            <FormattedMessage
                id='footer.additional'
                defaultMessage='Made in Alfa-Laboratory'
            />
        ),
        showSocial: true,
        showYears: false
    };

    render(cn) {
        return (
            <I18n locale={ this.props.locale }>
                <div className={ cn }>
                    <div className={ cn('inner') }>
                        {
                            this.props.menu &&
                            <div className={ cn('menu') }>
                                { this.props.menu }
                            </div>
                        }
                        <div className={ cn('additional') }>
                            { this.props.additional }
                        </div>
                        <div className={ cn('info') }>
                            {
                                this.props.showSocial &&
                                <div className={ cn('social') }>
                                    { this.props.social || <Social /> }
                                </div>
                            }
                            <div className={ cn('copyright') }>
                                { this.props.copyright || <Copyright showYears={ this.props.showYears } /> }
                            </div>
                        </div>
                    </div>
                </div>
            </I18n>
        );
    }
}

export default Footer;
