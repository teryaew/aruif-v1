import { autobind } from 'core-decorators';
import React from 'react';

import AutosubmitInput from '../autosubmit-input/autosubmit-input';
import Label from '../label/label';
import Modal from '../modal/modal';
import Spin from '../spin/spin';

import { keyboardCode } from '../lib/keyboard';

import cn from '../cn';
require('./confirmation-modal.css');

@cn('confirmation-modal')
class ConfirmationModal extends React.Component {
    static propTypes = {
        /** Управляет видимостью компонента */
        visible: React.PropTypes.bool,
        /** Управляет доступностью инпута для ввода */
        disabled: React.PropTypes.bool,
        /** Отображение попапа с ошибкой в момент когда фокус находится в поле ввода */
        error: React.PropTypes.string,
        /** Количество символов, после ввода которых будет вызван onInputFinished */
        desiredChars: React.PropTypes.number,
        /** Управление отображением спина */
        isProcessing: React.PropTypes.bool,
        /** Показывать ли крестик */
        hasCloser: React.PropTypes.bool,
        /** Заголовок окна подтверждения */
        header: React.PropTypes.node,
        /** Футер окна */
        footer: React.PropTypes.node,
        /** Обработчик закрытия модального окна */
        onAbortClick: React.PropTypes.func,
        /** Обработчик изменения значения в инпуте  */
        onInputChange: React.PropTypes.func,
        /** Обработчик фокуса на инпут  */
        onInputFocus: React.PropTypes.func,
        /** Обработчик потери фокуса на инпуте  */
        onInputBlur: React.PropTypes.func,
        /** Обработчик завершения ввода */
        onInputFinished: React.PropTypes.func
    };

    static defaultProps = {
        isProcessing: false,
        visible: false,
        disabled: false,
        hasCloser: true
    };

    _input = null;

    componentDidMount() {
        if (this.props.visible && !this.props.disabled) {
            this._input.focus();
        }
    }

    componentDidUpdate(prevProps) {
        if (
            (this.props.visible && !prevProps.visible)
            || (this.props.visible && this.props.error && !prevProps.error)
            || (this.props.visible && !this.props.disabled && prevProps.disabled)
        ) {
            // focus in timeout because modal will re-render input after this update
            this._focusTimeout = setTimeout(() => this._input.focus(), 0);
        }
    }

    componentWillUnmount() {
        this._focusTimeout && clearTimeout(this._focusTimeout);
    }

    render(cn) {
        return (
            <Modal
                hasCloser={ this.props.hasCloser }
                onClose={ this.handleClose }
                visible={ this.props.visible }
                className={ cn }
                autoclosable={ false }
            >
                <div className={ cn('body') }>
                    <div className={ cn('header') }>
                        <Label size='l'>
                            { this.props.header }
                        </Label>
                    </div>
                    <AutosubmitInput
                        className={ cn('input') }
                        ref={ input => { this._input = input; } }
                        desiredChars={ this.props.desiredChars }
                        onFinished={ this.handleInputFinished }
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyDown={ this.handleInputKeyDown }
                        disabled={ this.props.disabled }
                        error={ this.props.error }
                        size='xxl'
                        errorDirections={ ['bottom-center'] }
                    />
                    <Spin size='l' visible={ this.props.isProcessing } className={ cn('spin') } />
                    <div className={ cn('footer') }>{ this.props.footer }</div>
                </div>
            </Modal>
        );
    }

    @autobind
    handleClose() {
        this.props.onAbortClick && this.props.onAbortClick();
    }

    @autobind
    handleInputChange(value, event) {
        this.props.onInputChange && this.props.onInputChange(value, event);
    }

    @autobind
    handleInputFinished(value) {
        this.props.onInputFinished && this.props.onInputFinished(value);
    }

    @autobind
    handleInputFocus(event) {
        event.preventDefault();
        document.body.scrollTop = 0;

        this.props.onInputFocus && this.props.onInputFocus(event);
    }

    @autobind
    handleInputBlur(event) {
        this.props.onInputBlur && this.props.onInputBlur(event);
    }

    @autobind
    handleInputKeyDown(event) {
        if (event.keyCode === keyboardCode.ENTER) {
            this.props.onInputFinished && this.props.onInputFinished(event.target.value);
        }
    }
}

export default ConfirmationModal;
