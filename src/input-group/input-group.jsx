import createFragment from 'react-addons-create-fragment';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./input-group.css');
require('./input-group_theme_alfa-on-white.css');

/**
 * Компонент группы полей для текстового ввода.
 *
 * @example
 * ```
 * import 'Input' from 'arui-feather/src/input/input';
 * import 'InputGroup' from 'arui-feather/src/input-group/input-group';
 *
 * // Группа полей для ввода
 * <InputGroup>
 *    <Input />
 *    <Input />
 *    <Input />
 * </InputGroup>
 *
 * // Группа полей для ввода, растягивающаяся на всю ширину
 * <InputGroup width='available'>
 *    <Input />
 *    <Input />
 *    <Input />
 * </InputGroup>
 * ```
 */
@cn('input-group')
@performance()
class InputGroup extends FeatherComponent {
    static propTypes = {
        /** Управление возможностью компонента занимать всю ширину родителя */
        width: Type.oneOf(['default', 'available']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    state = {
        focused: false
    };

    render(cn) {
        let children = this.props.children
            ? this.props.children.length
            ? this.props.children
            : [this.props.children]
            : null;

        let inputGroupParts = {};

        if (children) {
            React.Children.forEach(children, (input, index) => {
                input = React.cloneElement(input, {
                    width: this.props.width
                });

                inputGroupParts[`input-${index}`] = (
                    <span
                        className={ cn('input-case', {
                            invalid: !!input.props.error,
                            disabled: input.props.disabled
                        }) }
                    >
                        { input }
                    </span>
                );
            });
        }

        return (
            <span
                role='group'
                className={ cn({
                    width: this.props.width
                }) + ' control-group' }
                tabIndex='-1'
            >
                { createFragment(inputGroupParts) }
            </span>
        );
    }
}

export default InputGroup;
