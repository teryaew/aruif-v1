import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';

require('./link.css');

/**
 * Компонент ссылки.
 */
@cn('link')
@performance()
class Link extends FeatherComponent {
    static propTypes = {
        /** Отображение как второстепенная информация */
        minor: Type.bool,
        /** Иконка ссылки */
        icon: Type.node,
        /** Текст ссылки */
        text: Type.node,
        /** href ссылки */
        url: Type.string,
        /** target ссылки */
        target: Type.oneOf(['_self', '_blank', '_parent', '_top']),
        /** Последовательность перехода между контролами при нажатии на Tab */
        tabIndex: Type.number,
        /** Управление возможностью клика по ссылке */
        disabled: Type.bool,
        /** Псевдо-ссылка (border-bottom: dotted) */
        pseudo: Type.bool,
        /** Управление подчеркиванием ссылки */
        underlined: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['xs', 's', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик клика но ссылке */
        onClick: Type.func,
        /** Обработчик фокуса компонента */
        onFocus: Type.func,
        /** Обработчик снятия фокуса компонента */
        onBlur: Type.func,
        /** Обработчик события наведения курсора на ссылку */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с ссылки */
        onMouseLeave: Type.func
    };

    static defaultProps = {
        size: 'm',
        url: '#',
        tabIndex: 0,
        disabled: false,
        pseudo: false,
        minor: false,
        underlined: true
    };

    state = {
        hovered: false,
        focused: false
    };

    root;

    render(cn) {
        return (
            <a
                href={ this.props.url }
                className={ cn({
                    disabled: this.props.disabled,
                    pseudo: this.props.pseudo,
                    underlined: this.props.underlined,
                    size: this.props.size,
                    focused: this.state.focused,
                    hovered: this.state.hovered,
                    minor: this.props.minor
                }) }
                tabIndex={ this.props.tabIndex }
                target={ this.props.target }
                onClick={ this.handleClick }
                onFocus={ this.handleFocus }
                onBlur={ this.handleBlur }
                onMouseEnter={ this.handleMouseEnter }
                onMouseLeave={ this.handleMouseLeave }
                ref={ (root) => { this.root = root; } }
            >
                { this.props.children }
                { this.props.icon &&
                    <span className={ cn('icon') }>
                        { this.props.icon }
                    </span>
                }
                { this.props.text &&
                    <span className={ cn('text') }>
                        { this.props.text }
                    </span>
                }
            </a>
        );
    }

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */
    getNode() {
        return this.root;
    }

    /**
     * Ставит фокус на ссылку.
     *
     * @public
     */
    focus() {
        this.root.focus();
    }

    /**
     * Убирает фокус с ссылки.
     *
     * @public
     */
    blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    }

    @autobind
    handleClick(event) {
        if (!!this.props.pseudo) {
            event.preventDefault();
        }
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    @autobind
    handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    }

    @autobind
    handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    @autobind
    handleMouseEnter(event) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    }

    @autobind
    handleMouseLeave(event) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    }
}

export default Link;
