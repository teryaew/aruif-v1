const ObjectAssign = require('es6-object-assign');
const ArrayFind = require('array.prototype.find');
const ArrayFrom = require('array-from');
const ObjectIs = require('object-is');
require('es6-promise');
require('es6-weak-map'); // for autobind from core-decorators
require('array.prototype.fill');
require('ima-babel6-polyfill'); // fix super constructor call for ie < 10, see https://phabricator.babeljs.io/T3041

if (typeof window !== 'undefined') {
    require('matches-selector-polyfill/dist/matches-selector-polyfill.js');
    require('matchmedia-polyfill'); // window.matchMedia polyfill for ie 9
    require('matchmedia-polyfill/matchMedia.addListener.js'); // addListener polyfill for ie 9
    require('raf').polyfill(); // window.requestAnimationFrame for ie < 10 & android 4.0..4.3

    if ('performance' in window === false) {
        window.performance = {};
    }

    // ie 9 has window.performance, but not window.performance.now
    if ('now' in window.performance === false) {
        window.performance.now = require('performance-now');
    }

    require('classlist-polyfill');
}

if (!Object.assign) {
    ObjectAssign.polyfill();
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: ArrayFind.getPolyfill()
    });
}

if (!Object.is) {
    Object.is = ObjectIs;
}

if (!Array.from) {
    Array.from = ArrayFrom;
}
