// partly based on fbjs/lib/shallowEqual

/**
 * Функции проверки равенства двух объектов.
 *
 * @param {*} objA первый объект
 * @param {*} objB второй объект
 * @param {Boolean} [deep=false] запускать ли глубокую проверку равенства
 * @returns {Boolean}
 */
export function isEqual(objA, objB, deep = false) {
    // if objects are equal we don't need to run deep check
    if (Object.is(objA, objB)) {
        return true;
    }

    if (typeof objA !== 'object' || objA === null || typeof objB !== 'object' || objB === null) {
        return false;
    }

    // prototypes on object must be the same
    if (objA.prototype !== objB.prototype) {
        return false;
    }

    const keysA = Object.keys(objA);
    const keysB = Object.keys(objB);

    // If object has different number of keys, they are definitely not equal
    if (keysA.length !== keysB.length) {
        return false;
    }

    // Check every key equality
    while (keysA.length > 0) {
        const key = keysA.pop();
        // check that both objects have this property
        if (!objB.hasOwnProperty(key)) {
            return false;
        }

        const a = objA[key];
        const b = objB[key];

        if (!Object.is(a, b)) {
            // we don't need run deep check for not equal primitives
            if (!deep || typeof a !== 'object' || typeof b !== 'object' || a === null || b === null) {
                return false;
            }

            if (a._isAMomentObject && b._isAMomentObject) {
                // moment.js dates is deep equal when they are cloned, even if date in one is already changed,
                // so compare them with moment built-in function
                // also moment.js object can has different keys length, even if they has same value
                if (!a.isSame(b)) {
                    return false;
                }
            } else if (!isEqual(a, b, deep)) {
                return false;
            }
        }
    }

    return true;
}

/**
 * "Поверхностная" проверка равенства props и state компонента.
 *
 * @param {*} nextProps next component props
 * @param {*} nextState next component state
 * @param {*} nextContext next component context
 * @returns {Boolean}
 */
function shallow(nextProps, nextState, nextContext) {
    return !isEqual(this.props, nextProps)
        || !isEqual(this.state, nextState)
        || !isEqual(this.context, nextContext);
}

/**
 * Запускает глубокую проверку равенства props и state компонента.
 * Глубокая проверка менее производительна, но позволяет проверять равенство массивов и объектов.
 *
 * @param {*} nextProps next component props
 * @param {*} nextState next component state
 * @param {*} nextContext next component context
 * @returns {Boolean}
 */
function deep(nextProps, nextState, nextContext) {
    return !isEqual(this.props, nextProps, true)
        || !isEqual(this.state, nextState, true)
        || !isEqual(this.context, nextContext, true);
}

/**
 * Декоратор для улучшения производительности React компонентов. Работает за счет реализации метода
 * [shouldComponentUpdate](https://facebook.github.io/react/docs/advanced-performance.html#avoiding-reconciling-the-dom).
 *
 * У декоратора есть два режима работы - глубокая и "поверхностная" проверка. В случае, если все props и state
 * компонента состоит только из примитивных значений (`number`, `string`, `null`, `undefined`) стоит использовать
 * поверхностную проверку, которая будет проверять простое равенство значений в `props` и `state`.

 * В случае, если props или state компонентов имеют сложную структуру (массивы, объекты) необходимо использовать
 * глубокую проверку.
 *
 * @param {Boolean} [useDeep=false] Использовать глубокую проверку равенства
 * @returns {Function}
 */
export default function performance(useDeep = false) {
    return function (target) {
        target.prototype.shouldComponentUpdate = useDeep ? deep : shallow;
    };
}
