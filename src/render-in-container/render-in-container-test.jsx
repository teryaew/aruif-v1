import { render, cleanUp } from '../test-utils';

import RenderInContainer from './render-in-container';

describe('render-in-container', () => {
    afterEach(cleanUp);

    it('should place children in body when `container` prop not set', () => {
        let renderInContainer = render(
            <RenderInContainer>
                <div>Render-test</div>
            </RenderInContainer>
        );

        let contentNode = renderInContainer.instance.getNode();

        expect(renderInContainer.node).not.to.exist;
        expect(contentNode.parentNode).to.equal(document.body);
    });


    describe('with custom container', () => {
        function getRenderContainer() {
            return document.getElementById('render_container');
        }

        beforeEach(() => {
            let renderContainerNode = document.createElement('div');
            renderContainerNode.setAttribute('id', 'render_container');
            document.body.appendChild(renderContainerNode);
        });

        afterEach(() => {
            let renderContainerNode = getRenderContainer();
            document.body.removeChild(renderContainerNode);
            cleanUp();
        });

        it('should render empty component and place it in container set via `container` prop', function () {
            let renderInContainer = render(
                <RenderInContainer container={ getRenderContainer() }>
                    <div>Render-test</div>
                </RenderInContainer>
            );
            let contentNode = renderInContainer.instance.getNode();
            let containerNode = renderInContainer.instance.getContainer();

            expect(renderInContainer.node).not.to.exist;
            expect(contentNode.parentNode).to.equal(getRenderContainer());
            expect(containerNode).to.equal(getRenderContainer());
        });
    });

    it('should call `onRender` callback after element was rendered', () => {
        let onRender = chai.spy();

        render(
            <RenderInContainer onRender={ onRender }>
                <div>Render-test</div>
            </RenderInContainer>
        );

        expect(onRender).to.have.been.called.once;
    });
});
