import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';
import ReactDOM from 'react-dom';

import FeatherComponent from '../feather/feather';
import { HtmlElement } from '../lib/prop-types';

/**
 * Компонент, позволяющий визуализировать другие компоненты в произвольном контейнере.
 */
class RenderInContainer extends FeatherComponent {
    static propTypes = {
        /** Callback на рендер компонента */
        onRender: Type.func,
        /** Дополнительный класс */
        className: Type.any,
        /** Контейнер, в котором будет визуализирован компонент */
        container: HtmlElement
    };

    /**
     * Возвращает HTMLElement враппера компонента.
     *
     * @public
     * @returns {HTMLElement}
     */
    getNode() {
        return this.element;
    }

    /**
     * Возвращает HTMLElement контейнера, в который отрендерился компонент.
     *
     * @public
     * @returns {HTMLElement}
     */
    getContainer() {
        return this.props.container || document.body;
    }

    componentDidMount() {
        this.buildElements();
        this.renderLayer();
    }

    buildElements() {
        if (this.element && this.container) {
            ReactDOM.unmountComponentAtNode(this.element);
            this.container.removeChild(this.element);
        }

        this.container = this.getContainer();
        this.element = document.createElement('div');

        this.container.appendChild(this.element);
    }

    componentDidUpdate() {
        if (this.container !== this.getContainer()) {
            this.buildElements();
        }
        this.renderLayer();
    }

    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.element);
        this.container.removeChild(this.element);
    }

    renderLayer() {
        if (this.props.className) {
            this.element.className = this.props.className.toString();
        }

        ReactDOM.unstable_renderSubtreeIntoContainer(
            this,
            this.props.children,
            this.element,
            this.handleRender
        );
    }

    render() {
        return false;
    }

    @autobind
    handleRender() {
        if (this.props.onRender) {
            this.props.onRender();
        }
    }
}

export default RenderInContainer;
