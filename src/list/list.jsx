import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./list.css');

/**
 * Компонент списка.
 */
@cn('list')
@performance(true)
class List extends FeatherComponent {
    static propTypes = {
        /** Список элементов */
        items: Type.arrayOf(Type.shape({
            /** Уникальный ключ элемента */
            key: Type.string.isRequired,
            /** Содержание элемента */
            value: Type.node.isRequired
        })),
        /** Тип списка */
        type: Type.oneOf(['default', 'ordered']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        let listElement = ((this.props.type !== 'ordered') ? 'ul' : 'ol');

        let listContent = (this.props.items || []).map((item) => (
            <li
                key={ 'item-' + item.key }
                className={ cn('item') }
            >
                { item.value }
            </li>
        ));

        let listProps = {
            className: cn({
                type: this.props.type
            })
        };

        return React.createElement(
            listElement,
            listProps,
            listContent
        );
    }
}

export default List;
