import { render, cleanUp } from '../test-utils';

import Highlight from './highlight';

describe('highlight', () => {
    afterEach(cleanUp);

    it('should render without problems', () => {
        let highlight = render(<Highlight>Notice me</Highlight>);

        expect(highlight.node).to.exist;
        expect(highlight.node).to.have.text('Notice me');
    });
});
