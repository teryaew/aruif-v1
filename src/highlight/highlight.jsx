import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./highlight.css');

/**
 * Компонент подсветки текста. Используйте его, чтобы выделить текст на странице.
 * Текст необходимо передать в виде дочерних компонентов.
 *
 * @example
 * ```javascript
 * Слоган <Highlight>«Найдётся всё»</Highlight> придумали в 2000 году.
 * ```
 */
@cn('highlight')
@performance()
class Highlight extends FeatherComponent {
    static propTypes = {
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        return (
            <span className={ cn() }>
                { this.props.children }
            </span>
        );
    }
}

export default Highlight;
