import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';
import ReactDOM from 'react-dom';

import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';
import Link from '../link/link';
import Logo from '../logo/logo';
import Menu from '../menu/menu';

import Popup from '../popup/popup';
import ThemeProvider from '../theme-provider/theme-provider';

import performance from '../performance';

import cn from '../cn';
require('./header.css');

/**
 * Компонент шапки сайта: лого, меню и пользовательский профиль.
 * Обычно используется совместно с компонентом `Page`.
 *
 * @example
 * ```javascript
 * import Page from 'arui-feather/src/page/page';
 * import Header from 'arui-feather/src/header/header';
 * import Footer from 'arui-feather/src/footer/footer';
 *
 * <Page header={ <Header /> } footer={ <Footer /> }>
 *     Контент страницы...
 * </Page>
 * ```
 */
@cn('header')
@performance()
class Header extends FeatherComponent {
    static propTypes = {
        /** Контент для меню в попапе; TODO @teryaew: rewrite */
        popupMenuContent: Type.array,
        /** Корневая ссылка */
        root: Type.string,
        /** Содержимое меню в шапке */
        menu: Type.node,
        /** Содержимое элемента справки */
        help: Type.node,
        /** Содержимое элемента пользователя */
        user: Type.node,
        /** Содержимое элемента контактов поддержки */
        support: Type.node,
        /** Произвольный контент над логотипом и меню */
        topContent: Type.node,
        /** Управление возможностью фиксирования шапки к верхнему краю окна */
        fixed: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик события клика по логотипу Альфа-Банк */
        onLogoClick: Type.func,
        /** Обработчик события клика по названию */
        onTitleClick: Type.func
    };

    static defaultProps = {
        fixed: false,
        root: '/'
    };

    state = {
        fixed: false,
        colored: false,
        isMenuPopupVisible: false
    };

    height;
    contentHeight;

    root;
    topContent;

    componentDidMount() {
        if (this.refs.menuPopup) {
            this.refs.menuPopup.setTarget(ReactDOM.findDOMNode(this.refs.menuIcon));
        }

        if (this.props.fixed) {
            window.addEventListener('scroll', this.handleScroll);
        }
    }

    componentWillUnmount() {
        if (this.props.fixed) {
            window.removeEventListener('scroll', this.handleScroll);
        }
    }

    render(cn) {
        return (
            <div className={ cn({ fixed: this.state.fixed }) } ref={ (root) => { this.root = root; } }>
                { this.props.topContent &&
                    <div className={ cn('top-content') } ref={ (topContent) => { this.topContent = topContent; } }>
                        { this.props.topContent }
                    </div>
                }
                <div className={ cn('main-case', { fixed: this.state.fixed, colored: this.state.colored }) }>
                    <div className={ cn('inner') }>
                        <Link
                            className={ cn('logo') }
                            url={ this.props.root }
                            onClick={ this.handleLogoClick }
                        >
                            <Logo theme='alfa-on-color' size='s' />
                        </Link>
                        {
                            this.props.title &&
                            <Link
                                className={ cn('title') }
                                url={ this.props.root }
                                onClick={ this.handleTitleClick }
                            >
                                { this.props.title }
                            </Link>
                        }
                        {
                            this.props.help
                        }
                        {
                            this.props.menu &&
                            <div className={ cn('menu') }>
                                <div
                                    ref='menuIcon'
                                    className={ cn('menu-icon') }
                                    onClick={ this.handleMenuIconClick }
                                >
                                    <Icon tool='help' size='m' />
                                </div>
                                <div className={ cn('menu-content') }>
                                    { this.props.menu }
                                </div>
                                <ThemeProvider theme='alfa-on-white'>
                                    <Popup
                                        ref='menuPopup'
                                        autoclosable={ true }
                                        directions={ ['bottom-right'] }
                                        size='l'
                                        type='default'
                                        visible={ this.state.isMenuPopupVisible }
                                        onClickOutside={ this.handleClickOutsidePopup }
                                    >
                                        <Menu
                                            view='popup'
                                            size='l'
                                            content={ this.props.popupMenuContent }
                                        />
                                    </Popup>
                                </ThemeProvider>
                            </div>
                        }
                        {
                            this.props.user &&
                            <div className={ cn('user') }>
                                { this.props.user }
                            </div>
                        }
                        {
                            this.props.support &&
                            <div className={ cn('support') }>
                                <Link
                                    className={ cn('support-icon') }
                                    url='tel:88002000000'
                                >
                                    <Icon action='call' size='m' />
                                </Link>
                                <div className={ cn('support-content') }>
                                    { this.props.support }
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }

    @autobind
    handleClickOutsidePopup() {
        this.setState({
            isMenuPopupVisible: false
        });
    }

    @autobind
    handleMenuIconClick() {
        this.setState({
            isMenuPopupVisible: !this.state.isMenuPopupVisible
        });
    }

    @autobind
    handleScroll() {
        this.solveFixedColoredState();
    }

    @autobind
    handleLogoClick(event) {
        if (this.props.onLogoClick) {
            this.props.onLogoClick(event);
        }
    }

    recountHeightStyleState() {
        let topContentHeight = (this.topContent && this.topContent.offsetHeight) || 0;
        let headerHeight = this.root.offsetHeight;

        if (this.props.onResize
            && (headerHeight !== this.height || topContentHeight !== this.contentHeight)) {
            this.props.onResize(headerHeight, topContentHeight);
        }

        this.contentHeight = topContentHeight;
        this.height = headerHeight;

        if (this.props.fixed) {
            this.solveFixedColoredState();
        }
    }

    @autobind
    handleTitleClick(event) {
        if (this.props.onTitleClick) {
            this.props.onTitleClick(event);
        }
    }

    solveFixedColoredState() {
        let y = window.pageYOffset;
        let topDataContainer = this.topContent;
        let positionFixedBreakpoint = !this.props.topContent
            ? 0
            : topDataContainer.offsetHeight;

        let positionColoredBreakpoint = !this.props.topContent
            ? 10
            : topDataContainer.offsetHeight;

        if (y >= positionFixedBreakpoint) {
            if (!this.state.fixed) {
                this.setState({
                    fixed: true
                });
            }
        } else if (this.state.fixed) {
            this.setState({
                fixed: false
            });
        }

        if (y >= positionColoredBreakpoint) {
            if (!this.state.colored) {
                this.setState({
                    colored: true
                });
            }
        } else if (this.state.colored) {
            this.setState({
                colored: false
            });
        }
    }
}

export default Header;
