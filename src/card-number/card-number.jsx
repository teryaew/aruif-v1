import createFragment from 'react-addons-create-fragment';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./card-number.css');

/**
 * Компонент для отображения номера банковской карты.
 * Маскирует карту, если передать все 16 цифр номера карты.
 */
@cn('card-number')
@performance()
class CardNumber extends FeatherComponent {
    static propTypes = {
        /** Номер карты */
        value: Type.oneOfType([Type.string, Type.number]),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        return (
            <span
                className={ cn() }
            >
                { this.renderNumber(cn) }
            </span>
        );
    }

    renderNumber(cn) {
        let value = this.props.children || this.props.value;
        if (value) {
            let splittedValue = value.replace(/ /g, '').split(/\*+/);

            if (splittedValue.length === 1) {
                // not masked value
                let number = splittedValue[0];
                if (number.length >= 16) {
                    let numberBlockList = [];
                    for (let i = 0; i < 3; i++) {
                        numberBlockList.push(number.substring(4 * i, 4 * (i + 1)));
                    }
                    numberBlockList.push(number.substring(12, number.length));
                    return numberBlockList.join(' ');
                }

                // to short for card number
                return number;
            }

            // masked value
            if (splittedValue[0]) {
                splittedValue[0] = splittedValue[0].trim().substring(0, 4);
            }

            if (splittedValue[1]) {
                splittedValue[1] = splittedValue[1].trim().substring(0, 4);
            }

            let dot = this.renderDot(cn);
            return createFragment({
                cardNumberStart: splittedValue[0],
                dot1: dot,
                dot2: dot,
                dot3: dot,
                dot4: dot,
                cardNumberEnd: splittedValue[1]
            });
        }

        return undefined;
    }

    renderDot(cn) {
        return (
            <span className={ cn('dot') } />
        );
    }
}

export default CardNumber;
