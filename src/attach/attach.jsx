import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import { FormattedMessage } from 'react-intl';
import I18n from '../i18n/i18n';

import FeatherComponent from '../feather/feather';
import Button from '../button/button';
import Input from '../input/input';

import performance from '../performance';

import cn from '../cn';
require('./attach.css');

/**
 * Компонент прикрепления файлов
 */
@cn('attach')
@performance()
class Attach extends FeatherComponent {
    static propTypes = {
        /** Содержимое поля ввода, указанное по умолчанию */
        value: Type.string,
        /** Уникальное имя блока */
        name: Type.string,
        /** Последовательность перехода между контролами при нажатии на Tab */
        tabIndex: Type.number,
        /** Локализация компонента */
        locale: Type.string,
        /** Текст для случая, когда файл не загружен */
        noFileText: Type.oneOfType([Type.node, Type.string]),
        /** Содержимое кнопки для выбора файла */
        buttonContent: Type.node,
        /** Свойства для кнопки */
        buttonProps: Type.object,
        /** Управление возможностью изменения значения компонента */
        disabled: Type.bool,
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик клика по компоненту кнопки */
        onClick: Type.func,
        /** Обработчик изменения значения 'value' */
        onChange: Type.func,
        /** Обработчик клика по крестику, сбрасываещему значение 'value' */
        onClearClick: Type.func,
        /** Обработчик фокуса компонента */
        onFocus: Type.func,
        /** Обработчик снятия фокуса компонента */
        onBlur: Type.func,
        /** Обработчик события наведения курсора на кнопку */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с кнопки */
        onMouseLeave: Type.func
    };

    static defaultProps = {
        buttonProps: {},
        buttonContent: (
            <FormattedMessage
                id='attach.buttonContent'
                defaultMessage='Choose a file'
            />
        ),
        size: 'm',
        disabled: false,
        noFileText: (
            <FormattedMessage
                id='attach.noFileText'
                defaultMessage='No file'
            />
        )
    };

    state = {
        focused: false,
        hovered: false,
        value: ''
    };

    input;

    render(cn) {
        return (
            <I18n locale={ this.props.locale }>
                <span
                    className={ cn({
                        size: this.props.size,
                        disabled: this.props.disabled,
                        hovered: this.state.hovered,
                        focused: this.state.focused
                    }) }
                    ref={ (root) => { this.root = root; } }
                    onMouseEnter={ this.handleMouseEnter }
                    onMouseLeave={ this.handleMouseLeave }
                >
                    { this.renderButton(cn) }
                    { this.renderStatusText(cn) }
                </span>
            </I18n>
        );
    }

    renderButton(cn) {
        let buttonProps = {
            ...this.props.buttonProps,
            className: cn('button'),
            disabled: this.props.disabled,
            size: this.props.size,
            focused: this.state.focused
        };

        return (
            <Button
                { ...buttonProps }
                tag='span'
                leftAddons={
                    <Input
                        ref={ (input) => { this.input = input; } }
                        name={ this.props.name }
                        tabindex={ this.props.tabIndex }
                        className={ cn('control') }
                        size={ this.props.size }
                        type='file'
                        disabled={ this.props.disabled }
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleFocus }
                        onBlur={ this.handleBlur }
                        value={ this.props.value || this.state.value }
                    />
                }
                onClick={ this.handleButtonClick }
            >
                { this.props.buttonContent }
            </Button>
        );
    }

    renderStatusText(cn) {
        let inputValue = this.props.value !== undefined ? this.props.value : this.state.value;
        let fileName = this.extractFileNameFromPath(inputValue);

        let fileBlock = (
            <div className={ cn('file') }>
                <span className={ cn('text') }>
                    { fileName }
                </span>
                <span
                    className={ cn('clear') }
                    onClick={ this.handleClearClick }
                />
            </div>
        );

        let noFileBlock = (
            <div className={ cn('no-file') }>
                { this.props.noFileText }
            </div>
        );

        return fileName ? fileBlock : noFileBlock;
    }

    /**
     * @public
     */
    focus() {
        this.input.focus();
    }

    /**
     * @public
     */
    blur() {
        this.input.blur();
    }

    @autobind
    handleInputChange(value) {
        this.setState({ value });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    @autobind
    handleClearClick() {
        this.setState({
            value: ''
        });

        if (this.props.onClearClick) {
            this.props.onClearClick();
        }
    }

    extractFileNameFromPath(path) {
        return path.split('\\').pop();
    }

    @autobind
    handleButtonClick() {
        if (this.props.onClick) {
            this.props.onClick();
        }
    }

    @autobind
    handleFocus() {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus();
        }
    }

    @autobind
    handleBlur() {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur();
        }
    }

    @autobind
    handleMouseEnter() {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter();
        }
    }

    @autobind
    handleMouseLeave() {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave();
        }
    }
}

export default Attach;
