import { render, cleanUp } from '../test-utils';

import Icon from './icon';

describe('icon', () => {
    afterEach(cleanUp);

    it('renders without problems', () => {
        let icon = render(<Icon>icon-content</Icon>);

        expect(icon.node).to.have.text('icon-content');
    });

    (function () {
        let iconType = 'action';
        let iconValues = ['check', 'error', 'fail', 'ok', 'repeat', 'down', 'left', 'right', 'up'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, () => {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'format';
        let iconValues = ['1c', 'csv', 'default', 'doc', 'pdf', 'png', 'ppt', 'sketch', 'svg', 'txt', 'xls', 'xml'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, () => {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'bank';
        let iconValues = ['244', '256', '285', '351', '404', '439', '1309', '1415', '1490', '1516', '2377', '2449',
            '3001', '3308', '4267', '4924', '5030', '5475', '6415', '7311', '7686', '7687', '8967', '9908', '10223'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'card';
        let iconValues = ['maestro', 'mastercard', 'visa', 'visaelectron', 'belkart'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'currency';
        let iconValues = ['rub', 'chf', 'eur', 'gbp', 'jpy', 'usd'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'tool';
        let iconValues = ['close', 'calendar', 'email', 'help', 'helpfilled', 'info',
            'payment_rounded_plus', 'payment_plus', 'printer', 'search'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'network';
        let iconValues = ['vk', 'facebook', 'twitter'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'user';
        let iconValues = ['body', 'logout'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    (function () {
        let iconType = 'category';
        let iconValues = ['appliances', 'auto', 'books_movies', 'business', 'charity',
            'dress', 'education', 'entertainment', 'family', 'finance', 'forgot',
            'gasoline', 'gibdd_fines', 'health', 'hobby', 'housekeeping', 'investments',
            'loans', 'medicine', 'mobile_internet', 'mortgage', 'other', 'person',
            'pets', 'rent', 'repairs', 'restaurants', 'shopping', 'tax_fines',
            'transport', 'travel', 'user'];

        return iconValues.forEach(value => (
            it(`render ${iconType} - ${value} icon without problems`, function () {
                let props = { [`${iconType}`]: value };
                let icon = render(<Icon { ...props } />);

                expect(icon.node).to.have.class(`icon_${iconType}_${value}`);
            })
        ));
    }());

    it('should call `onClick` callback after icon was clicked', function () {
        let onClick = chai.spy();
        let icon = render(<Icon onClick={ onClick } />);

        icon.node.click();

        expect(onClick).to.have.been.called.once;
    });
});
