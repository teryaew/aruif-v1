import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import performance from '../performance';

import cn from '../cn';
require('./icon.css');

require('./icon_action_alfa-on-color.css');
require('./icon_action_alfa-on-white.css');
require('./icon_action_alfa-on-colored.css');

require('./icon_bank_alfa-on-color.css');
require('./icon_bank_alfa-on-white.css');
require('./icon_bank_alfa-on-colored.css');

require('./icon_card_alfa-on-color.css');
require('./icon_card_alfa-on-white.css');
require('./icon_card_alfa-on-colored.css');

require('./icon_currency_alfa-on-color.css');
require('./icon_currency_alfa-on-white.css');

require('./icon_format_alfa-on-color.css');
require('./icon_format_alfa-on-white.css');

require('./icon_network_alfa-on-color.css');
require('./icon_network_alfa-on-white.css');

require('./icon_tool_alfa-on-color.css');
require('./icon_tool_alfa-on-white.css');

require('./icon_user_alfa-on-color.css');
require('./icon_user_alfa-on-white.css');

require('./icon_category_alfa-on-color.css');

/**
 * Компонент иконки. Содержит в себе пресеты самых популярных иконок.
 *
 * С компонентов иконки идет набор иконок, разбитый по группам:
 *
 * * `action` - иконки различный действий
 * * `bank` - логотипы банков
 * * `card` - логотипы платежных систем
 * * `format` - иконки форматов файлов
 * * `currency` - иконки валют
 * * `tool` - иконки инструментов
 * * `network` - иконки социальных сетей
 * * `user` - иконки имеющие отношение к пользователю
 * * `category` - иконки платежных категорий
 *
 * Все наборы иконок идут в двух цветовых темах `alfa-on-color` и `alfa-on-white`.
 *
 * Для набора иконок `action`, `bank` и `card`, также есть цветной вариант иконок,
 * реализуемый темой `alfa-on-colored`.
 *
 * По-умолчанию в финальную сборку вашего проекта попадают все наборы иконок во всех
 * цветовых схемах. Если вы хотите облегчить сборку и исключить неиcпользуемый набор
 * иконок, то используйте plugin `webpack.NormalModuleReplacementPlugin` из стандартной
 * поставки `webpack` с пакетом `node-noop`.
 *
 * ```
 *     npm i --save-dev node-noop
 * ```
 *
 * ```
 *    // Примеры:
 *
 *    webpackConfig.plugins.push(
 *         // Исключает из сборки иконки действий для белого фона
 *         new webpack.NormalModuleReplacementPlugin(/icon_action_alfa-on-white\.css$/, 'node-noop'),
 *
 *         // Исключает из сборки иконки действий для цветного фона
 *         new webpack.NormalModuleReplacementPlugin(/icon_action_alfa-on-colored\.css$/, 'node-noop')
 *    );
 * ```
 */
@cn('icon')
@performance()
class Icon extends FeatherComponent {
    static propTypes = {
        /** Тип иконки */
        action: Type.oneOf([
            'call', 'check', 'error', 'fail', 'more', 'ok', 'ok_filled', 'repeat', 'down', 'left', 'right', 'up'
        ]),
        /** Тип иконки */
        bank: Type.oneOf([
            '244', '256', '285', '351', '404', '439', '1309', '1415', '1490', '1516', '2377', '2449', '3001', '3308',
            '4267', '4924', '5030', '5475', '6415', '7311', '7686', '7687', '8967', '9908', '10223'
        ]),
        /** Тип иконки */
        card: Type.oneOf(['maestro', 'mastercard', 'visa', 'visaelectron', 'belkart']),
        /** Тип иконки */
        format: Type.oneOf(['1c', 'csv', 'default', 'doc', 'pdf', 'png', 'ppt', 'sketch', 'svg', 'txt', 'xls', 'xml']),
        /** Тип иконки */
        currency: Type.oneOf(['rub', 'chf', 'eur', 'gbp', 'jpy', 'usd']),
        /** Тип иконки */
        tool: Type.oneOf(['atm', 'close', 'calendar', 'email', 'help', 'helpfilled', 'info', 'mobile', 'office',
            'payment_plus', 'payment_rounded_plus', 'printer', 'search', 'site']),
        /** Тип иконки */
        network: Type.oneOf(['vk', 'facebook', 'twitter']),
        /** Тип иконки */
        user: Type.oneOf(['body', 'logout']),
        /** Тип иконки */
        category: Type.oneOf(['appliances', 'auto', 'books_movies', 'business', 'charity', 'dress',
            'education', 'entertainment', 'family', 'finance', 'forgot', 'gasoline', 'gibdd_fines', 'health', 'hobby',
            'housekeeping', 'investments', 'loans', 'medicine', 'mobile_internet', 'mortgage', 'other', 'person',
            'pets', 'rent', 'repairs', 'restaurants', 'shopping', 'tax_fines', 'transport', 'travel', 'user']),
        /** Размер компонента */
        size: Type.oneOf(['s', 'm', 'l', 'xl', 'xxl']),
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white', 'alfa-on-colored']),
        /** Дополнительный класс */
        className: Type.any,
        /** Обработчик события наведения курсора на иконку */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с иконки */
        onMouseLeave: Type.func,
        /** Обработчик клика по иконке */
        onClick: Type.func
    };

    static defaultProps = {
        size: 'm'
    };

    render(cn) {
        return (
            <span
                className={ cn({
                    size: this.props.size,
                    action: this.props.action,
                    format: this.props.format,
                    bank: this.props.bank,
                    card: this.props.card,
                    currency: this.props.currency,
                    tool: this.props.tool,
                    network: this.props.network,
                    user: this.props.user,
                    category: this.props.category
                }) }
                onMouseEnter={ this.handleMouseEnter }
                onMouseLeave={ this.handleMouseLeave }
                onClick={ this.handleClick }
            >
                { this.props.children }
            </span>
        );
    }

    @autobind
    handleMouseEnter(e) {
        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(e);
        }
    }

    @autobind
    handleMouseLeave(e) {
        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(e);
        }
    }

    @autobind
    handleClick(e) {
        if (this.props.onClick) {
            this.props.onClick(e);
        }
    }
}

export default Icon;
