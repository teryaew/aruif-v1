import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import IsolatedContainer from '../isolated-container/isolated-container';

import { HtmlElement } from '../lib/prop-types';

import cn from '../cn';
require('./popup-container.css');

/**
 * Становится родительским элементом для всех дочерних блоков Popup.
 * Предполагается задавать этому элементу `position:fixed` в стилях.
 *
 * @example
 * ```javascript
 * import PopupContainerProvider from 'arui-feather/src/popup-container-provider/popup-container-provider';
 * import Popup from 'arui-feather/src/popup/popup';
 * import Page from 'arui-feather/src/page/page';
 *
 *  <Page>
 *     <PopupContainerProvider
 *         style={
 *             {
 *                 position: 'fixed',
 *                 top: 0,
 *                 right: 0,
 *                 bottom: 0,
 *                 width: '400px',
 *                 overflow: 'auto'
 *             }
 *         }
 *     >
 *         <Popup>
 *             Попап отрендерился в PopupContainerProvider, а не в body
 *             При скролле внутри блока, попап ездит вместе с остальным контентом
 *         </Popup>
 *     </PopupContainerProvider>
 *  </Page>
 * ```
 */
@cn('popup-container')
class PopupContainerProvider extends FeatherComponent {
    static childContextTypes = {
        isInCustomContainer: Type.bool,
        renderContainerElement: HtmlElement,
        positioningContainerElement: HtmlElement
    };

    static propTypes = {
        /** Дополнительный класс */
        className: Type.any,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Объект с css-стилями */
        style: Type.object
    };

    state = {
        didRender: false
    };

    getChildContext() {
        return {
            isInCustomContainer: true,
            renderContainerElement: (this.renderContainer && this.renderContainer.getNode()),
            positioningContainerElement: this.positioningContainer
        };
    }

    componentDidMount() {
        this.handleContainerDidRender();
    }

    render(cn) {
        if (this.props.children && this.props.children.length > 1) {
            throw new Error('You can provide only one child element to <PopupContainerProvider />');
        }

        return (
            <div
                style={ this.props.style }
                className={ cn }
                ref={
                    positioningContainer => { this.positioningContainer = positioningContainer; }
                }
            >
                {
                    this.props.children
                        ? React.cloneElement(this.props.children)
                        : null
                }
                <IsolatedContainer
                    ref={
                        renderContainer => { this.renderContainer = renderContainer; }
                    }
                />
            </div>
        );
    }

    /**
     * Необходимо для обновления childContext сразу после получения refs.
     */
    handleContainerDidRender() {
        this.setState({
            didRender: true
        });
    }
}

export default PopupContainerProvider;
