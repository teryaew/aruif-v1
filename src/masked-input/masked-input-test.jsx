import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import MaskedInput from './masked-input';

function getContainer() {
    return document.getElementById('container');
}

function renderMaskedInput(component) {
    return ReactDOM.render(component, getContainer());
}

describe('masked-input', () => {
    beforeEach(() => {
        let domContainerNode = document.createElement('div');
        domContainerNode.setAttribute('id', 'container');
        document.body.appendChild(domContainerNode);
    });

    afterEach(() => {
        let domContainerNode = getContainer();
        document.body.removeChild(domContainerNode);
    });

    it('should render without problems', () => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        expect(maskedInputNode).to.exist;
        expect(maskedInputNode).to.match('input');
    });

    it('should format unformatted value', () => {
        let maskedInput = renderMaskedInput(
            <MaskedInput mask='1111 1111 1111 1111' value='1234567890123456' />
        );
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);
        expect(maskedInputNode.value).to.equal('1234 5678 9012 3456');
    });

    it('should strip value size to mask size', () => {
        let maskedInput = renderMaskedInput(
            <MaskedInput mask='1111 1111 1111 1111' value='1234 5678 9012 3456 7890 1234' />
        );
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);
        expect(maskedInputNode.value).to.equal('1234 5678 9012 3456');
    });

    it('should format value skipping non maskable chars', () => {
        let maskedInput = renderMaskedInput(
            <MaskedInput mask='1111 1111 1111 1111' value='Abc 1234 @&&*() 5678 klm90123456' />
        );
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);
        expect(maskedInputNode.value).to.equal('1234 5678 9012 3456');
    });

    it('should reformat value by new mask after mask was changed', () => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' value='1234567890123456' />);

        maskedInput = renderMaskedInput(<MaskedInput mask='+1 (111) 111-11-11' value='1234567890123456' />);

        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);
        expect(maskedInputNode.value).to.equal('+1 (234) 567-89-01');
    });

    it('should reformat value after value was changed', () => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' value='1234567890123456' />);

        maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' value='0987654321098765' />);

        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);
        expect(maskedInputNode.value).to.equal('0987 6543 2109 8765');
    });

    it('should focus on input after focus() call', (done) => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            expect(document.activeElement === maskedInputNode).to.be.equal(true);
            done();
        }, 0);
    });

    it('should remove focus from input after blur() call', (done) => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            maskedInput.blur();
            expect(document.activeElement !== maskedInputNode).to.be.equal(true);
            done();
        }, 0);
    });

    it('should call `onProcessInputEvent` during `onInput` process', (done) => {
        let onProcessInputEvent = chai.spy();
        let maskedInput = renderMaskedInput(
            <MaskedInput
                mask='1111 1111 1111 1111'
                value='1234 5'
                onProcessInputEvent={ onProcessInputEvent }
            />
        );
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            TestUtils.Simulate.beforeInput(maskedInputNode);
            TestUtils.Simulate.input(maskedInputNode, { target: { value: '1234 5' } });

            expect(onProcessInputEvent).to.have.been.called.once;

            done();
        }, 0);
    });

    it('should move caret position from uneditable position to next editable position during `onInput`',
    (done) => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' value='1234 5' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            maskedInputNode.selectionStart = 4;
            maskedInputNode.selectionEnd = 4;

            TestUtils.Simulate.beforeInput(maskedInputNode);
            TestUtils.Simulate.input(maskedInputNode, { target: { value: '1234 5' } });

            expect(maskedInputNode.selectionStart).to.equal(5);
            expect(maskedInputNode.selectionEnd).to.equal(5);

            done();
        }, 0);
    });

    it('should move caret position from uneditable position to prev editable position during `onInput` (delete)',
    (done) => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='1111 1111 1111 1111' value='1234 5' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            maskedInputNode.selectionStart = 5;
            maskedInputNode.selectionEnd = 5;

            TestUtils.Simulate.beforeInput(maskedInputNode);
            TestUtils.Simulate.input(maskedInputNode, { target: { value: '1234 ' } });

            expect(maskedInputNode.selectionStart).to.equal(4);
            expect(maskedInputNode.selectionEnd).to.equal(4);

            done();
        }, 0);
    });

    it('should move caret position from uneditable position to next editable position during `onInput` (replace)',
    (done) => {
        let maskedInput = renderMaskedInput(<MaskedInput mask='+1 111 11 11 11' value='+7 903 752' />);
        let maskedInputNode = ReactDOM.findDOMNode(maskedInput);

        maskedInput.focus();

        setTimeout(() => {
            maskedInputNode.selectionStart = 2;
            maskedInputNode.selectionEnd = 5;

            TestUtils.Simulate.beforeInput(maskedInputNode);
            TestUtils.Simulate.input(maskedInputNode, { target: { value: '+7 84' } });

            expect(maskedInputNode.selectionStart).to.equal(3);
            expect(maskedInputNode.selectionEnd).to.equal(3);

            done();
        }, 0);
    });
});
