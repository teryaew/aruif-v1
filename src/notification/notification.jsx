import { autobind } from 'core-decorators';
import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Icon from '../icon/icon';
import Message from '../message/message';

import { deprecated } from '../lib/prop-types';
import { isEventOutsideClientBounds } from '../lib/window';
import performance from '../performance';

import cn from '../cn';
require('./notification.css');

/**
 * Компонент всплывающего окна.
 */
@cn('notification')
@performance()
class Notification extends FeatherComponent {
    static propTypes = {
        /** Тип компонента */
        status: Type.oneOf(['error', 'fail', 'ok']),
        /** Управление видимостью компонента */
        visible: Type.bool,
        /** Используйте `offset` взамен. Отступ [x, y] от верхнего левого угла viewport (window), если stickTo="left", от правого верхнего, если stickTo="right" */
        position: deprecated(Type.arrayOf(Type.number), 'Use `offset` instead'),
        /** Отступ от верхнего края */
        offset: Type.number,
        /** К какому краю прижат попап */
        stickTo: Type.oneOf(['left', 'right']),
        /** Управляет отображением кнопки закрытия уведомления */
        hasCloser: Type.bool,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any,
        /** Заголовок сообщения */
        title: Type.node,
        /** Замена стандартной иконки */
        icon: Type.node,
        /** Время до закрытия компонента */
        autoCloseDelay: Type.number,
        /** Управление возможностью закрытия компонента по клику вне его */
        outsideClickClosable: Type.bool,
        /** Обработчик события истечения времени до закрытия компонента */
        onCloseTimeout: Type.func,
        /** Обработчик клика по крестику компонента */
        onCloserClick: Type.func,
        /** Используйте `onCloserClick`. Обработчик клика по крестику компонента */
        onCloseClick: deprecated(Type.func),
        /** Обработчик события наведения курсора на попап */
        onMouseEnter: Type.func,
        /** Обработчик события снятия курсора с попапа */
        onMouseLeave: Type.func,
        /** Обработчик клика вне компонента */
        onClickOutside: Type.func,
        /** Обработчик клика по компоненту */
        onClick: Type.func
    };

    static defaultProps = {
        autoCloseDelay: 5000,
        stickTo: 'left',
        offset: 0,
        position: [0, 0],
        hasCloser: true
    };

    state = {
        hovered: false
    };

    root;

    closeTimeout = null;
    clickEventBindTimeout = null;
    isWindowClickBinded = false;

    componentDidMount() {
        this.startCloseTimer();
        this.props.outsideClickClosable && this.ensureClickEvent();
    }

    componentWillUnmount() {
        this.stopCloseTimer();
        this.props.outsideClickClosable && this.ensureClickEvent(true);
    }

    componentDidUpdate(prevProps) {
        if (this.props.outsideClickClosable) {
            if (prevProps.onClickOutside !== this.props.onClickOutside) {
                this.ensureClickEvent();
            } else {
                if (prevProps.visible !== this.props.visible) {
                    this.ensureClickEvent(!this.props.visible);
                }
            }
        }
    }

    render(cn) {
        return (
            <div
                className={ cn({
                    visible: this.props.visible,
                    status: this.props.status,
                    hovered: this.state.hovered,
                    'stick-to': this.props.stickTo
                }) }
                onMouseEnter={ this.handleMouseEnter }
                onMouseLeave={ this.handleMouseLeave }
                onClick={ this.handleClick }
                style={ this.getPosition() }
                ref={ (root) => { this.root = root; } }
            >
                <div className={ cn('icon') }>
                {
                    this.props.icon ||
                    <Icon
                        theme='alfa-on-colored'
                        action={ this.props.status }
                        size='m'
                    />
                }
                </div>
                { this.props.title &&
                    <div className={ cn('title') }>
                        { this.props.title }
                    </div>
                }
                <div className={ cn('content') }>
                    <Message className={ cn('message') }>
                        { this.props.children }
                    </Message>
                </div>
                { this.props.hasCloser &&
                    <Icon
                        className={ cn('close') }
                        size='m'
                        tool='close'
                        onClick={ this.handleCloserClick }
                    />
                }
            </div>
        );
    }

    @autobind
    handleCloserClick() {
        let onCloserClick = this.props.onCloserClick || this.props.onCloseClick;

        if (onCloserClick) {
            onCloserClick();
        }
    }

    @autobind
    handleMouseEnter(event) {
        this.setState({ hovered: true });
        this.stopCloseTimer();

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    }

    @autobind
    handleMouseLeave(event) {
        this.setState({ hovered: false });
        this.stopCloseTimer();
        this.startCloseTimer();

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    }

    @autobind
    handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    @autobind
    handleWindowClick(event) {
        if (this.props.outsideClickClosable && this.root &&
            isEventOutsideClientBounds(event, this.root)) {
            if (this.props.onClickOutside) {
                this.props.onClickOutside(event);
            }
        }
    }

    getPosition() {
        return {
            top: this.props.offset || this.props.position[1]
        };
    }

    startCloseTimer() {
        this.closeTimeout = setTimeout(() => {
            if (this.props.onCloseTimeout) {
                this.props.onCloseTimeout();
            }
        }, this.props.autoCloseDelay);
    }

    stopCloseTimer() {
        clearTimeout(this.closeTimeout);
        this.closeTimeout = null;
    }

    ensureClickEvent(isDestroy) {
        let isNeedBindEvent = isDestroy !== undefined ? !isDestroy : this.props.visible;

        // We need timeouts to not to catch the event that causes
        // popup opening (because it propagates to the `window`).
        if (this.clickEventBindTimeout) {
            clearTimeout(this.clickEventBindTimeout);
            this.clickEventBindTimeout = null;
        }

        this.clickEventBindTimeout = setTimeout(() => {
            if (!this.isWindowClickBinded && isNeedBindEvent) {
                window.addEventListener('click', this.handleWindowClick);
                this.isWindowClickBinded = true;
            } else if (this.isWindowClickBinded && !isNeedBindEvent) {
                window.removeEventListener('click', this.handleWindowClick);
                this.isWindowClickBinded = false;
            }
        }, 0);
    }
}

export default Notification;
