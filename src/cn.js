import { PropTypes as Type } from 'react';
import bem from 'bem-cn-fast';

function getTheme(props, context) {
    return props.theme || context.theme || 'alfa-on-color';
}

function getFunctionCn(className, theme, cn) {
    let resultCn = function () {
        let blockSelector = false;
        let args = Array.prototype.slice.call(arguments, 0);

        if (typeof args[0] === 'undefined' || typeof args[0] === 'object') {
            if (args.length === 0) {
                args.push({});
            }

            args[0].theme = theme;
            blockSelector = true;
        }

        return cn.apply(cn, args) + ((blockSelector && className) ? ` ${className}` : '');
    };

    resultCn.toString = function () {
        return cn({ theme }).toString() + (className ? ` ${className}` : '');
    };

    return resultCn;
}

/**
 * Декорирует `React.Component`, изменяя метод `render`, так, что он
 * принимает первым аргументов экземпляр объекта `cn`, который может
 * использован для генерации имен классов БЭМ сущностей.
 *
 * Добавляет внешний prop `theme`, который рендерится
 * соотвествующим css-классом в верстке.
 *
 * Добавляет внешний prop `className`, который может
 * быть использован для добавления дополнительного класса.
 *
 * @example
 * ```
 *     \@cn('my-block')
 *     class MyBlock extends FeatherComponent {
 *          render(cn) {
 *              return (
 *                  <div className={ cn } />
 *              );
 *          }
 *     }
 * ```
 *
 * Результат рендера компонента:
 *
 * ```
 *     <MyBlock />
 *     // <div class="my-block my-block_theme_alfa-on-color"></div>
 *
 *     <MyBlock theme="alfa-on-white" />
 *     // <div class="my-block my-block_theme_alfa-on-white"></div>
 *
 *     <MyBlock className="additional-class" />
 *     // <div class="my-block my-block_theme_alfa-on-color additional-class"></div>
 * ```
 *
 * Также `cn` декоратор реализует dependency injection,
 * что позволяет гибко менять исходную композицию компонента.
 *
 * ```
 * // phone-input.jsx
 *
 * import Input from 'input';
 * import './input.css';
 *
 * // Исходных компонент, использующий БЭМ наименование `phone-input` и
 * // использующий внутри себя стандартный компонент `Input`.
 * \@cn('phone-input', Input)
 * class PhoneInput extends React.Component {
 *     render(cn, Input) {
 *          return <Input className={ cn } />;
 *     }
 * }
 *
 * // my-phone-input.jsx
 *
 * import PhoneInput from 'phone-input';
 * import MyInput from 'my-input';
 * import './my-phone-input.css';
 *
 * // Отнаследованный компонент, использующий БЭМ наименование `my-phone-input` и
 * // используйющий внутри себя кастомный компонент `MyInput`.
 * \@cn('my-phone-input', MyInput)
 * class MyPhoneInput extends PhoneInput {}
 * ```
 *
 * @param {String} componentName Имя css-класса компонента
 * @param {...Function} [components] Набор компонентов для инъекции в рендер.
 * @returns {Function}
 */
function cn(componentName, ...components) {
    return function (target) {
        target._cn = bem(componentName);
        target._cnComponents = components;

        if (target.prototype.render.length >= 1) {
            target.contextTypes = {
                ...target.contextTypes,
                theme: Type.string
            };

            target.childContextTypes = {
                ...target.childContextTypes,
                theme: Type.string
            };

            const originalRender = target.prototype.render;
            target.prototype.render = function () {
                /**
                 * Переполучает `cn` при изменении `this.props.className`.
                 * Необходимо для корректного ререндера компонентов
                 * при передаче `cn` без скобок в `props`:
                 *
                 * ```
                 *     <div className={ cn } />
                 * ```
                 */
                const currentClassName = this.props.className;
                const currentTheme = getTheme(this.props, this.context);

                if (!this._cnArgs
                    || this._oldClassName !== currentClassName
                    || this._oldTheme !== currentTheme
                ) {
                    this._cnArgs = [
                        getFunctionCn(currentClassName, currentTheme, this.constructor._cn),
                        ...this.constructor._cnComponents
                    ];

                    this._oldClassName = currentClassName;
                    this._oldTheme = currentTheme;
                }
                return originalRender.apply(this, this._cnArgs);
            };
        }
    };
}

export default cn;
