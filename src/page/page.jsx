import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import ThemeProvider from '../theme-provider/theme-provider';

import cn from '../cn';

require('./page.css');

/**
 * Компонент страницы.
 * Как правило является корневым компонентов страницы.
 * Обычно используется совместно с компонентами `Header`, `Footer`
 * и компонентами `AppTitle`, `AppMenu` и `AppContent`.
 *
 * @example
 * ```javascript
 * import Page from 'arui-feather/src/page/page';
 * import Header from 'arui-feather/src/header/header';
 * import Footer from 'arui-feather/src/footer/footer';
 *
 * import AppTitle from 'arui-feather/src/app-title/app-title';
 * import AppMenu from 'arui-feather/src/app-menu/app-menu';
 * import AppContent from 'arui-feather/src/app-content/app-content';
 *
 * import Heading from 'arui-feather/src/heading/heading';
 * import Menu from 'arui-feather/src/menu/menu';
 * import Paragraph from 'arui-feather/src/paragraph/paragraph';
 *
 * <Page header={ <Header /> } footer={ <Footer /> }>
 *     <AppTitle>
 *         <Heading>Заголовок страницы</Heading>
 *     </AppTitle>
 *     <AppMenu>
 *         <Menu />
 *     </AppMenu>
 *     <AppContent>
 *         <Paragraph>Контент страницы...</Paragraph>
 *     </AppContent>
 * </Page>
 * ```
 */
@cn('page')
class Page extends FeatherComponent {
    static propTypes = {
        /** Вид */
        view: Type.oneOf(['default', 'text']),
        /** Шапка страницы */
        header: Type.node,
        /** Футер страницы */
        footer: Type.node,
        /** Тема компонента */
        theme: Type.oneOf(['alfa-on-color', 'alfa-on-white']),
        /** Дополнительный класс */
        className: Type.any
    };

    render(cn) {
        return (
            <div className={ cn({ view: this.props.view }) }>
                {
                    this.props.header &&
                    <div className={ cn('header') }>
                        { this.props.header }
                    </div>
                }
                <div className={ cn('content') }>
                    <div className={ cn('content-inner') }>
                        {
                            this.props.view === 'text'
                            ? <ThemeProvider theme='alfa-on-white'>{ this.props.children }</ThemeProvider>
                            : this.props.children
                        }
                    </div>
                </div>
                {
                    this.props.footer && this.props.view !== 'text' &&
                    <div className={ cn('footer') }>
                        <div className={ cn('footer-inner') }>
                            { this.props.footer }
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default Page;
