import { PropTypes as Type } from 'react';

import FeatherComponent from '../feather/feather';
import Input from '../input/input';

import performance from '../performance';

import cn from '../cn';

/**
 * Компонент ввода телефона по маске
 *
 * @extends Input
 */
@cn('phone-input', Input)
@performance()
class PhoneInput extends FeatherComponent {
    static propTypes = {
        /** Маска для текстового поля */
        mask: Type.string,
        /** Подсказка в текстовом поле */
        placeholder: Type.string
    };

    static defaultProps = {
        mask: '+1 111 111 11 11',
        placeholder: '+7 000 000 00 00'
    };

    root;

    render(cn, Input) {
        return (
            <Input
                { ...this.props }
                type='tel'
                ref={ (root) => { this.root = root; } }
                noValidate={ true }
                className={ cn }
            />
        );
    }

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */
    focus() {
        this.root.focus();
    }

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */
    blur() {
        this.root.blur();
    }

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */
    scrollTo() {
        this.root.scrollTo();
    }
}

export default PhoneInput;
