import { PropTypes as Type } from 'react';
import { addLocaleData, IntlProvider } from 'react-intl';

import FeatherComponent from '../feather/feather';

const IS_BROWSER = typeof window !== 'undefined';
const DEFAULT_LOCALE = 'ru';

let navigatorLanguage = IS_BROWSER &&
    (navigator.languages && navigator.languages[0] || navigator.language || navigator.userLanguage);
let language = IS_BROWSER && navigatorLanguage.toLowerCase().split(/[_-]+/)[0];

/**
 * Подгрузка словарей.
 *
 * @param {String} locale Код для обозначения названия языка (ISO 639-1)
 * @returns {Object}
 */
function getLocaleData(locale) {
    addLocaleData(require(`react-intl/locale-data/${locale}`));
    return require(`../../locales/${locale}.json`);
}

class I18n extends FeatherComponent {
    static PropTypes = {
        /** Локализация */
        locale: Type.string,
        /** Сообщения */
        messages: Type.object
    };

    render() {
        return (
            /* TODO: SSR suport for locale @teryaew */
            <IntlProvider
                locale={ this.props.locale || DEFAULT_LOCALE }
                messages={ getLocaleData(this.props.locale || language || DEFAULT_LOCALE) }
            >
                { this.props.children }
            </IntlProvider>
        );
    }
}

export default I18n;
