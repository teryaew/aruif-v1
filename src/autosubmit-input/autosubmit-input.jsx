import { autobind } from 'core-decorators';
import React from 'react';

import Input from '../input/input';

import cn from '../cn';

@cn('autosubmit-input')
class AutosubmitInput extends React.Component {
    static propTypes = {
        /** Количество символов, после ввода onFinished будет вызван */
        desiredChars: React.PropTypes.number.isRequired,
        /** Обработчик изменения ввода */
        onChange: React.PropTypes.func,
        /** Обработчик окончания ввода */
        onFinished: React.PropTypes.func
    };

    _input = null;

    render(cn) {
        const props = {
            ...this.props,
            className: cn(),
            ref: (input) => { this._input = input; },
            maxLength: this.props.desiredChars,
            type: 'tel',
            onChange: this.handleChange
        };

        return <Input {...props} />;
    }

    focus() {
        this._input.focus();
        const input = this._input.getControl();
        input.setSelectionRange(0, input.value.length);
    }

    blur() {
        this._input.blur();
    }

    @autobind
    handleChange(value) {
        this.props.onChange && this.props.onChange(value);
        if (this.props.desiredChars <= value.length && this.props.onFinished) {
            this.props.onFinished(value);
        }
    }
}

export default AutosubmitInput;
