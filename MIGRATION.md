Миграция проектов на основе Babel 5.x и TypeScript
==================================================

Этот гайд поможет вам начать использовать Arui Feather в
ваших проектах на основе Babel 5.x и TypeScript.

Установка ARUI Feather
----------------------

`npm install --save git+ssh://git@git/ef/arui-feather.git`

Обновление Babel
----------------

ARUI Feather использует Babel 6.x, если ваш проект на Babel 5.x,
то вам необходимо будет обвновить Babel до последней стабильной версии.

Удалите из `package.json` пакет `babel` и замените его на последнюю версию `babel-core`:

```
npm install --save-dev babel-core@latest
```

Также добавьте необходимые Babel модуля для сборки ARUI Feather:

```
npm install --save-dev babel-loader@latest
npm install --save-dev babel-plugin-transform-decorators-legacy@latest
npm install --save-dev babel-preset-es2015@latest
npm install --save-dev babel-preset-react@latest
npm install --save-dev babel-preset-stage-0@latest
```

Обновите Webpack загрузчики:

```
npm install --save-dev css-loader@latest
npm install --save-dev ejs-compiled-loader@latest
npm install --save-dev file-loader@latest
npm install --save-dev image-webpack-loader@latest
npm install --save-dev imports-loader@latest
npm install --save-dev json-loader@latest
npm install --save-dev style-loader@latest
npm install --save-dev stylus-loader@latest
npm install --save-dev svg-loader@latest
npm install --save-dev url-loader@latest
npm install --save-dev awesome-typescript-loader@latest
```

Добавьте модули Postcss необходимые для сборки ARUI Feather:

```
npm install --save-dev autoprefixer@latest
npm install --save-dev lost@latest
npm install --save-dev postcss-calc@latest
npm install --save-dev postcss-custom-media@latest
npm install --save-dev postcss-custom-properties@latest
npm install --save-dev postcss-each@latest
npm install --save-dev postcss-for@latest
npm install --save-dev postcss-import@latest
npm install --save-dev postcss-loader@latest
npm install --save-dev postcss-mixins@latest
npm install --save-dev postcss-nested@latest
npm install --save-dev postcss-simple-vars@latest
npm install --save-dev postcss-url@latest
```

Добавьте необходимые плагины в проект:

```
npm install --save-dev webpack-postcss-tools@latest
```

Измените ваш `.babelrc` в проекте на следующий:

```
{
  'presets': ['react', 'es2015', 'stage-0'],
  'plugins': ['transform-decorators-legacy']
}
```

Добавление сборочных настроек ARUI Feather к проекту
----------------------------------------------------

Найдите в вашем `webpack.config.js` секцию с загрузчиками и добавьте туда
загрузчики для ARUI Feather импортировав шаблон загрузчиков ARUI Feather:

```
var FEATHER_WEBPACK_CONFIG = require('arui-feather/webpack.config.template.js');

...

    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loaders: ['react-hot', 'awesome-typescript-loader?' + ATL_OPTIONS]
            }
        ].concat(FEATHER_WEBPACK_CONFIG.module.loaders)
    }

...

```

Добавьте плагины необходимые ARUI Feather:

```

...
    plugins: [
        new ForkCheckerPlugin(),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en-gb/),
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(
                'bower.json',
                ['main']
            )
        )
    ].concat(FEATHER_WEBPACK_CONFIG.plugins)
...


```

Добавьте необходимые плагины для PostCSS:

```

...

    config.postcss = FEATHER_WEBPACK_CONFIG.postcss;

...

```

Не забудьте добавить *.js и *.jsx расширения в резольвер:

```
    resolve: {
        extensions: ['', '.ts', '.tsx', '.webpack.js', '.web.js', '.js', '.jsx'],
    }
```


Обновление сторонних модулей для поддержки Babel 6.x
----------------------------------------------------

В наших проектах часто используются устаревшие версии модулей,
которые работают с Babel 5.x. Убедитесь, что у вас стоят последние версии:

```
npm install --save react@latest
npm install --save react-dom@latest
npm install --save react-redux@latest
npm install --save redux@latest
npm install --save redux-devtools@latest
npm install --save redux-devtools-diff-monitor@latest
npm install --save redux-thunk@latest
```

*Важно!* У Redux новых версий изменились девелоперские тулзы. Вам придется удалить старый код подключения DevTools. Как использовать новую версию можно найти здесь: https://github.com/gaearon/redux-devtools/blob/master/docs/Walkthrough.md

Удаление алиасов для lodash-node
--------------------------------

Пакет `lodash-node` объявлен как `deprecated`. Уберите все алисы `lodash` -> `lodash-node` из вашего кода сборки, если они у вас были:

```
    ...

    resolve: {
        root: [
            path.join(__dirname),
            path.join(__dirname, 'node_modules'),
            path.join(__dirname, 'node_modules/arui-nyc/bower_components'),
            path.join(__dirname, 'node_modules/arui-nyc/node_modules'),
            path.join(__dirname, 'node_modules/arui/node_modules')
        ],
        extensions: ['', '.ts', '.tsx', '.webpack.js', '.web.js', '.js', '.jsx', '.styl'],
        alias: {
            //'lodash': 'lodash-node/modern',
            'tsflux/index': 'tsflux/index.tsx'
        }
    },

    ...
```

Подключение компонентов ARUI Feather в tsx файлах
-------------------------------------------------

```
// import Button from 'arui-nyc/src/button/button';
const Button = require('arui-feather/src/button/button').default;
```

Использование совместно с ARUI + ARUI Nyc
-----------------------------------------

Подключиет версию ARUI и ARUI Nyc с удаленными конфигами для Babel 5.x:

```
npm install --save git+ssh://git@git/ef/arui.git#babel6
npm install --save git+ssh://git@git/ef/arui-nyc.git#babel6
```

Теперь вы можете использовать две библиотеки совместно и осущсвить плавный переход проекта.

Если что-то не получилось
-------------------------

https://alfabank.slack.com/messages/arui-feather/
