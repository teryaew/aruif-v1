<a name="0.11.3"></a>
## 0.11.3 (2017-02-07)


### Bug Fixes

* **inline-error-test:** test name ([5434947](http://git/projects/EF/repos/arui-feather/commits/5434947))
* **test-utils:** jsdoc ([ff0e656](http://git/projects/EF/repos/arui-feather/commits/ff0e656))



<a name="0.11.2"></a>
## 0.11.2 (2017-02-03)


### Bug Fixes

* **input:** after clear doesn't call 'onChange' action ([4ab1caa](http://git/projects/EF/repos/arui-feather/commits/4ab1caa))


### Features

* **icon:** add default document icon ([ca55b63](http://git/projects/EF/repos/arui-feather/commits/ca55b63))
* **input:** use MaskedInput as di ([64765e5](http://git/projects/EF/repos/arui-feather/commits/64765e5))



<a name="0.11.1"></a>
## 0.11.1 (2017-01-31)


### Bug Fixes

* **menu:** fix problems when rendering menu with empty content and mode=radio ([727180a](http://git/projects/EF/repos/arui-feather/commits/727180a))
* **sidebar:** fix sidebar left, right padding in mobile ([511c289](http://git/projects/EF/repos/arui-feather/commits/511c289))


### Features

* add more jsdoc validation rules ([fed6d64](http://git/projects/EF/repos/arui-feather/commits/fed6d64))



<a name="0.11.0"></a>
# 0.11.0 (2017-01-23)


### Features

* **calendar:** get rid of moment.js ([91eabb1](http://git/projects/EF/repos/arui-feather/commits/91eabb1))
* **input:** add setSelectionRange method ([3990a37](http://git/projects/EF/repos/arui-feather/commits/3990a37))



<a name="0.10.8"></a>
## 0.10.8 (2017-01-23)


### Bug Fixes

* **label:** add prop to set display to inline and white-space: no wrap ([657c74c](http://git/projects/EF/repos/arui-feather/commits/657c74c))



<a name="0.10.7"></a>
## 0.10.7 (2017-01-20)


### Features

* **label:** add smaller line-height to Label in case multi line text in it ([4dcf043](http://git/projects/EF/repos/arui-feather/commits/4dcf043))



<a name="0.10.6"></a>
## 0.10.6 (2017-01-18)


### Features

* **select:** add error ([4607fad](http://git/projects/EF/repos/arui-feather/commits/4607fad))



<a name="0.10.5"></a>
## 0.10.5 (2017-01-16)


### Bug Fixes

* **masked-input:** fix broken caret for android < 4.4 ([7267159](http://git/projects/EF/repos/arui-feather/commits/7267159))



<a name="0.10.4"></a>
## 0.10.4 (2017-01-12)


### Bug Fixes

* **mq:** small brakepoint only till 767px (portrait ipad) ([5b2e95a](http://git/projects/EF/repos/arui-feather/commits/5b2e95a))


### Features

* background css color vars ([98a5149](http://git/projects/EF/repos/arui-feather/commits/98a5149))
* unit test to check mask update in money-input ([a06496b](http://git/projects/EF/repos/arui-feather/commits/a06496b))
* update mask if money-input component receive value property ([d2c805e](http://git/projects/EF/repos/arui-feather/commits/d2c805e))



<a name="0.10.3"></a>
## 0.10.3 (2017-01-11)


### Features

* **copyright:** happy new year ([0058aa9](http://git/projects/EF/repos/arui-feather/commits/0058aa9))



<a name="0.10.2"></a>
## 0.10.2 (2017-01-10)


### Bug Fixes

* add missed onChange call for input autocomplete ([a317b3f](http://git/projects/EF/repos/arui-feather/commits/a317b3f))
* **calendar:** fix calendar year enable condition ([35b3899](http://git/projects/EF/repos/arui-feather/commits/35b3899))
* **input:** fix input addons button border radius ([9641e5e](http://git/projects/EF/repos/arui-feather/commits/9641e5e))
* **masked-input:** fix bug on android chrome with caret setting ([2a85ec4](http://git/projects/EF/repos/arui-feather/commits/2a85ec4))


### Features

* move bem-cn to npmjs.org ([bf32393](http://git/projects/EF/repos/arui-feather/commits/bf32393))



<a name="0.10.1"></a>
## 0.10.1 (2016-12-23)


### Bug Fixes

* fix control groups border styles ([c3b3024](http://git/projects/EF/repos/arui-feather/commits/c3b3024))
* **checkbox,radio:** correct change value with prop ([308f0eb](http://git/projects/EF/repos/arui-feather/commits/308f0eb))
* **radio:** long-text break ([115c092](http://git/projects/EF/repos/arui-feather/commits/115c092))



<a name="0.10.0"></a>
# 0.10.0 (2016-12-17)


### Bug Fixes

* update autoprefixer browsers list ([b82afb7](http://git/projects/EF/repos/arui-feather/commits/b82afb7))
* **changelog:** fix changelog urls ([fab0444](http://git/projects/EF/repos/arui-feather/commits/fab0444))
* **footer:** fix footer styles ([28c5e17](http://git/projects/EF/repos/arui-feather/commits/28c5e17))
* **icon_action_ok:** change green color value ([d08021a](http://git/projects/EF/repos/arui-feather/commits/d08021a))


### Features

* **sidebar:** update sidebar transition ([6099845](http://git/projects/EF/repos/arui-feather/commits/6099845))



<a name="0.9.5"></a>
## 0.9.5 (2016-12-06)


### Bug Fixes

* **textarea:** pass maxLength prop to textarea ([259dda5](http://git/projects/EF/repos/arui-feather/commits/259dda5))



<a name="0.9.4"></a>
## 0.9.4 (2016-12-01)


### Bug Fixes

* fix layout styles ([e72927b](http://git/projects/EF/repos/arui-feather/commits/e72927b))
* **amount:** make currency mono-width ([afe8396](http://git/projects/EF/repos/arui-feather/commits/afe8396))


### Features

* **yarn:** check in yarn.lock ([274c0f9](http://git/projects/EF/repos/arui-feather/commits/274c0f9))



<a name="0.9.3"></a>
## 0.9.3 (2016-11-25)


### Features

* **input:** add view line mode to input ([bd93645](http://git/projects/EF/repos/arui-feather/commits/bd93645))



<a name="0.9.2"></a>
## 0.9.2 (2016-11-24)


### Features

* update gemini screens ([8f407eb](http://git/projects/EF/repos/arui-feather/commits/8f407eb))



<a name="0.9.1"></a>
## 0.9.1 (2016-11-23)


### Features

* **header:** add logo click handler ([56ba195](http://git/projects/EF/repos/arui-feather/commits/56ba195))



<a name="0.9.0"></a>
# 0.9.0 (2016-11-18)


### Bug Fixes

* fix font metrics ([2f38187](http://git/projects/EF/repos/arui-feather/commits/2f38187))



<a name="0.8.3"></a>
## 0.8.3 (2016-11-17)


### Bug Fixes

* **input:** fix autofilled input control background overflow ([34febe9](http://git/projects/EF/repos/arui-feather/commits/34febe9))
* **vars:** fix translucent typo ([ed10fec](http://git/projects/EF/repos/arui-feather/commits/ed10fec))


### Features

* **icons:** payment plus icons, rounded and in xxl ([3ed81c4](http://git/projects/EF/repos/arui-feather/commits/3ed81c4))



<a name="0.8.2"></a>
## 0.8.2 (2016-11-15)


### Bug Fixes

* **collapse:** fix collapse icon svg background scaling in ie9 ([5fa48c6](http://git/projects/EF/repos/arui-feather/commits/5fa48c6))


### Features

* **amount:** add ability to render in heading element ([48f629a](http://git/projects/EF/repos/arui-feather/commits/48f629a))
* **button:** add id attr ([0fd2cef](http://git/projects/EF/repos/arui-feather/commits/0fd2cef))
* **checkbox:** add checkbox id attr ([c67b8c6](http://git/projects/EF/repos/arui-feather/commits/c67b8c6))
* **form:** add id & name attrs ([6d2780f](http://git/projects/EF/repos/arui-feather/commits/6d2780f))
* **input:** add type password prop ([beac6ec](http://git/projects/EF/repos/arui-feather/commits/beac6ec))



<a name="0.8.1"></a>
## 0.8.1 (2016-11-08)


### Features

* **plate:** add flat prop ([c6824da](http://git/projects/EF/repos/arui-feather/commits/c6824da))



<a name="0.8.0"></a>
# 0.8.0 (2016-11-08)


### Bug Fixes

* fix layout styles in page, footer, header ([aa03945](http://git/projects/EF/repos/arui-feather/commits/aa03945))
* **collapse:** fix images file name typo ([297958c](http://git/projects/EF/repos/arui-feather/commits/297958c))
* **input:** fix transition ([00f5181](http://git/projects/EF/repos/arui-feather/commits/00f5181))
* **modernizr:** prevent modernizr from build on every postinstall ([564ae7f](http://git/projects/EF/repos/arui-feather/commits/564ae7f))


### Features

* colors for content ([e64ebf5](http://git/projects/EF/repos/arui-feather/commits/e64ebf5))
* **calendar:** speedup calendar ([a31d838](http://git/projects/EF/repos/arui-feather/commits/a31d838))



<a name="0.7.16"></a>
## 0.7.16 (2016-10-28)


### Bug Fixes

* set scroll-to default duration to 0 ([a0de488](http://git/projects/EF/repos/arui-feather/commits/a0de488))
* **postinstall:** prevent modernizr from build on every postinstall ([c36ff07](http://git/projects/EF/repos/arui-feather/commits/c36ff07))


### Features

* **calendar:** add years arrows ([236e385](http://git/projects/EF/repos/arui-feather/commits/236e385))
* **notification:** add hasCloser prop, fix naming for Plate and Sidebar ([00cd1a3](http://git/projects/EF/repos/arui-feather/commits/00cd1a3))
* **textarea:** add autosize prop to textarea ([f658a27](http://git/projects/EF/repos/arui-feather/commits/f658a27))



<a name="0.7.15"></a>
## 0.7.15 (2016-10-26)


### Features

* add gemini-optipng ([d873a12](http://git/projects/EF/repos/arui-feather/commits/d873a12))
* add onClick prop to Input component ([caada73](http://git/projects/EF/repos/arui-feather/commits/caada73))



<a name="0.7.14"></a>
## 0.7.14 (2016-10-24)


### Bug Fixes

* **radio-group:** backward compatibility ([43e53b1](http://git/projects/EF/repos/arui-feather/commits/43e53b1))


### Features

* **select:** equal popup and button width prop ([e271461](http://git/projects/EF/repos/arui-feather/commits/e271461))



<a name="0.7.13"></a>
## 0.7.13 (2016-10-21)


### Bug Fixes

* add forgotten vars import to app.css ([145b6ad](http://git/projects/EF/repos/arui-feather/commits/145b6ad))
* remove babel-plugin-react-require ([aa3275c](http://git/projects/EF/repos/arui-feather/commits/aa3275c))



<a name="0.7.12"></a>
## 0.7.12 (2016-10-20)


### Bug Fixes

* fix proptypes descriptions in accordance with guidelines ([f7497ff](http://git/projects/EF/repos/arui-feather/commits/f7497ff))
* **app.css:** add margin ([fa55a7e](http://git/projects/EF/repos/arui-feather/commits/fa55a7e))
* **select:** add blur on value change for radio-check ([89119e2](http://git/projects/EF/repos/arui-feather/commits/89119e2))
* **select:** add blur on value change if mode is radio ([525680e](http://git/projects/EF/repos/arui-feather/commits/525680e))



<a name="0.7.11"></a>
## 0.7.11 (2016-10-19)


### Features

* **input-autocomplete:** add logic on esc click ([bd647a6](http://git/projects/EF/repos/arui-feather/commits/bd647a6))



<a name="0.7.10"></a>
## 0.7.10 (2016-10-19)


### Bug Fixes

* **header:** remove header main case color style ([14f61f3](http://git/projects/EF/repos/arui-feather/commits/14f61f3))


### Features

* app-menu on mobile scrollable ([1795760](http://git/projects/EF/repos/arui-feather/commits/1795760))
* **gemini:** add ui regression testing infrastructure ([a8fe44e](http://git/projects/EF/repos/arui-feather/commits/a8fe44e))
* **radio-group:** add `value` and `onChange` props ([183f6fc](http://git/projects/EF/repos/arui-feather/commits/183f6fc))



<a name="0.7.9"></a>
## 0.7.9 (2016-10-13)


### Features

* use autobind decorator from core-decorators ([386ee82](http://git/projects/EF/repos/arui-feather/commits/386ee82))



<a name="0.7.8"></a>
## 0.7.8 (2016-10-12)



<a name="0.7.7"></a>
## 0.7.7 (2016-10-12)


### Features

* **page:** new background gradient for page ([f9a4c6e](http://git/projects/EF/repos/arui-feather/commits/f9a4c6e))



<a name="0.7.6"></a>
## 0.7.6 (2016-10-12)


### Bug Fixes

* **menu:** fix getFirstItem method ([94d843e](http://git/projects/EF/repos/arui-feather/commits/94d843e))
* **notification:** vertical align for notification title, line up title with icon ([91c714d](http://git/projects/EF/repos/arui-feather/commits/91c714d))
* **popup:** unnecessary scroll appears in safari ([b6b9f7d](http://git/projects/EF/repos/arui-feather/commits/b6b9f7d))
* **sidebar:** RECSYS-1558  wrap Sidebar with PopupContextProvider ([ce288fc](http://git/projects/EF/repos/arui-feather/commits/ce288fc))


### Features

* add scrollTo public methods to Input, Select, Textarea, Checkbox, Radio ([1e85289](http://git/projects/EF/repos/arui-feather/commits/1e85289))
* **prop-types:** add deprecated prop type ([1e5ca40](http://git/projects/EF/repos/arui-feather/commits/1e5ca40))



<a name="0.7.5"></a>
## 0.7.5 (2016-10-07)


### Bug Fixes

* **select:** scroll custom element when select opens ([b772729](http://git/projects/EF/repos/arui-feather/commits/b772729))
* **select:** scrollbar appears outside options list in Chrome ([1eab260](http://git/projects/EF/repos/arui-feather/commits/1eab260))


### Features

* add debounce from core-decoration ([903ab4e](http://git/projects/EF/repos/arui-feather/commits/903ab4e))
* dependency injection support for cn ([bc53802](http://git/projects/EF/repos/arui-feather/commits/bc53802))
* move font metrics to vars.css ([54a5faa](http://git/projects/EF/repos/arui-feather/commits/54a5faa))



<a name="0.7.4"></a>
## 0.7.4 (2016-10-05)


### Bug Fixes

* **sidebar:** fix closed sidebar visibility ([ef365bc](http://git/projects/EF/repos/arui-feather/commits/ef365bc))



<a name="0.7.3"></a>
## 0.7.3 (2016-09-30)


### Bug Fixes

* fix input-group, radio, radio-group tab navigation ([5e63007](http://git/projects/EF/repos/arui-feather/commits/5e63007))
* **sidebar:** fix paddings, closer icon position ([6d626ba](http://git/projects/EF/repos/arui-feather/commits/6d626ba))
* **sidebar:** fix sidebar top padding ([1d0baae](http://git/projects/EF/repos/arui-feather/commits/1d0baae))
* **sidebar:** remove strange artefact ([f6ac1dd](http://git/projects/EF/repos/arui-feather/commits/f6ac1dd))


### Features

* **.githooks:** add git commit message validation hook ([1af0da2](http://git/projects/EF/repos/arui-feather/commits/1af0da2))



<a name="0.7.2"></a>
## 0.7.2 (2016-09-29)


### Bug Fixes

* **cn:** refresh cn on className change for React to rerender ([7ac642e](http://git/projects/EF/repos/arui-feather/commits/7ac642e))



<a name="0.7.1"></a>
## 0.7.1 (2016-09-28)


### Bug Fixes

* use default props where it possible ([787a842](http://git/projects/EF/repos/arui-feather/commits/787a842))
* **amount:** fix minus showing in negative amount values ([66b8e30](http://git/projects/EF/repos/arui-feather/commits/66b8e30))
* **checkbox:** default props ([d8c5fc4](http://git/projects/EF/repos/arui-feather/commits/d8c5fc4))
* **input-autocomplete:** call onBlur callback when blur was triggered by click outside ([12b7cee](http://git/projects/EF/repos/arui-feather/commits/12b7cee))


### Features

* dont load unnecessary moment locales ([bd3cecc](http://git/projects/EF/repos/arui-feather/commits/bd3cecc))
* **sidebar:** black refrigerator ([10d21d6](http://git/projects/EF/repos/arui-feather/commits/10d21d6))



<a name="0.7.0"></a>
# 0.7.0 (2016-09-26)


### Bug Fixes

* run unit tests in phantomjs ([1f9e975](http://git/projects/EF/repos/arui-feather/commits/1f9e975))
* **Dockerfile:** run tests without colored output ([7f282ed](http://git/projects/EF/repos/arui-feather/commits/7f282ed))
* **render-in-container:** declare forgotten react import in render-in-container ([6a8c21d](http://git/projects/EF/repos/arui-feather/commits/6a8c21d))
* **render-in-container:** handle receive container on popup will mount ([6a34a22](http://git/projects/EF/repos/arui-feather/commits/6a34a22))


### Features

* **notification:** add title ([913087d](http://git/projects/EF/repos/arui-feather/commits/913087d))
* **popup:** render popup in custom container set via context. ([4335539](http://git/projects/EF/repos/arui-feather/commits/4335539))



<a name="0.6.4"></a>
## 0.6.4 (2016-09-23)


### Features

* **CONTRIBUTION.md:** initial contribution guide ([02b576d](http://git/projects/EF/repos/arui-feather/commits/02b576d))



<a name="0.6.3"></a>
## 0.6.3 (2016-09-23)


### Bug Fixes

* **input.css:** fix continually repaint in empty input after blur in safari ([a5f2a7e](http://git/projects/EF/repos/arui-feather/commits/a5f2a7e))
* **package.json:** correct naming for npm scripts ([f90d074](http://git/projects/EF/repos/arui-feather/commits/f90d074))
* **webpack.config.template:** remove postcss-calc warnings ([2607b09](http://git/projects/EF/repos/arui-feather/commits/2607b09))


### Features

* add input-group component ([16f5bc0](http://git/projects/EF/repos/arui-feather/commits/16f5bc0))
* add react optimization plugins ([d00df53](http://git/projects/EF/repos/arui-feather/commits/d00df53))
* add width available property to radio-group ([5cb668c](http://git/projects/EF/repos/arui-feather/commits/5cb668c))
* change type to tel in CardInput ([67c5674](http://git/projects/EF/repos/arui-feather/commits/67c5674))
* **bem-cn:** custom bem-cn without perfomance degradation ([ac334b2](http://git/projects/EF/repos/arui-feather/commits/ac334b2))
* **calendar-input:** new error design ([b06c372](http://git/projects/EF/repos/arui-feather/commits/b06c372))



<a name="0.6.2"></a>
## 0.6.2 (2016-09-15)


### Features

* **icon:** Add category icons ([fad310d](http://git/projects/EF/repos/arui-feather/commits/fad310d))



<a name="0.6.1"></a>
## 0.6.1 (2016-09-13)


### Bug Fixes

* normalize input width ([ff80e4f](http://git/projects/EF/repos/arui-feather/commits/ff80e4f))
* **select:** fix options.text prop type ([e78d653](http://git/projects/EF/repos/arui-feather/commits/e78d653))


### Features

* **input:** allow custom error popup directions ([73db80d](http://git/projects/EF/repos/arui-feather/commits/73db80d))



<a name="0.6.0"></a>
# 0.6.0 (2016-09-07)


### Bug Fixes

* use default placeholder for disabled first optgroup/option in native select ([8c6b429](http://git/projects/EF/repos/arui-feather/commits/8c6b429))
* **main:** equalize font antialiasing between os x webkit and moz browsers ([0bb6f37](http://git/projects/EF/repos/arui-feather/commits/0bb6f37))
* **mq:** correct breakpoint values in a more accurate ([06b73c4](http://git/projects/EF/repos/arui-feather/commits/06b73c4))


### Features

* allow to run demo on any port ([66bba17](http://git/projects/EF/repos/arui-feather/commits/66bba17))
* **prop-types:** add react prop types, update doc builder ([b459c18](http://git/projects/EF/repos/arui-feather/commits/b459c18))
* **readme:** add supported browsers section in readme ([e1d9ada](http://git/projects/EF/repos/arui-feather/commits/e1d9ada))



<a name="0.5.7"></a>
## 0.5.7 (2016-09-06)


### Bug Fixes

* **icon:** add missed ok_filled icon ([c8e93f6](http://git/projects/EF/repos/arui-feather/commits/c8e93f6))



<a name="0.5.6"></a>
## 0.5.6 (2016-09-06)


### Bug Fixes

* **polyfill:** add `matchMedia addListener` polyfill for ie9 ([4b2c303](http://git/projects/EF/repos/arui-feather/commits/4b2c303))



<a name="0.5.5"></a>
## 0.5.5 (2016-09-05)


### Bug Fixes

* fix button inner aligning with icon and without text ([59f95bf](http://git/projects/EF/repos/arui-feather/commits/59f95bf))
* fix native select disabled options ([4bd2434](http://git/projects/EF/repos/arui-feather/commits/4bd2434))


### Features

* **radio-group:** show error popup on focus ([95833d1](http://git/projects/EF/repos/arui-feather/commits/95833d1))



<a name="0.5.4"></a>
## 0.5.4 (2016-09-05)


### Bug Fixes

* add value to event.target.value in select onFocus/onBlur callbacks ([23692ac](http://git/projects/EF/repos/arui-feather/commits/23692ac))
* fix button inner aligning ([28a53f7](http://git/projects/EF/repos/arui-feather/commits/28a53f7))
* remove deprecated components ([777db3e](http://git/projects/EF/repos/arui-feather/commits/777db3e))
* remove deprecated period & period-picker from demo ([631e136](http://git/projects/EF/repos/arui-feather/commits/631e136))



<a name="0.5.3"></a>
## 0.5.3 (2016-09-02)


### Bug Fixes

* **calendar:** fix comparing props on componentWillReceiveProps, add `visible` props to calendar ([400e90b](http://git/projects/EF/repos/arui-feather/commits/400e90b))


### Features

* **header:** app `fixed` props to header ([0b48c4d](http://git/projects/EF/repos/arui-feather/commits/0b48c4d))
* **radio-group:** add public focus/blur methods and onFocus/onBlur callbacks ([c6f360d](http://git/projects/EF/repos/arui-feather/commits/c6f360d))



<a name="0.5.2"></a>
## 0.5.2 (2016-09-01)


### Bug Fixes

* fix page padding with float header height ([71f0f46](http://git/projects/EF/repos/arui-feather/commits/71f0f46))
* **performance:** check context in performance decorator, fixes ARUIF-73 ([f23433c](http://git/projects/EF/repos/arui-feather/commits/f23433c))


### Features

* add auto scrolling on adaptive select & input-autocomplete keyboard navigation ([572434d](http://git/projects/EF/repos/arui-feather/commits/572434d))
* **radio:** add public focus and blur methods ([0970942](http://git/projects/EF/repos/arui-feather/commits/0970942))



<a name="0.5.1"></a>
## 0.5.1 (2016-08-25)


### Bug Fixes

* **menu-item:** prevent click native behavior when disabled=true ([33d1d8f](http://git/projects/EF/repos/arui-feather/commits/33d1d8f))
* **select:** add disabled mod to classname ([622fbf6](http://git/projects/EF/repos/arui-feather/commits/622fbf6))


### Features

* **input:** add pattern attribute ([971c323](http://git/projects/EF/repos/arui-feather/commits/971c323))



<a name="0.5.0"></a>
# 0.5.0 (2016-08-25)


### Bug Fixes

* **card-input:** wrong docs ([d4ab7cd](http://git/projects/EF/repos/arui-feather/commits/d4ab7cd))
* **input:** decision problems IE11 and masked input ([eb32878](http://git/projects/EF/repos/arui-feather/commits/eb32878))
* **input:** fix input disabled state ([4dc56ad](http://git/projects/EF/repos/arui-feather/commits/4dc56ad))


### Features

* add fading gradient to popup with adaptive height ([6eebfb7](http://git/projects/EF/repos/arui-feather/commits/6eebfb7))
* add param target ([54da506](http://git/projects/EF/repos/arui-feather/commits/54da506))
* caret for money input ([da7c5f1](http://git/projects/EF/repos/arui-feather/commits/da7c5f1))
* **select:** add native text for option ([554f596](http://git/projects/EF/repos/arui-feather/commits/554f596))



<a name="0.4.9"></a>
## 0.4.9 (2016-08-22)


### Bug Fixes

* **input-autocomplete:** invisible options list ([b0b14c5](http://git/projects/EF/repos/arui-feather/commits/b0b14c5))



<a name="0.4.8"></a>
## 0.4.8 (2016-08-21)


### Features

* **link:** add target prop ([b34fb55](http://git/projects/EF/repos/arui-feather/commits/b34fb55))



<a name="0.4.7"></a>
## 0.4.7 (2016-08-21)


### Bug Fixes

* fix adaptive popup styles on ios safari ([3f42141](http://git/projects/EF/repos/arui-feather/commits/3f42141))
* **app-title/demo:** codestyle ([87dbfdd](http://git/projects/EF/repos/arui-feather/commits/87dbfdd))
* **demo/textarea:** remove unused css ([d0a42b7](http://git/projects/EF/repos/arui-feather/commits/d0a42b7))
* **header:** remove background with small breakpoint ([8a692a4](http://git/projects/EF/repos/arui-feather/commits/8a692a4))


### Features

* **button/demo:** improve ([9e9db53](http://git/projects/EF/repos/arui-feather/commits/9e9db53))
* **demo/demo.css:** menu background color lighter ([6068347](http://git/projects/EF/repos/arui-feather/commits/6068347))



<a name="0.4.6"></a>
## 0.4.6 (2016-08-18)


### Bug Fixes

* adapt popup to window edge ([405ce66](http://git/projects/EF/repos/arui-feather/commits/405ce66))


### Features

* **calendar-input:** add onInputKeyDown, onCalendarKeyDown callback, add close() public method ([950c5c8](http://git/projects/EF/repos/arui-feather/commits/950c5c8))



<a name="0.4.5"></a>
## 0.4.5 (2016-08-16)


### Bug Fixes

* **calendar-input:** fix opacity for custom field on ios ([41d3875](http://git/projects/EF/repos/arui-feather/commits/41d3875))
* **calendar-input:** show native calendar widget on android ([06cc5a7](http://git/projects/EF/repos/arui-feather/commits/06cc5a7))
* **demo/demo-section:** mobile css breakpoint ([4eb0fad](http://git/projects/EF/repos/arui-feather/commits/4eb0fad))
* **error-page:** docs ([10288e1](http://git/projects/EF/repos/arui-feather/commits/10288e1))
* **input:** fix css for type hidden ([0955ccf](http://git/projects/EF/repos/arui-feather/commits/0955ccf))
* **input:** show native calendar-input errors ([8dea077](http://git/projects/EF/repos/arui-feather/commits/8dea077))


### Features

* add stylelint ([fc9af28](http://git/projects/EF/repos/arui-feather/commits/fc9af28))
* **demo/demo-section:** update mobile layouts ([cc1e60c](http://git/projects/EF/repos/arui-feather/commits/cc1e60c))
* **masked-input:** new masked input ([31cfc0b](http://git/projects/EF/repos/arui-feather/commits/31cfc0b))



<a name="0.4.4"></a>
## 0.4.4 (2016-08-09)


### Bug Fixes

* **calendar-input:** width available ([334a304](http://git/projects/EF/repos/arui-feather/commits/334a304))


### Features

* **calendar-input:** popup directions ([a59b8df](http://git/projects/EF/repos/arui-feather/commits/a59b8df))



<a name="0.4.3"></a>
## 0.4.3 (2016-08-09)


### Bug Fixes

* **demo:** run demo/ through eslint, fix eslint warnings ([6c937e2](http://git/projects/EF/repos/arui-feather/commits/6c937e2))
* **select:** fix native select first option bug in radio-check mode ([77981c2](http://git/projects/EF/repos/arui-feather/commits/77981c2))



<a name="0.4.2"></a>
## 0.4.2 (2016-08-08)


### Bug Fixes

* **modernizr:** modernizr for server side rendering ([f00b40e](http://git/projects/EF/repos/arui-feather/commits/f00b40e))


### Features

* **README.md:** beta ([39bb1a6](http://git/projects/EF/repos/arui-feather/commits/39bb1a6))



<a name="0.4.1"></a>
## 0.4.1 (2016-08-08)


### Features

* **radio:** add tab index ([8d3a0a8](http://git/projects/EF/repos/arui-feather/commits/8d3a0a8))



<a name="0.4.0"></a>
# 0.4.0 (2016-08-08)


### Bug Fixes

* **header/demo:** typo ([e3ae398](http://git/projects/EF/repos/arui-feather/commits/e3ae398))
* **modernizr:** prebuild modernizr on postinstall ([e0bd751](http://git/projects/EF/repos/arui-feather/commits/e0bd751))
* **radio:** add invalidate on component ([30e8643](http://git/projects/EF/repos/arui-feather/commits/30e8643))
* **radio:** add invalidate on component ([2c8f425](http://git/projects/EF/repos/arui-feather/commits/2c8f425))
* **radio:** default events ([a143a61](http://git/projects/EF/repos/arui-feather/commits/a143a61))



<a name="0.3.8"></a>
## 0.3.8 (2016-08-04)


### Features

* add mobile calendar-input ([bab5017](http://git/projects/EF/repos/arui-feather/commits/bab5017))
* **karma:** throw error in tests on react warnings ([d119a17](http://git/projects/EF/repos/arui-feather/commits/d119a17))



<a name="0.3.7"></a>
## 0.3.7 (2016-08-03)


### Bug Fixes

* remove webpack-postcss-tools ([8f3bde9](http://git/projects/EF/repos/arui-feather/commits/8f3bde9))
* **input:** fix onChange in MaskedInput in ie11 ([2e822be](http://git/projects/EF/repos/arui-feather/commits/2e822be))


### Features

* add pre-commit hook for building docs ([3bcb45e](http://git/projects/EF/repos/arui-feather/commits/3bcb45e))



<a name="0.3.6"></a>
## 0.3.6 (2016-08-01)


### Bug Fixes

* use event argument in blur callbacks in form fields props ([3eefb8a](http://git/projects/EF/repos/arui-feather/commits/3eefb8a))
* **select:** autofocus on first render ([d3dfe4d](http://git/projects/EF/repos/arui-feather/commits/d3dfe4d))


### Features

* add to header free block before logo ([29db67f](http://git/projects/EF/repos/arui-feather/commits/29db67f))
* **period-picker:** make deprecated ([01ef478](http://git/projects/EF/repos/arui-feather/commits/01ef478))



<a name="0.3.5"></a>
## 0.3.5 (2016-07-26)


### Bug Fixes

* **index.html:** fix url of vendor.js script ([8677810](http://git/projects/EF/repos/arui-feather/commits/8677810))
* **input:** left and right addons layouts ([2171655](http://git/projects/EF/repos/arui-feather/commits/2171655))
* **money-input:** fix fraction part ([90b4414](http://git/projects/EF/repos/arui-feather/commits/90b4414))


### Features

* add react-hot-loader plugin ([6c760b0](http://git/projects/EF/repos/arui-feather/commits/6c760b0))
* **select:** change external interface ([1e81e82](http://git/projects/EF/repos/arui-feather/commits/1e81e82))
* **webpack.config:** speadup development build ([6ff427a](http://git/projects/EF/repos/arui-feather/commits/6ff427a))



<a name="0.3.4"></a>
## 0.3.4 (2016-07-26)


### Bug Fixes

* add missed html tag ([e350ec2](http://git/projects/EF/repos/arui-feather/commits/e350ec2))
* move back to original karma-ios-simulator-launcher ([5064d81](http://git/projects/EF/repos/arui-feather/commits/5064d81))
* update readme & karma config ([37129a7](http://git/projects/EF/repos/arui-feather/commits/37129a7))
* **input:** fix paste event in ie11 ([4cf6af6](http://git/projects/EF/repos/arui-feather/commits/4cf6af6))
* **input-autocomplte:** add key props, fix react keys warning ([2b739db](http://git/projects/EF/repos/arui-feather/commits/2b739db))
* **karma.conf:** update karma conf ([250a93d](http://git/projects/EF/repos/arui-feather/commits/250a93d))
* **logo:** fix logo text svg css background scaling in ie11 ([32ec143](http://git/projects/EF/repos/arui-feather/commits/32ec143))
* **readme:** update mobile karma testing line in readme ([ac58990](http://git/projects/EF/repos/arui-feather/commits/ac58990))
* **select:** fix react warning about missing keys ([dbff5a2](http://git/projects/EF/repos/arui-feather/commits/dbff5a2))


### Features

* add heading for demo page ([b1e8a8f](http://git/projects/EF/repos/arui-feather/commits/b1e8a8f))
* add IE meta ([ae2278b](http://git/projects/EF/repos/arui-feather/commits/ae2278b))
* patch karma-ios-simulator-launcher for using with different devices ([b5e2861](http://git/projects/EF/repos/arui-feather/commits/b5e2861))
* replace Object.assign with spread operator ([fb6ae77](http://git/projects/EF/repos/arui-feather/commits/fb6ae77))
* use one mq config for everything ([af6cfc7](http://git/projects/EF/repos/arui-feather/commits/af6cfc7))
* **heading:** update heading font sizes for mobile ([dd09c3e](http://git/projects/EF/repos/arui-feather/commits/dd09c3e))



<a name="0.3.3"></a>
## 0.3.3 (2016-07-16)


### Bug Fixes

* **calendar-input:** error popup position ([ac4c27d](http://git/projects/EF/repos/arui-feather/commits/ac4c27d))



<a name="0.3.2"></a>
## 0.3.2 (2016-07-16)


### Bug Fixes

* fix/test select on ios safari; update mq ([0174d06](http://git/projects/EF/repos/arui-feather/commits/0174d06))
* update mq docs, update getMatchMedia func ([988c1a4](http://git/projects/EF/repos/arui-feather/commits/988c1a4))


### Features

* **calendar-input:** add onChange callback ([b0672de](http://git/projects/EF/repos/arui-feather/commits/b0672de))



<a name="0.3.1"></a>
## 0.3.1 (2016-07-15)


### Bug Fixes

* code review fixes ([cc7a9de](http://git/projects/EF/repos/arui-feather/commits/cc7a9de))
* move matchMedia polyfill requiring to polyfills.js ([8b42a90](http://git/projects/EF/repos/arui-feather/commits/8b42a90))
* rewrite mq tests, delete ref counters query on release in mq ([1228072](http://git/projects/EF/repos/arui-feather/commits/1228072))
* update mql pool & handlematch, update native select prop ([a7a04df](http://git/projects/EF/repos/arui-feather/commits/a7a04df))
* use mqlPool with refs counting on mounting/unmounting; fix select codestyle ([e6b3531](http://git/projects/EF/repos/arui-feather/commits/e6b3531))
* **amount:** whitespace after minus symbol in negative amounts ([fb23f17](http://git/projects/EF/repos/arui-feather/commits/fb23f17))
* **currency-codes:** change symbols for CAD ([ed11639](http://git/projects/EF/repos/arui-feather/commits/ed11639))
* **docs:** generate mq docs ([95037b5](http://git/projects/EF/repos/arui-feather/commits/95037b5))
* **docs:** update docs mq readme ([d86d1de](http://git/projects/EF/repos/arui-feather/commits/d86d1de))
* **main:** allow to use percentage max-width & overflow in table cells ([6fdb392](http://git/projects/EF/repos/arui-feather/commits/6fdb392))
* **mq:** add mq touch prop default value, remove insurance in ms gesture assignment ([88f8310](http://git/projects/EF/repos/arui-feather/commits/88f8310))
* **mq:** update handleMatch ([80d7d3a](http://git/projects/EF/repos/arui-feather/commits/80d7d3a))
* **mq:** update mq docs ([e9a470e](http://git/projects/EF/repos/arui-feather/commits/e9a470e))
* **mq:** update mq pool, add pool tests ([1dbe14d](http://git/projects/EF/repos/arui-feather/commits/1dbe14d))
* **select:** add key props for array, fix react warnings ([3d8659a](http://git/projects/EF/repos/arui-feather/commits/3d8659a))
* **select:** add value, disabled & onClick props to native select ([5b7bd45](http://git/projects/EF/repos/arui-feather/commits/5b7bd45))
* **select:** use Array.from instead of [].method.call ([4cbad76](http://git/projects/EF/repos/arui-feather/commits/4cbad76))


### Features

* add ios-simulator-launcher to karma testing ([beeb01c](http://git/projects/EF/repos/arui-feather/commits/beeb01c))
* show just rendered components in demo menu ([8b63d96](http://git/projects/EF/repos/arui-feather/commits/8b63d96))
* update mq & select ([4cc6cd3](http://git/projects/EF/repos/arui-feather/commits/4cc6cd3))
* **notification:** mobile version ([874c871](http://git/projects/EF/repos/arui-feather/commits/874c871))



<a name="0.3.0"></a>
# 0.3.0 (2016-07-08)


### Features

* add environment variable to render just few components ([b373bab](http://git/projects/EF/repos/arui-feather/commits/b373bab))



<a name="0.2.0"></a>
# 0.2.0 (2016-07-08)


### Features

* add should component update method ([0684f64](http://git/projects/EF/repos/arui-feather/commits/0684f64))
* use one mq config for everything ([b3c1aa5](http://git/projects/EF/repos/arui-feather/commits/b3c1aa5))



<a name="0.1.30"></a>
## 0.1.30 (2016-07-06)


### Features

* **demo:** add mobile menu & mobile styles for demo page ([f8a92fe](http://git/projects/EF/repos/arui-feather/commits/f8a92fe))
* **error-page:** add error-page component ([7e78430](http://git/projects/EF/repos/arui-feather/commits/7e78430))
* **form:** add noValidate and autoComplete props ([35fde4c](http://git/projects/EF/repos/arui-feather/commits/35fde4c))
* **input:** add onPaste handler ([8eb62af](http://git/projects/EF/repos/arui-feather/commits/8eb62af))
* **tests:** allow to run only few tests if needed ([42dbc28](http://git/projects/EF/repos/arui-feather/commits/42dbc28))



<a name="0.1.29"></a>
## 0.1.29 (2016-07-01)


### Features

* optimize all svg images with svgo ([9ce5b72](http://git/projects/EF/repos/arui-feather/commits/9ce5b72))
* **footer:** allow to pass custom social and copyright components ([02873f7](http://git/projects/EF/repos/arui-feather/commits/02873f7))
* **textarea:** add resize prop ([6a857b7](http://git/projects/EF/repos/arui-feather/commits/6a857b7))



<a name="0.1.28"></a>
## 0.1.28 (2016-06-25)


### Bug Fixes

* **list:** type typo ([3724e5f](http://git/projects/EF/repos/arui-feather/commits/3724e5f))



<a name="0.1.27"></a>
## 0.1.27 (2016-06-25)


### Bug Fixes

* **dropdown:** hover mode ([4199aca](http://git/projects/EF/repos/arui-feather/commits/4199aca))



<a name="0.1.26"></a>
## 0.1.26 (2016-06-25)


### Bug Fixes

* **dropdown:** default size ([6a28f93](http://git/projects/EF/repos/arui-feather/commits/6a28f93))
* **popup:** docs typo ([84d5f7e](http://git/projects/EF/repos/arui-feather/commits/84d5f7e))


### Features

* **dropdown:** autoclose on click outside popup and additional callbacks ([57f92c4](http://git/projects/EF/repos/arui-feather/commits/57f92c4))



<a name="0.1.25"></a>
## 0.1.25 (2016-06-24)


### Bug Fixes

* **radio-group:** correct import and type ([c9bbe05](http://git/projects/EF/repos/arui-feather/commits/c9bbe05))


### Features

* **checkbox-group:** add type line ([8934cbb](http://git/projects/EF/repos/arui-feather/commits/8934cbb))
* **radio-group:** add type line ([f3e699b](http://git/projects/EF/repos/arui-feather/commits/f3e699b))



<a name="0.1.24"></a>
## 0.1.24 (2016-06-24)


### Bug Fixes

* remove jshint ([aa723c5](http://git/projects/EF/repos/arui-feather/commits/aa723c5))
* **select:** cut text with width=available ([93fc0fa](http://git/projects/EF/repos/arui-feather/commits/93fc0fa))
* **select:** fix focused state of select ([c65b411](http://git/projects/EF/repos/arui-feather/commits/c65b411))


### Features

* **money-input:** add raw value to onChange callback ([4ba2bfa](http://git/projects/EF/repos/arui-feather/commits/4ba2bfa))



<a name="0.1.23"></a>
## 0.1.23 (2016-06-20)


### Features

* additional cs tests ([b5cca94](http://git/projects/EF/repos/arui-feather/commits/b5cca94))
* remove unused dep ([af8da7e](http://git/projects/EF/repos/arui-feather/commits/af8da7e))
* **money-input:** add number format options (integer size and fraction size) ([729ae53](http://git/projects/EF/repos/arui-feather/commits/729ae53))
* **popup:** move css to lib ([f3cc96e](http://git/projects/EF/repos/arui-feather/commits/f3cc96e))



<a name="0.1.22"></a>
## 0.1.22 (2016-06-16)


### Bug Fixes

* **polyfills:** add polyfill for constructor call in ie<10 ([7cd3a6d](http://git/projects/EF/repos/arui-feather/commits/7cd3a6d))


### Features

* add perfomance test infrastructure ([dcaad01](http://git/projects/EF/repos/arui-feather/commits/dcaad01))
* **font:** move css to lib ([c5cc53f](http://git/projects/EF/repos/arui-feather/commits/c5cc53f))
* **icon:** optimize webpack build docs ([1a258a5](http://git/projects/EF/repos/arui-feather/commits/1a258a5))
* **menu:** move css to lib ([8cd469a](http://git/projects/EF/repos/arui-feather/commits/8cd469a))
* **menu-item:** move css to lib ([850afd5](http://git/projects/EF/repos/arui-feather/commits/850afd5))



<a name="0.1.21"></a>
## 0.1.21 (2016-06-12)


### Bug Fixes

* some demo css improvements ([38266a9](http://git/projects/EF/repos/arui-feather/commits/38266a9))
* **amount:** missed jsdoc property name ([228977b](http://git/projects/EF/repos/arui-feather/commits/228977b))
* **button:** make disabled button disabled ([499ffc8](http://git/projects/EF/repos/arui-feather/commits/499ffc8))
* **form-field:** fix demo page, add jsdoc description ([a4fac5a](http://git/projects/EF/repos/arui-feather/commits/a4fac5a))
* **resize-sensor-test:** spies counter ([21aae6b](http://git/projects/EF/repos/arui-feather/commits/21aae6b))


### Features

* **icon:** move css to lib ([41022a3](http://git/projects/EF/repos/arui-feather/commits/41022a3))
* **message:** move css to lib ([37fba25](http://git/projects/EF/repos/arui-feather/commits/37fba25))
* **paragraph:** move css to lib ([cb2730c](http://git/projects/EF/repos/arui-feather/commits/cb2730c))
* **plate:** move css to lib ([a6aa3f1](http://git/projects/EF/repos/arui-feather/commits/a6aa3f1))



<a name="0.1.20"></a>
## 0.1.20 (2016-06-08)


### Bug Fixes

* fix position bug in firefox, add default direction to input error popup ([edf235b](http://git/projects/EF/repos/arui-feather/commits/edf235b))
* run tests only when source changed ([f2ab6ae](http://git/projects/EF/repos/arui-feather/commits/f2ab6ae))
* **header:** invisible background on scrolled page reload ([6cda8da](http://git/projects/EF/repos/arui-feather/commits/6cda8da))
* **radio:** disable tab stop for button radio mode ([e6dabe5](http://git/projects/EF/repos/arui-feather/commits/e6dabe5))


### Features

* **label:** move css to lib ([e4c1242](http://git/projects/EF/repos/arui-feather/commits/e4c1242))
* **link:** move css to lib ([35203ab](http://git/projects/EF/repos/arui-feather/commits/35203ab))
* **list:** move css to lib ([d5c1b7d](http://git/projects/EF/repos/arui-feather/commits/d5c1b7d))



<a name="0.1.19"></a>
## 0.1.19 (2016-06-06)


### Bug Fixes

* fix radio, checkbox check handlers ([e8a76a1](http://git/projects/EF/repos/arui-feather/commits/e8a76a1))
* **header:** mobile layout background ([dbb8050](http://git/projects/EF/repos/arui-feather/commits/dbb8050))
* **highlight:** css name ([6739630](http://git/projects/EF/repos/arui-feather/commits/6739630))


### Features

* check jsdoc with eslint ([3e6fe4c](http://git/projects/EF/repos/arui-feather/commits/3e6fe4c))
* code-quality + unit testing pre-commit hook ([6095864](http://git/projects/EF/repos/arui-feather/commits/6095864))
* **amount:** use chai-dom in test ([57bfd9e](http://git/projects/EF/repos/arui-feather/commits/57bfd9e))
* **calendar-input:** move css to lib ([d85b61e](http://git/projects/EF/repos/arui-feather/commits/d85b61e))
* **copyright:** move css to lib ([8c9f403](http://git/projects/EF/repos/arui-feather/commits/8c9f403))
* **highlight:** move css to lib ([7c138f4](http://git/projects/EF/repos/arui-feather/commits/7c138f4))
* **input-autocomplete:** move css to lib ([04085a0](http://git/projects/EF/repos/arui-feather/commits/04085a0))
* **radio:** move css to lib ([3a4e475](http://git/projects/EF/repos/arui-feather/commits/3a4e475))



<a name="0.1.18"></a>
## 0.1.18 (2016-06-02)


### Bug Fixes

* **select:** do not show select menu during disabled state ([e0bf29e](http://git/projects/EF/repos/arui-feather/commits/e0bf29e))


### Features

* **amount:** add tests ([87d3dbb](http://git/projects/EF/repos/arui-feather/commits/87d3dbb))



<a name="0.1.17"></a>
## 0.1.17 (2016-06-01)


### Bug Fixes

* **select:** correct onFocus/onBlur behaviour ([7da3f9a](http://git/projects/EF/repos/arui-feather/commits/7da3f9a))


### Features

* **menu:** onFocus and onBlur events ([8e47ebd](http://git/projects/EF/repos/arui-feather/commits/8e47ebd))
* **select:** additional callbacks for select ([859789d](http://git/projects/EF/repos/arui-feather/commits/859789d))



<a name="0.1.16"></a>
## 0.1.16 (2016-06-01)


### Bug Fixes

* **calendar-input:** placeholder char width as number ([4d3f4d5](http://git/projects/EF/repos/arui-feather/commits/4d3f4d5))
* **select:** add default empty options ([66acbe7](http://git/projects/EF/repos/arui-feather/commits/66acbe7))


### Features

* remove min-height for select and textarea for width=available mode ([61b80c6](http://git/projects/EF/repos/arui-feather/commits/61b80c6))
* **calendar:** move css to lib ([bff3311](http://git/projects/EF/repos/arui-feather/commits/bff3311))
* **card-number:** move css to lib ([6aa6bb5](http://git/projects/EF/repos/arui-feather/commits/6aa6bb5))
* **checkbox:** move css to lib ([e787d09](http://git/projects/EF/repos/arui-feather/commits/e787d09))
* **checkbox-group:** move css to lib ([c8f92d5](http://git/projects/EF/repos/arui-feather/commits/c8f92d5))
* **form-field:** move css to lib ([c6fdce6](http://git/projects/EF/repos/arui-feather/commits/c6fdce6))
* **input:** move css to lib ([6e883d8](http://git/projects/EF/repos/arui-feather/commits/6e883d8))
* **radio-group:** move css to lib ([bf40bc6](http://git/projects/EF/repos/arui-feather/commits/bf40bc6))
* **social:** move css to lib ([0d4b960](http://git/projects/EF/repos/arui-feather/commits/0d4b960))



<a name="0.1.15"></a>
## 0.1.15 (2016-05-30)


### Features

* update ui dep to v5.0.1 ([002507d](http://git/projects/EF/repos/arui-feather/commits/002507d))
* **amount:** add component ([a23b3c0](http://git/projects/EF/repos/arui-feather/commits/a23b3c0))



<a name="0.1.14"></a>
## 0.1.14 (2016-05-30)


### Bug Fixes

* **card-number-test:** rename test suite ([76663e7](http://git/projects/EF/repos/arui-feather/commits/76663e7))
* **cn-test:** add extend class test case ([624e67e](http://git/projects/EF/repos/arui-feather/commits/624e67e))
* **input:** caret position with mask ([6957c04](http://git/projects/EF/repos/arui-feather/commits/6957c04))
* **menu-item-test:** rename test case ([4cf71d8](http://git/projects/EF/repos/arui-feather/commits/4cf71d8))


### Features

* extends support for docs ([13c2388](http://git/projects/EF/repos/arui-feather/commits/13c2388))
* mobile layouts ([171eb89](http://git/projects/EF/repos/arui-feather/commits/171eb89))
* **cn:** tests for cn ([c108539](http://git/projects/EF/repos/arui-feather/commits/c108539))



<a name="0.1.13"></a>
## 0.1.13 (2016-05-29)


### Bug Fixes

* **collapse:** remove unused css ([609bf73](http://git/projects/EF/repos/arui-feather/commits/609bf73))
* **header:** background color ([0d3cc5f](http://git/projects/EF/repos/arui-feather/commits/0d3cc5f))


### Features

* **collapse:** move css to lib ([f1495ad](http://git/projects/EF/repos/arui-feather/commits/f1495ad))



<a name="0.1.12"></a>
## 0.1.12 (2016-05-28)


### Bug Fixes

* **footer:** docs ([0500e73](http://git/projects/EF/repos/arui-feather/commits/0500e73))
* **header:** move css to lib ([3fc6a0c](http://git/projects/EF/repos/arui-feather/commits/3fc6a0c))


### Features

* **footer:** move css to lib ([14934df](http://git/projects/EF/repos/arui-feather/commits/14934df))
* **header:** move css to lib ([066f713](http://git/projects/EF/repos/arui-feather/commits/066f713))
* **heading:** move css to lib ([f5b35d6](http://git/projects/EF/repos/arui-feather/commits/f5b35d6))
* **logo:** move css to lib ([a9ed674](http://git/projects/EF/repos/arui-feather/commits/a9ed674))



<a name="0.1.11"></a>
## 0.1.11 (2016-05-28)


### Bug Fixes

* right icon color in notification ([4e1be2e](http://git/projects/EF/repos/arui-feather/commits/4e1be2e))
* **input-autocomplete:** fix popup width ([b236c8f](http://git/projects/EF/repos/arui-feather/commits/b236c8f))
* **select:** default size as m ([f06ccd8](http://git/projects/EF/repos/arui-feather/commits/f06ccd8))


### Features

* **form:** move css to lib ([6f61457](http://git/projects/EF/repos/arui-feather/commits/6f61457))
* **spin:** move css to lib ([d041c1d](http://git/projects/EF/repos/arui-feather/commits/d041c1d))
* **textarea:** move css to lib ([5331f59](http://git/projects/EF/repos/arui-feather/commits/5331f59))



<a name="0.1.10"></a>
## 0.1.10 (2016-05-26)


### Bug Fixes

* **select:** fix select popup width in width=available mode ([763b071](http://git/projects/EF/repos/arui-feather/commits/763b071))
* **select:** fix size for select demo ([fcf702c](http://git/projects/EF/repos/arui-feather/commits/fcf702c))


### Features

* **select:** move css to lib ([6c97280](http://git/projects/EF/repos/arui-feather/commits/6c97280))



<a name="0.1.9"></a>
## 0.1.9 (2016-05-26)


### Features

* **calendar-input:** option to disable icon ([143c7df](http://git/projects/EF/repos/arui-feather/commits/143c7df))



<a name="0.1.8"></a>
## 0.1.8 (2016-05-26)


### Bug Fixes

* **money-input:** fix methods and vars naming ([240be2b](http://git/projects/EF/repos/arui-feather/commits/240be2b))
* **phone-input:** remove block layout ([eb766d4](http://git/projects/EF/repos/arui-feather/commits/eb766d4))


### Features

* **app:** move css to lib ([3abd21e](http://git/projects/EF/repos/arui-feather/commits/3abd21e))
* **REAMDE.md:** add demo build command ([a318739](http://git/projects/EF/repos/arui-feather/commits/a318739))
* **support:** move css to lib ([dc5f988](http://git/projects/EF/repos/arui-feather/commits/dc5f988))



<a name="0.1.7"></a>
## 0.1.7 (2016-05-26)


### Features

* add Notification ability stick to right ([9d35b42](http://git/projects/EF/repos/arui-feather/commits/9d35b42))
* add spies for testing ([5aa841c](http://git/projects/EF/repos/arui-feather/commits/5aa841c))



<a name="0.1.6"></a>
## 0.1.6 (2016-05-25)


### Bug Fixes

* **icon:** add alfa-on-colored theme docs for icon ([9892fe7](http://git/projects/EF/repos/arui-feather/commits/9892fe7))


### Features

* **calendar-input:** add sizes for calendar-input ([18fc481](http://git/projects/EF/repos/arui-feather/commits/18fc481))



<a name="0.1.5"></a>
## 0.1.5 (2016-05-25)


### Bug Fixes

* jenkins failure ([3776697](http://git/projects/EF/repos/arui-feather/commits/3776697))
* remove all TestUtils.renderIntoDocument ([396f876](http://git/projects/EF/repos/arui-feather/commits/396f876))
* remove unmount in after hook ([e73a183](http://git/projects/EF/repos/arui-feather/commits/e73a183))
* **button:** rightAddons jsdoc ([3ef0fb7](http://git/projects/EF/repos/arui-feather/commits/3ef0fb7))
* **plate:** fix invisible close button on plate ([34e9041](http://git/projects/EF/repos/arui-feather/commits/34e9041))


### Features

* **attach:** move css to lib and fix demo ([fd344b6](http://git/projects/EF/repos/arui-feather/commits/fd344b6))
* **button:** move button to css to lib, add width=available and remove hover with disabled state ([4e46e64](http://git/projects/EF/repos/arui-feather/commits/4e46e64))



<a name="0.1.4"></a>
## 0.1.4 (2016-05-20)


### Bug Fixes

* **page:** fix page bottom padding and move css inside lib ([f8775a9](http://git/projects/EF/repos/arui-feather/commits/f8775a9))


### Features

* period picker ([82aa479](http://git/projects/EF/repos/arui-feather/commits/82aa479))
* **package.json:** move ui from commit hash to version ([8a337ef](http://git/projects/EF/repos/arui-feather/commits/8a337ef))



<a name="0.1.3"></a>
## 0.1.3 (2016-05-18)


### Features

* **button:** additional button views ([fa9fdf4](http://git/projects/EF/repos/arui-feather/commits/fa9fdf4))



<a name="0.1.2"></a>
## 0.1.2 (2016-05-18)


### Bug Fixes

* **money-input:** add support for number in props ([1acffbd](http://git/projects/EF/repos/arui-feather/commits/1acffbd))
* **money-input:** return formatted value with float value on change ([c6861d1](http://git/projects/EF/repos/arui-feather/commits/c6861d1))



<a name="0.1.1"></a>
## 0.1.1 (2016-05-12)


### Bug Fixes

* **cn:** add missed dependency ([d629469](http://git/projects/EF/repos/arui-feather/commits/d629469))
* **social:** add key ([5fd1802](http://git/projects/EF/repos/arui-feather/commits/5fd1802))


### Features

* update icon on demo page ([fdeba87](http://git/projects/EF/repos/arui-feather/commits/fdeba87))



<a name="0.1.0"></a>
# 0.1.0 (2016-05-10)


### Bug Fixes

* add keys to iterated children and components in array ([cbc350c](http://git/projects/EF/repos/arui-feather/commits/cbc350c))
* add keys to iterated children in right way ([789a9c0](http://git/projects/EF/repos/arui-feather/commits/789a9c0))
* **calendar:** remove calendar outline on focus ([2f36e4c](http://git/projects/EF/repos/arui-feather/commits/2f36e4c))
* **menu:** fix mutation property ([a2f2ad3](http://git/projects/EF/repos/arui-feather/commits/a2f2ad3))


### Features

* up react js version to 15 ([9ee4b57](http://git/projects/EF/repos/arui-feather/commits/9ee4b57))
* **package.json:** move to convention-changlelog-cli ([7dd3d09](http://git/projects/EF/repos/arui-feather/commits/7dd3d09))
* **README.md:** add demo link ([8d1520b](http://git/projects/EF/repos/arui-feather/commits/8d1520b))
* **user, icon:** new version of user ([7653fb7](http://git/projects/EF/repos/arui-feather/commits/7653fb7))



<a name="0.0.6"></a>
## 0.0.6 (2016-04-29)


### Features

* add build dockerfile ([24b10f2](http://git/projects/EF/repos/arui-feather/commits/24b10f2))
* add compression for production build ([66e1991](http://git/projects/EF/repos/arui-feather/commits/66e1991))
* add eslint code style checker and code style fixes ([ad30a1b](http://git/projects/EF/repos/arui-feather/commits/ad30a1b))



<a name="0.0.5"></a>
## 0.0.5 (2016-04-21)


### Bug Fixes

* **input-autocomplete:** check items fix ([73c8135](http://git/projects/EF/repos/arui-feather/commits/73c8135))
* **input-autocomplete:** fix checked elems ([c59d1c3](http://git/projects/EF/repos/arui-feather/commits/c59d1c3))
* **input-autocomplete:** fix docs ([bef1907](http://git/projects/EF/repos/arui-feather/commits/bef1907))
* **input-autocomplete:** highlighted item in props ([dbc0214](http://git/projects/EF/repos/arui-feather/commits/dbc0214))
* **input-autocomplete:** remove filter logic from input ([68ec14c](http://git/projects/EF/repos/arui-feather/commits/68ec14c))


### Features

* add ability to render custom component in options in select and input-autocomplete ([d92a598](http://git/projects/EF/repos/arui-feather/commits/d92a598))
* improve tools/lib-structure interface ([f7ed94f](http://git/projects/EF/repos/arui-feather/commits/f7ed94f))
* remove fonts from css with production build ([ee3974c](http://git/projects/EF/repos/arui-feather/commits/ee3974c))
* **input:** add autocomplete ([8861971](http://git/projects/EF/repos/arui-feather/commits/8861971))
* **input:** add error popup to input ([c32205e](http://git/projects/EF/repos/arui-feather/commits/c32205e))
* **README.md:** add jira link ([a96f315](http://git/projects/EF/repos/arui-feather/commits/a96f315))



<a name="0.0.4"></a>
## 0.0.4 (2016-04-18)


### Bug Fixes

* **resize-sensor-test:** fix resize sensor test ([a2245ae](http://git/projects/EF/repos/arui-feather/commits/a2245ae))


### Features

* move polyfills to webpack build ([41dfb8f](http://git/projects/EF/repos/arui-feather/commits/41dfb8f))
* **input:** add addons to input ([e99bd55](http://git/projects/EF/repos/arui-feather/commits/e99bd55))



<a name="0.0.3"></a>
## 0.0.3 (2016-04-11)


### Bug Fixes

* **cn:** fix props and context in closure ([61b44f7](http://git/projects/EF/repos/arui-feather/commits/61b44f7))
* **cn:** remove 'if (!this._cn)' condition ([b591ed6](http://git/projects/EF/repos/arui-feather/commits/b591ed6))
* **collapse:** fix update content height function ([0b2e1b7](http://git/projects/EF/repos/arui-feather/commits/0b2e1b7))
* **resize-sensor-test:** fix resize sensor test ([efdc564](http://git/projects/EF/repos/arui-feather/commits/efdc564))


### Features

* new components: AppTitle, AppMenu and AppContent ([e36068f](http://git/projects/EF/repos/arui-feather/commits/e36068f))
* **button:** proxy events for button ([2adecf7](http://git/projects/EF/repos/arui-feather/commits/2adecf7))
* **package.json:** release scripts ([8d91831](http://git/projects/EF/repos/arui-feather/commits/8d91831))



<a name="0.0.2"></a>
## 0.0.2 (2016-04-10)


### Bug Fixes

* add tests sourcemaps ([1629940](http://git/projects/EF/repos/arui-feather/commits/1629940))
* delete jshint some rules, fis bugs ([96c20f6](http://git/projects/EF/repos/arui-feather/commits/96c20f6))
* fix all css requires  in components ([82e879e](http://git/projects/EF/repos/arui-feather/commits/82e879e))
* fix autobind decorator in phantomjs ([7125ed6](http://git/projects/EF/repos/arui-feather/commits/7125ed6))
* fix jscs & jshint bugs ([5f78779](http://git/projects/EF/repos/arui-feather/commits/5f78779))
* multi select text ([a85448f](http://git/projects/EF/repos/arui-feather/commits/a85448f))
* remove bem-css-loader ([843e386](http://git/projects/EF/repos/arui-feather/commits/843e386))
* remove padding from button-checkbox and button-radio ([938155a](http://git/projects/EF/repos/arui-feather/commits/938155a))
* remove placeholder from masked input ([86788db](http://git/projects/EF/repos/arui-feather/commits/86788db))
* rename tests files for form, email-input and phone-input ([32859dc](http://git/projects/EF/repos/arui-feather/commits/32859dc))
* translate mouse events in jsdoc description ([ee2cc4e](http://git/projects/EF/repos/arui-feather/commits/ee2cc4e))
* **bem-css-loader:** fix components requires ([c5154ad](http://git/projects/EF/repos/arui-feather/commits/c5154ad))
* **button:** hack for undefined is not an object (evaluating 'this.setState') for button ([0462723](http://git/projects/EF/repos/arui-feather/commits/0462723))
* **calendar-input:** hide calendar when user blur input using tab key ([1a84e53](http://git/projects/EF/repos/arui-feather/commits/1a84e53))
* **card-number:** fix demo ([593d933](http://git/projects/EF/repos/arui-feather/commits/593d933))
* **collapse:** fix first click event ([5542edb](http://git/projects/EF/repos/arui-feather/commits/5542edb))
* **copyright:** rename tests file ([8178d32](http://git/projects/EF/repos/arui-feather/commits/8178d32))
* **demo:** compononents in alphabetic order ([2faebc7](http://git/projects/EF/repos/arui-feather/commits/2faebc7))
* **demo:** scroll menu, code style ([50da613](http://git/projects/EF/repos/arui-feather/commits/50da613))
* **email-input:** fix email input tests typo ([cb4726c](http://git/projects/EF/repos/arui-feather/commits/cb4726c))
* **form-field:** add prop view ([45b0254](http://git/projects/EF/repos/arui-feather/commits/45b0254))
* **form-field:** fix for server render ([f9e3fea](http://git/projects/EF/repos/arui-feather/commits/f9e3fea))
* **lib:** rename lib file ([2a5f014](http://git/projects/EF/repos/arui-feather/commits/2a5f014))
* **main.css:** fix path to fonts ([10b843c](http://git/projects/EF/repos/arui-feather/commits/10b843c))
* **menu:** fix server side render for menu ([de43b5e](http://git/projects/EF/repos/arui-feather/commits/de43b5e))
* **menu:** key generation for items ([78bc156](http://git/projects/EF/repos/arui-feather/commits/78bc156))
* **menu:** menu item onclick ([ddf7ee5](http://git/projects/EF/repos/arui-feather/commits/ddf7ee5))
* **menu:** menu-item click, when mode is basic ([2fc31d5](http://git/projects/EF/repos/arui-feather/commits/2fc31d5))
* **notification:** throw error when no position set ([66da017](http://git/projects/EF/repos/arui-feather/commits/66da017))
* **package.json:** move polyfills to deps ([d2a2f1a](http://git/projects/EF/repos/arui-feather/commits/d2a2f1a))
* **package.json:** move postcss-mixins to dev dependencies ([23f8d8a](http://git/projects/EF/repos/arui-feather/commits/23f8d8a))
* **plate:** plate styles fix ([3b24a37](http://git/projects/EF/repos/arui-feather/commits/3b24a37))
* **popup:** fix rebase ([45f9cef](http://git/projects/EF/repos/arui-feather/commits/45f9cef))
* **select:** docs fix ([bcfb247](http://git/projects/EF/repos/arui-feather/commits/bcfb247))
* **select:** fix click outside test ([612acaf](http://git/projects/EF/repos/arui-feather/commits/612acaf))
* **spin:** fix spin in button demo ([5bae2d1](http://git/projects/EF/repos/arui-feather/commits/5bae2d1))
* **textarea:** add default size ([5f235a4](http://git/projects/EF/repos/arui-feather/commits/5f235a4))
* **textarea:** textarea tests fix ([979200d](http://git/projects/EF/repos/arui-feather/commits/979200d))
* **webpack:** add hot reload plugin only for dev server mode ([a69a2ca](http://git/projects/EF/repos/arui-feather/commits/a69a2ca))
* **webpack:** correct paths for bem-css-loader ([92c966a](http://git/projects/EF/repos/arui-feather/commits/92c966a))
* **webpack.config:** move devtools to local build ([11d8536](http://git/projects/EF/repos/arui-feather/commits/11d8536))
* **webpack.config:** move optimizations to local config ([038d3ee](http://git/projects/EF/repos/arui-feather/commits/038d3ee))
* **webpack.config.template:** remove test specifix case from template ([1951040](http://git/projects/EF/repos/arui-feather/commits/1951040))


### Features

* add editorconfig file ([38c8cb2](http://git/projects/EF/repos/arui-feather/commits/38c8cb2))
* add Object.assign polyfill ([b6db753](http://git/projects/EF/repos/arui-feather/commits/b6db753))
* add tests for Footer, Hightlight, Label, Social, Spin ([a9c723d](http://git/projects/EF/repos/arui-feather/commits/a9c723d))
* add tools for coding style checks ([a7867fb](http://git/projects/EF/repos/arui-feather/commits/a7867fb))
* build css seperate ([39a0b2e](http://git/projects/EF/repos/arui-feather/commits/39a0b2e))
* docs ([a50f109](http://git/projects/EF/repos/arui-feather/commits/a50f109))
* run tests inside docker ([3513b37](http://git/projects/EF/repos/arui-feather/commits/3513b37))
* update README ([c8809f2](http://git/projects/EF/repos/arui-feather/commits/c8809f2))
* **attach:** add attach component ([ecf9189](http://git/projects/EF/repos/arui-feather/commits/ecf9189))
* **attach:** add attach tests ([1a55d01](http://git/projects/EF/repos/arui-feather/commits/1a55d01))
* **button:** button enter, space key handlers ([7c8e7a2](http://git/projects/EF/repos/arui-feather/commits/7c8e7a2))
* **calendar:** add calendar component ([bff8946](http://git/projects/EF/repos/arui-feather/commits/bff8946))
* **calendar:** calendar arrows navigation ([141cdc2](http://git/projects/EF/repos/arui-feather/commits/141cdc2))
* **calendar:** rebase ([4007abf](http://git/projects/EF/repos/arui-feather/commits/4007abf))
* **calendar-input:** add calendar component-input ([bd984f8](http://git/projects/EF/repos/arui-feather/commits/bd984f8))
* **calendar-input:** calendar input tests ([c49302d](http://git/projects/EF/repos/arui-feather/commits/c49302d))
* **card-input:** car input tests ([145e3dd](http://git/projects/EF/repos/arui-feather/commits/145e3dd))
* **card-number:** add card-number component ([0c67427](http://git/projects/EF/repos/arui-feather/commits/0c67427))
* **card-number:** add test ([f076513](http://git/projects/EF/repos/arui-feather/commits/f076513))
* **checkbox:** add checkbox component ([1ca5f7b](http://git/projects/EF/repos/arui-feather/commits/1ca5f7b))
* **checkbox:** add checkbox group component ([a71a939](http://git/projects/EF/repos/arui-feather/commits/a71a939))
* **checkbox:** add checkbox, checkbox group tests ([991e19d](http://git/projects/EF/repos/arui-feather/commits/991e19d))
* **collapse:** add collapse component ([0d40ca6](http://git/projects/EF/repos/arui-feather/commits/0d40ca6))
* **collapse:** add collapse test ([a351df3](http://git/projects/EF/repos/arui-feather/commits/a351df3))
* **copyright:** add copyright component ([a9caca9](http://git/projects/EF/repos/arui-feather/commits/a9caca9))
* **copyright:** copyright tests ([25fae41](http://git/projects/EF/repos/arui-feather/commits/25fae41))
* **coverage:** normal coverage ([82bf878](http://git/projects/EF/repos/arui-feather/commits/82bf878))
* **dropdown:** add dropdown test ([f632f2e](http://git/projects/EF/repos/arui-feather/commits/f632f2e))
* **email-input:** email input tests ([e11df48](http://git/projects/EF/repos/arui-feather/commits/e11df48))
* **footer:** add footer ([fcaaa1f](http://git/projects/EF/repos/arui-feather/commits/fcaaa1f))
* **footer:** add link ([8c97f0e](http://git/projects/EF/repos/arui-feather/commits/8c97f0e))
* **form:** add form component ([36c1b3b](http://git/projects/EF/repos/arui-feather/commits/36c1b3b))
* **form:** form tests ([28189a4](http://git/projects/EF/repos/arui-feather/commits/28189a4))
* **form-field:** add form-field component ([f296ef1](http://git/projects/EF/repos/arui-feather/commits/f296ef1))
* **header:** fix logo mixin ([90d6c33](http://git/projects/EF/repos/arui-feather/commits/90d6c33))
* **header, page, link:** add new components and improve existing ([a5b54a4](http://git/projects/EF/repos/arui-feather/commits/a5b54a4))
* **heading:** add heading component ([1855ab6](http://git/projects/EF/repos/arui-feather/commits/1855ab6))
* **heading:** add heading test ([c802aad](http://git/projects/EF/repos/arui-feather/commits/c802aad))
* **highlight:** add highlight component ([b5e91a1](http://git/projects/EF/repos/arui-feather/commits/b5e91a1))
* **icon:** add icon component ([f359595](http://git/projects/EF/repos/arui-feather/commits/f359595))
* **icon:** add test for icon ([0ee3ba2](http://git/projects/EF/repos/arui-feather/commits/0ee3ba2))
* **icon, user:** proxy events ([bbfc6c8](http://git/projects/EF/repos/arui-feather/commits/bbfc6c8))
* **input:** add input test ([e2981ff](http://git/projects/EF/repos/arui-feather/commits/e2981ff))
* **input:** add mask ([9a1f848](http://git/projects/EF/repos/arui-feather/commits/9a1f848))
* **input:** add radio, radio group test ([b1364b4](http://git/projects/EF/repos/arui-feather/commits/b1364b4))
* **label:** add label component ([fdf3907](http://git/projects/EF/repos/arui-feather/commits/fdf3907))
* **link:** add link tests ([9809785](http://git/projects/EF/repos/arui-feather/commits/9809785))
* **link:** add proxy events to all callbacks for link ([8fdc750](http://git/projects/EF/repos/arui-feather/commits/8fdc750))
* **list:** add list component ([1ca86f6](http://git/projects/EF/repos/arui-feather/commits/1ca86f6))
* **list:** add test for list component ([02e6fd8](http://git/projects/EF/repos/arui-feather/commits/02e6fd8))
* **menu:** add events proxy ([f3fefad](http://git/projects/EF/repos/arui-feather/commits/f3fefad))
* **menu:** add menu, menu-item, dropdown components ([26f23bb](http://git/projects/EF/repos/arui-feather/commits/26f23bb))
* **menu:** add onItemClick ([0bef0ce](http://git/projects/EF/repos/arui-feather/commits/0bef0ce))
* **menu:** add test, add es6 find polyfill ([1bef4b3](http://git/projects/EF/repos/arui-feather/commits/1bef4b3))
* **menu:** menu item props merge in menu component ([eeb23f8](http://git/projects/EF/repos/arui-feather/commits/eeb23f8))
* **menu-item:** proxy events for menu-item callbacks ([1343440](http://git/projects/EF/repos/arui-feather/commits/1343440))
* **message:** add message component ([5e380f4](http://git/projects/EF/repos/arui-feather/commits/5e380f4))
* **message:** add message tests ([4dc67e2](http://git/projects/EF/repos/arui-feather/commits/4dc67e2))
* **MIGRATION.md:** migration guide for legacy stuff ([6d0ed0b](http://git/projects/EF/repos/arui-feather/commits/6d0ed0b))
* **money-input:** money input tests ([296dcfa](http://git/projects/EF/repos/arui-feather/commits/296dcfa))
* **notification:** add notification component ([2de1c24](http://git/projects/EF/repos/arui-feather/commits/2de1c24))
* **page:** add page component ([ac6d423](http://git/projects/EF/repos/arui-feather/commits/ac6d423))
* **page:** add page tests ([61b8aa4](http://git/projects/EF/repos/arui-feather/commits/61b8aa4))
* **page:** replace image with css gradient ([a852e80](http://git/projects/EF/repos/arui-feather/commits/a852e80))
* **paragraph:** add paragraph component ([1a96037](http://git/projects/EF/repos/arui-feather/commits/1a96037))
* **paragraph:** add test and fix property name ([35f28dc](http://git/projects/EF/repos/arui-feather/commits/35f28dc))
* **phone-input:** phone input tests ([8ba89ea](http://git/projects/EF/repos/arui-feather/commits/8ba89ea))
* **plate:** add plate component ([8c95461](http://git/projects/EF/repos/arui-feather/commits/8c95461))
* **plate:** notification tests ([f0ae2a2](http://git/projects/EF/repos/arui-feather/commits/f0ae2a2))
* **plate:** plate tests ([8abfee2](http://git/projects/EF/repos/arui-feather/commits/8abfee2))
* **popup:** add popup component ([c694f2c](http://git/projects/EF/repos/arui-feather/commits/c694f2c))
* **popup:** add popup component ([3ee66fd](http://git/projects/EF/repos/arui-feather/commits/3ee66fd))
* **radio, radio-group:** add radio and radio-group component ([95ce472](http://git/projects/EF/repos/arui-feather/commits/95ce472))
* **README.md:** docs about docs ([03153da](http://git/projects/EF/repos/arui-feather/commits/03153da))
* **README.md:** update info about tests ([ae0a96f](http://git/projects/EF/repos/arui-feather/commits/ae0a96f))
* **render-in-body:** render-in-body tests ([49ea5b9](http://git/projects/EF/repos/arui-feather/commits/49ea5b9))
* **resize-sensor:** add ResizeSensor test ([91c7fdc](http://git/projects/EF/repos/arui-feather/commits/91c7fdc))
* **select:** add ability to render custom content for select button ([c1d68ce](http://git/projects/EF/repos/arui-feather/commits/c1d68ce))
* **select:** add test, add removing popup from document after each test ([de159a5](http://git/projects/EF/repos/arui-feather/commits/de159a5))
* **select:** fix select timeout ([8424955](http://git/projects/EF/repos/arui-feather/commits/8424955))
* **select:** select arrows navigation ([0450b02](http://git/projects/EF/repos/arui-feather/commits/0450b02))
* **spin:** add spin component ([8aa8956](http://git/projects/EF/repos/arui-feather/commits/8aa8956))
* **support:** add support component ([437da65](http://git/projects/EF/repos/arui-feather/commits/437da65))
* **textarea:** add default textarea size ([53936a1](http://git/projects/EF/repos/arui-feather/commits/53936a1))
* **textarea:** add textatea test ([765c07e](http://git/projects/EF/repos/arui-feather/commits/765c07e))
* **user:** add user component ([64c7b07](http://git/projects/EF/repos/arui-feather/commits/64c7b07))
* **user:** user tests ([46abe3f](http://git/projects/EF/repos/arui-feather/commits/46abe3f))
* **webpack:** add support for media queries ([65d8425](http://git/projects/EF/repos/arui-feather/commits/65d8425))
* **webpack:** seperate webpack common template ([6b07749](http://git/projects/EF/repos/arui-feather/commits/6b07749))
* **webpack.config.js:** production minified build ([ea9978e](http://git/projects/EF/repos/arui-feather/commits/ea9978e))



