const path = require('path');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.template.js');

Object.assign(
    webpackConfig.module.loaders.find(loader => loader.loader === 'babel'),
    {
        include: [
            path.resolve(__dirname, 'node_modules', 'gemini'),
            path.resolve(__dirname, 'gemini'),
            path.resolve(__dirname, 'src')
        ]
    }
);

webpackConfig.devtool = 'eval';
webpackConfig.plugins.push(
    new webpack.EnvironmentPlugin([
        'ALL_SIZES'
    ])
);

module.exports = webpackConfig;
