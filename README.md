Alfa Bank UI lightweight library (Beta)
=======================================

Как запустить?
--------------

Необходимые внешние зависимости системы:

`node` 5+ и `npm` (https://nodejs.org/en/).

Шаги запуска демо:

1. `npm i`
2. `npm run demo`

Запустить demo на произвольном порту:

`PORT=8888 npm run demo`

Запустить demo для отдельных компонентов:

`COMPONENTS="calendar-input, popup" npm run demo`

Запустить demo с автоматичесим обновлением содержимого страницы при изменении компонента:

`COMPONENTS="calendar-input, popup" HOT_LOADER=1 npm run demo `

Использовать только для обновления нескольких компонентов. Иначе не хватает памяти.

Автоматические проверки кода
----------------------------

Перед каждым коммитом js код проверяется через `eslint`, css через `stylelint`.

Ручной запуск проверки кода линтерами и юнит тестами `npm run quality-check`.

Запуск unit-тестов `npm run test`.

Запуск unit-тестов для определенных компонентов `TESTS=amount,calendar npm run test`.

Запуск unit-тестов используя Chrome `npm run test -- --browsers=Chrome`

Запуск тестов призводительности `npm run karma-benchmark`.

Запуск линтера для css `npm run lint-css`.

Запуск линтера для js `npm run lint-js`.

[Запуск тестов на мобильных устройствах](./README_MOBILE.md#mobile-testing).

Регрессионное тестирование
--------------------------

Для тестирования деградаций в вёрстке используется [`gemini`](https://github.com/gemini-testing/gemini).

Тесты для каждого компонента хранятся в `gemini/<component>.gemini.js`.

Запуск тестов производится командой `npm run gemini`.

Обновление или создание новых эталонных скриншотов: `npm run gemini-update`.

Запуск `gemini` на выборочное число сьютов (используется опция `--grep` c JS RegExp): `SUITES='button|select' npm run gemini`.

Во время тестирования рендер каждого сьюта доступен по URL, например: `http://localhost:8888/popup/popup_theme_alfa-on-color.popup_size_m.popup_prop-set_1`

Минификация svg
---------------

Для оптимизация svg используется [svgo](https://github.com/svg/svgo)
```
npm install svgo -g
find src -name *.svg -print0 | xargs -0 -L 1 svgo
```

Документацая по компонентам
---------------------------

Документация генерируется на основе кода библиотеки и комментариев в формате jsdoc.

Перед каждым коммитом документация автоматически перегенирируется.

[Референс по всем блокам](./docs)

Ручной запуск перегенерации документации: `npm run build-docs`.

Использование в TypeScript проектах
-----------------------------------

[Подробнее о миграции](./MIGRATION.md)

Правила контрибуции
-------------------

[Подробнее о том, как контрибьютить в проект](./CONTRIBUTION.md)

Группа поддержки в Slack
------------------------

https://alfabank.slack.com/messages/arui-feather/

Стрим библиотеки в Jira
-----------------------

http://jira/browse/ARUIF/

Демо проекта
------------

[http://mannodeint2/arui-feather-ui/](http://mannodeint2/arui-feather-ui/)

Сборка демо проекта: `NODE_ENV=production npm run build`

Оптимизация производительности компонентов
-------------------------------------------
Для оптимизации производительности компонентов используется метод
[shouldComponentUpdate](https://facebook.github.io/react/docs/advanced-performance.html#avoiding-reconciling-the-dom),
реализуемый декоратором [@performance](./src/performance.js).

Пример использования:
```
import performance from '../performance';

@performance(true)
class Component extends React.Component {}
```

Использование Modernizr
-----------------------
Поскольку modernizr на данный момент не умеет правильно работать внутри webpack
используется postinstall хук, который собирает правильную версию modernizr при установке библиотеки
и кладет ее в `tools/modernizr`.

Настройки modernizr находятся в файле `.modernizrrc`. [Список доступных опций](https://github.com/Modernizr/Modernizr/blob/master/lib/config-all.json)

Для ручного обновления собранного modernizr
```
npm run modernizr
```

Поддерживаемые браузеры
-----------------------

**Desktop**

  * Chrome *(две последние стабильные версии)*
  * Edge *(две последние стабильные версии)*
  * Firefox *(две последние стабильные версии)*
  * IE 9+
  * Opera *(две последние стабильные версии)*
  * Safari *(две последние стабильные версии)*
  * Yandex *(две последние стабильные версии)*

**Mobile**

  * Android 4+
  * iOS *(две последние стабильные версии)*

Мобильная вёрстка
-----------------
[Использование в адаптивной/мобильной среде](./README_MOBILE.md)
