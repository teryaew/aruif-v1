'use strict';

const libStructure = require('./tools/lib-structure');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const rimraf = require('rimraf');

const DOCS_PATH = './docs';
const TEMPLATE_INDEX = fs.readFileSync('./tools/docs-templates/index.md.ejs').toString();
const TEMPLATE_COMPONENT = fs.readFileSync('./tools/docs-templates/component.md.ejs').toString();
const COMPONENTS_PATH = path.join(__dirname, 'src');

if (fs.existsSync(DOCS_PATH)) {
    rimraf.sync(DOCS_PATH);
}
fs.mkdirSync(DOCS_PATH);

const componentsNames = libStructure.getComponents(COMPONENTS_PATH, ['lib']);

const components = componentsNames
    .map(componentName => {
        let filePath = path.join(COMPONENTS_PATH, componentName, `${componentName}.jsx`);
        return libStructure.getDocForFile(filePath, componentName);
    });

components
    .forEach(component => {
        fs.mkdir(path.join(DOCS_PATH, component.source), () => {
            fs.writeFile(
                path.resolve(DOCS_PATH, component.source, 'README.md'),
                ejs.render(TEMPLATE_COMPONENT, { component: component }),
                err => { if (err) throw err; }
            );
        });
    });

fs.writeFile(
    path.resolve(DOCS_PATH, 'README.md'),
    ejs.render(TEMPLATE_INDEX, { components: components }),
    err => { if (err) throw err; }
);
