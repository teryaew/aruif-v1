const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const ASSETS_BASE_QUERY = {
    name: '[name].[hash].[ext]',
    limit: 10000
};

module.exports = {
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader'),
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'application/font-woff' }, ASSETS_BASE_QUERY)
            },
            {
                test: /\.(ttf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'application/octet-stream' }, ASSETS_BASE_QUERY)
            },
            {
                test: /\.(jpe?g)$/i,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'image/jpeg' }, ASSETS_BASE_QUERY)
            },
            {
                test: /\.(png)$/i,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'image/png' }, ASSETS_BASE_QUERY)
            },
            {
                test: /\.(gif)$/i,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'image/gif' }, ASSETS_BASE_QUERY)
            },
            {
                test: /\.(svg)$/i,
                loader: 'url-loader',
                query: Object.assign({ mimetype: 'image/svg+xml' }, ASSETS_BASE_QUERY)
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            sourceMap: false,
            warnings: false
        }),
        new ExtractTextPlugin('[name]/index.css'),
        new CompressionPlugin({
            asset: '[file].gz',
            algorithm: 'gzip',
            regExp: /\.js$|\.css$|\.ttf$|\.svg$/,
            threshold: 10240,
            minRatio: 0.8
        })
    ]
};
