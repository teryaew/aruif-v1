# User

Компонент доступа к пользовательскому профилю: cодержит имя пользователя и кнопку "Выйти".

```javascript
import User from 'arui-feather/src/user/user';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| url | String | `'#'`  |  | href ссылки с именем пользователя |
| text | String |  |  | Имя пользователя |
| icon | Node |  |  | Иконка пользователя |
| onClick | Function |  |  | Обработчик клика по пользователю |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



