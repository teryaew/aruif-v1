# Form

Компонент формы

```javascript
import Form from 'arui-feather/src/form/form';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| align | [AlignEnum](#AlignEnum) |  |  | Выравнивание поля в форме // TODO @teryaew: think about it |
| enctype | [EnctypeEnum](#EnctypeEnum) | `'application/x-www-form-urlencoded'`  |  | Способ кодирования данных формы при их отправке |
| action | String | `'/'`  |  | Адрес отправки данных на сервер |
| method | [MethodEnum](#MethodEnum) | `'post'`  |  | Метод запроса |
| view | [ViewEnum](#ViewEnum) |  |  | Тип формы |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| header | Node |  |  | Заголовок для формы |
| footer | Node |  |  | Футер для формы |
| noValidate | Boolean | `false`  |  | Управление встроенным в браузер механизмом валидации формы |
| autocomplete | Boolean | `true`  |  | Управление автозаполнением формы |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| id | String |  |  | Идентификатор компонента в DOM |
| name | String |  |  | Имя компонента в DOM |
| onSubmit | Function |  |  | Обработчик отправки формы |







## Типы






### <a id="AlignEnum"></a>AlignEnum

 * `'left'`
 * `'center'`
 * `'right'`
 * `'justify'`


### <a id="EnctypeEnum"></a>EnctypeEnum

 * `'application/x-www-form-urlencoded'`
 * `'multipart/form-data'`
 * `'text/plain'`


### <a id="MethodEnum"></a>MethodEnum

 * `'post'`
 * `'get'`


### <a id="ViewEnum"></a>ViewEnum

 * `'line'`
 * `'normal'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



