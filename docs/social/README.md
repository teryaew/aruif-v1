# Social

Компонент отображающий "иконостас" социальных сетей.
По-умолчанию отображает следующие социальные сети: Vk, Facebook, Twitter.

```javascript
import Social from 'arui-feather/src/social/social';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| networks | Array.<[NetworksType](#NetworksType)> | `[<br>    { name: 'vk', url: 'https://vk.com/alfabank' },<br>    { name: 'facebook', url: 'https://facebook.com/alfabank' },<br>    { name: 'twitter', url: 'https://twitter.com/alfabank' }<br>]`  |  | Список социальных сетей |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы




### <a id="NetworksType"></a>NetworksType

| Prop  | Тип  | Описание |
| ----- | ---- |----------|
| name | String |  |
| url | String |  |







### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



