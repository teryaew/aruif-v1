# Sidebar

Компонент боковой панели aka холодильник.

```javascript
import Sidebar from 'arui-feather/src/sidebar/sidebar';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| children | Node |  |  | Дочерние компоненты |
| showCloser | Устарело |  |  | Используйте `hasCloser`. Признак для отрисовки элемента закрытия |
| hasCloser | Boolean | `true`  |  | Признак для отрисовки элемента закрытия |
| visible | Boolean |  | Да | Признак появления холодильника |
| onCloserClick | Function |  |  | Обработчик клика на элемент закрытия |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



