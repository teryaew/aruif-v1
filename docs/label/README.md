# Label

Компонента лейбла.

```javascript
import Label from 'arui-feather/src/label/label';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| minor | Boolean | `false`  |  | Отображение как второстепенная информация |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| isNoWrap | Boolean | `false`  |  | Управление возможностью рендерить компонент в одну сроку |







## Типы






### <a id="SizeEnum"></a>SizeEnum

 * `'xs'`
 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



