# RenderInContainer

Компонент, позволяющий визуализировать другие компоненты в произвольном контейнере.

```javascript
import RenderInContainer from 'arui-feather/src/render-in-container/render-in-container';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| onRender | Function |  |  | Callback на рендер компонента |
| className | any |  |  | Дополнительный класс |
| container | HtmlElement |  |  | Контейнер, в котором будет визуализирован компонент |





## Публичные методы
| Метод  | Описание |
| ------ | -------- |
| getNode(): HTMLElement | Возвращает HTMLElement враппера компонента. |
| getContainer(): HTMLElement | Возвращает HTMLElement контейнера, в который отрендерился компонент. |









