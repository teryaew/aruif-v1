# Support

Компонент с информацией о поддержке для клиентов.
Включает в себя город и телефон.

```javascript
import Support from 'arui-feather/src/support/support';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| city | String |  |  | Название города |
| phone | String |  |  | Номер телефона |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onCityClick | Function |  |  | Обработчик клика по городу |
| onPhoneClick | Function |  |  | Обработчик клика по телефону |







## Типы






### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



