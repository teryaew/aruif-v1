# Heading

Компонент заголовка.

```javascript
import Heading from 'arui-feather/src/heading/heading';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| align | [AlignEnum](#AlignEnum) |  |  | Выравнивание поля в форме // TODO @teryaew: think about it |
| className | any |  |  | Дополнительный класс |
| size | [SizeEnum](#SizeEnum) | `'xl'`  |  | Размер, определяющий какой тег заголовка будет использоваться |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |







## Типы






### <a id="AlignEnum"></a>AlignEnum

 * `'left'`
 * `'center'`
 * `'right'`
 * `'justify'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



