# Plate

Компонент плашки.

```javascript
import Plate from 'arui-feather/src/plate/plate';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| hasCloser | Boolean | `false`  |  | Управление наличием закрывающего крестика |
| hasClear | Устарело |  |  | Используйте `hasCloser`. Управляет наличием закрывающего крестика |
| isFlat | Boolean |  |  | Плоская тема |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onClick | Function |  |  | Обработчик клика по плашке |
| onCloserClick | Function |  |  | Обработчик клика по крестику |
| onCloseClick | Устарело |  |  | Используйте `onCloserClick`. Обработчик клика по крестику |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



