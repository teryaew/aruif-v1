# ErrorPage

Компонент страницы ошибки.
Как правило является корневым компонентом страницы.
Используется вместо компонента Page.

```javascript
import ErrorPage from 'arui-feather/src/error-page/error-page';
import Header from 'arui-feather/src/header/header';

<ErrorPage
     returnUrl='/login'
     header={ <Header /> }
/>
```

```javascript
import ErrorPage from 'arui-feather/src/error-page/error-page';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| title | Node\|String | `<FormattedMessage<br>    id='errorPage.title'<br>    defaultMessage='An error has occurred'<br>/>`  |  | Заголовок ошибки |
| text | Node\|String | `<FormattedMessage<br>    id='errorPage.text'<br>    defaultMessage='Please try again after some time.'<br>/>`  |  | Сообщение ошибки |
| header | Node |  |  | Шапка страницы |
| returnUrl | String |  |  | href для ссылки 'Вернуться в интернет-банк' |
| returnTitle | Node\|String | `<FormattedMessage<br>    id='errorPage.returnTitle'<br>    defaultMessage='Return to Internet-Bank'<br>/>`  |  | Альтернативный текст для ссылки 'Вернуться в интернет-банк' |











