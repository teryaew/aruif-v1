# Modal



```javascript
import Modal from 'arui-feather/src/modal/modal';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| hasCloser | Boolean | `false`  |  | Управление наличием закрывающего крестика |
| visible | Boolean | `true`  | Да | Управление видимостью компонента |
| autoclosable | Boolean | `true`  |  | Управление возможностью автозакрытия компонента |
| height | [HeightEnum](#HeightEnum) | `'auto'`  |  | Управляет высотой модального окна |
| onClose | Function |  | Да | Обработчик закрытия компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="HeightEnum"></a>HeightEnum

 * `'auto'`
 * `'available'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



