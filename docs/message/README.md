# Message

Компонент сообщения.

```javascript
import Message from 'arui-feather/src/message/message';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| type | [TypeEnum](#TypeEnum) | `'text'`  |  | Тип компонента |
| visible | Boolean |  |  | Управление видимостью компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="TypeEnum"></a>TypeEnum

 * `'text'`
 * `'popup'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



