# Icon

Компонент иконки. Содержит в себе пресеты самых популярных иконок.

С компонентов иконки идет набор иконок, разбитый по группам:

* `action` - иконки различный действий
* `bank` - логотипы банков
* `card` - логотипы платежных систем
* `format` - иконки форматов файлов
* `currency` - иконки валют
* `tool` - иконки инструментов
* `network` - иконки социальных сетей
* `user` - иконки имеющие отношение к пользователю
* `category` - иконки платежных категорий

Все наборы иконок идут в двух цветовых темах `alfa-on-color` и `alfa-on-white`.

Для набора иконок `action`, `bank` и `card`, также есть цветной вариант иконок,
реализуемый темой `alfa-on-colored`.

По-умолчанию в финальную сборку вашего проекта попадают все наборы иконок во всех
цветовых схемах. Если вы хотите облегчить сборку и исключить неиcпользуемый набор
иконок, то используйте plugin `webpack.NormalModuleReplacementPlugin` из стандартной
поставки `webpack` с пакетом `node-noop`.

```
    npm i --save-dev node-noop
```

```
   // Примеры:

   webpackConfig.plugins.push(
        // Исключает из сборки иконки действий для белого фона
        new webpack.NormalModuleReplacementPlugin(/icon_action_alfa-on-white\.css$/, 'node-noop'),

        // Исключает из сборки иконки действий для цветного фона
        new webpack.NormalModuleReplacementPlugin(/icon_action_alfa-on-colored\.css$/, 'node-noop')
   );
```

```javascript
import Icon from 'arui-feather/src/icon/icon';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| action | [ActionEnum](#ActionEnum) |  |  | Тип иконки |
| bank | [BankEnum](#BankEnum) |  |  | Тип иконки |
| card | [CardEnum](#CardEnum) |  |  | Тип иконки |
| format | [FormatEnum](#FormatEnum) |  |  | Тип иконки |
| currency | [CurrencyEnum](#CurrencyEnum) |  |  | Тип иконки |
| tool | [ToolEnum](#ToolEnum) |  |  | Тип иконки |
| network | [NetworkEnum](#NetworkEnum) |  |  | Тип иконки |
| user | [UserEnum](#UserEnum) |  |  | Тип иконки |
| category | [CategoryEnum](#CategoryEnum) |  |  | Тип иконки |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onMouseEnter | Function |  |  | Обработчик события наведения курсора на иконку |
| onMouseLeave | Function |  |  | Обработчик события снятия курсора с иконки |
| onClick | Function |  |  | Обработчик клика по иконке |







## Типы






### <a id="ActionEnum"></a>ActionEnum

 * `'call'`
 * `'check'`
 * `'error'`
 * `'fail'`
 * `'more'`
 * `'ok'`
 * `'ok_filled'`
 * `'repeat'`
 * `'down'`
 * `'left'`
 * `'right'`
 * `'up'`


### <a id="BankEnum"></a>BankEnum

 * `'244'`
 * `'256'`
 * `'285'`
 * `'351'`
 * `'404'`
 * `'439'`
 * `'1309'`
 * `'1415'`
 * `'1490'`
 * `'1516'`
 * `'2377'`
 * `'2449'`
 * `'3001'`
 * `'3308'`
 * `'4267'`
 * `'4924'`
 * `'5030'`
 * `'5475'`
 * `'6415'`
 * `'7311'`
 * `'7686'`
 * `'7687'`
 * `'8967'`
 * `'9908'`
 * `'10223'`


### <a id="CardEnum"></a>CardEnum

 * `'maestro'`
 * `'mastercard'`
 * `'visa'`
 * `'visaelectron'`
 * `'belkart'`


### <a id="FormatEnum"></a>FormatEnum

 * `'1c'`
 * `'csv'`
 * `'default'`
 * `'doc'`
 * `'pdf'`
 * `'png'`
 * `'ppt'`
 * `'sketch'`
 * `'svg'`
 * `'txt'`
 * `'xls'`
 * `'xml'`


### <a id="CurrencyEnum"></a>CurrencyEnum

 * `'rub'`
 * `'chf'`
 * `'eur'`
 * `'gbp'`
 * `'jpy'`
 * `'usd'`


### <a id="ToolEnum"></a>ToolEnum

 * `'atm'`
 * `'close'`
 * `'calendar'`
 * `'email'`
 * `'help'`
 * `'helpfilled'`
 * `'info'`
 * `'mobile'`
 * `'office'`
 * `'payment_plus'`
 * `'payment_rounded_plus'`
 * `'printer'`
 * `'search'`
 * `'site'`


### <a id="NetworkEnum"></a>NetworkEnum

 * `'vk'`
 * `'facebook'`
 * `'twitter'`


### <a id="UserEnum"></a>UserEnum

 * `'body'`
 * `'logout'`


### <a id="CategoryEnum"></a>CategoryEnum

 * `'appliances'`
 * `'auto'`
 * `'books_movies'`
 * `'business'`
 * `'charity'`
 * `'dress'`
 * `'education'`
 * `'entertainment'`
 * `'family'`
 * `'finance'`
 * `'forgot'`
 * `'gasoline'`
 * `'gibdd_fines'`
 * `'health'`
 * `'hobby'`
 * `'housekeeping'`
 * `'investments'`
 * `'loans'`
 * `'medicine'`
 * `'mobile_internet'`
 * `'mortgage'`
 * `'other'`
 * `'person'`
 * `'pets'`
 * `'rent'`
 * `'repairs'`
 * `'restaurants'`
 * `'shopping'`
 * `'tax_fines'`
 * `'transport'`
 * `'travel'`
 * `'user'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`
 * `'xxl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`
 * `'alfa-on-colored'`



