# ConfirmationModal



```javascript
import ConfirmationModal from 'arui-feather/src/confirmation-modal/confirmation-modal';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| visible | Boolean | `false`  |  | Управляет видимостью компонента |
| disabled | Boolean | `false`  |  | Управляет доступностью инпута для ввода |
| error | String |  |  | Отображение попапа с ошибкой в момент когда фокус находится в поле ввода |
| desiredChars | Number |  |  | Количество символов, после ввода которых будет вызван onInputFinished |
| isProcessing | Boolean | `false`  |  | Управление отображением спина |
| hasCloser | Boolean | `true`  |  | Показывать ли крестик |
| header | Node |  |  | Заголовок окна подтверждения |
| footer | Node |  |  | Футер окна |
| onAbortClick | Function |  |  | Обработчик закрытия модального окна |
| onInputChange | Function |  |  | Обработчик изменения значения в инпуте |
| onInputFocus | Function |  |  | Обработчик фокуса на инпут |
| onInputBlur | Function |  |  | Обработчик потери фокуса на инпуте |
| onInputFinished | Function |  |  | Обработчик завершения ввода |











