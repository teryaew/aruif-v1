# PopupContainerProvider

Становится родительским элементом для всех дочерних блоков Popup.
Предполагается задавать этому элементу `position:fixed` в стилях.

```javascript
import PopupContainerProvider from 'arui-feather/src/popup-container-provider/popup-container-provider';
```

## Примеры


```javascript
import PopupContainerProvider from 'arui-feather/src/popup-container-provider/popup-container-provider';
import Popup from 'arui-feather/src/popup/popup';
import Page from 'arui-feather/src/page/page';

 <Page>
    <PopupContainerProvider
        style={
            {
                position: 'fixed',
                top: 0,
                right: 0,
                bottom: 0,
                width: '400px',
                overflow: 'auto'
            }
        }
    >
        <Popup>
            Попап отрендерился в PopupContainerProvider, а не в body
            При скролле внутри блока, попап ездит вместе с остальным контентом
        </Popup>
    </PopupContainerProvider>
 </Page>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| className | any |  |  | Дополнительный класс |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| style | Object |  |  | Объект с css-стилями |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



