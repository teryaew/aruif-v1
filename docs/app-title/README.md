# AppTitle

Компонент заголовка страницы.
Обычно используется совместно с компонентом `Page`.

```javascript
import AppTitle from 'arui-feather/src/app-title/app-title';
```

## Примеры


```javascript
import Page from 'arui-feather/src/page/page';
import Header from 'arui-feather/src/header/header';
import Footer from 'arui-feather/src/footer/footer';

import AppTitle from 'arui-feather/src/app-title/app-title';
import AppMenu from 'arui-feather/src/app-menu/app-menu';
import AppContent from 'arui-feather/src/app-content/app-content';

import Heading from 'arui-feather/src/heading/heading';
import Menu from 'arui-feather/src/menu/menu';
import Paragraph from 'arui-feather/src/paragraph/paragraph';

<Page header={ <Header /> } footer={ <Footer /> }>
    <AppTitle>
        <Heading>Заголовок страницы</Heading>
    </AppTitle>
    <AppMenu>
        <Menu />
    </AppMenu>
    <AppContent>
        <Paragraph>Контент страницы...</Paragraph>
    </AppContent>
</Page>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



