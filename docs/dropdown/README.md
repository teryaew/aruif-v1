# Dropdown

Компонент "выпадашка": ссылка или кнопка. По клику показывается Popup.

```javascript
import Dropdown from 'arui-feather/src/dropdown/dropdown';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| locale | String |  |  | Локализация компонента |
| switcherType | [SwitcherTypeEnum](#SwitcherTypeEnum) | `'link'`  |  | Тип компонента |
| switcherText | Node | `<FormattedMessage<br>    id='dropdown.switcherText'<br>    defaultMessage='Switcher'<br>/>`  |  | Текст кнопки компонента |
| popupContent | Node |  |  | Компонент [Popup](../popup/) |
| popupProps | Object |  |  | Свойства для компонента [Popup](../popup/) |
| mode | [ModeEnum](#ModeEnum) |  |  | Управление возможностью отображать попап при наведении курсора |
| disabled | Boolean | `false`  |  | Управление возможностью открытия попапа |
| opened | Boolean |  |  | Управление состоянием открыт/закрыт попапа |
| togglable | [TogglableEnum](#TogglableEnum) |  |  | Только для switcherType='button'. Тип переключателя для кнопки, 'check' |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onSwitcherClick | Function |  |  | Обработчик клика по кнопке компонента |
| onSwitcherMouseEnter | Function |  |  | Обработчик события наведения курсора на кнопку компонента |
| onSwitcherMouseLeave | Function |  |  | Обработчик события снятия курсора с кнопки компонента |
| onPopupMouseEnter | Function |  |  | Обработчик события наведения курсора на попап |
| onPopupMouseLeave | Function |  |  | Обработчик события снятия курсора с попапа |
| onPopupClickOutside | Function |  |  | Обработчик события клика попапа за пределами попапа |







## Типы






### <a id="SwitcherTypeEnum"></a>SwitcherTypeEnum

 * `'link'`
 * `'button'`


### <a id="ModeEnum"></a>ModeEnum

 * `'hover'`
 * `'normal'`


### <a id="TogglableEnum"></a>TogglableEnum

 * `'button'`
 * `'check'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



