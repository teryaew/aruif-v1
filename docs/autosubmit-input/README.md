# AutosubmitInput



```javascript
import AutosubmitInput from 'arui-feather/src/autosubmit-input/autosubmit-input';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| desiredChars | Number |  | Да | Количество символов, после ввода onFinished будет вызван |
| onChange | Function |  |  | Обработчик изменения ввода |
| onFinished | Function |  |  | Обработчик окончания ввода |











