# Header

Компонент шапки сайта: лого, меню и пользовательский профиль.
Обычно используется совместно с компонентом `Page`.

```javascript
import Header from 'arui-feather/src/header/header';
```

## Примеры


```javascript
import Page from 'arui-feather/src/page/page';
import Header from 'arui-feather/src/header/header';
import Footer from 'arui-feather/src/footer/footer';

<Page header={ <Header /> } footer={ <Footer /> }>
    Контент страницы...
</Page>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| popupMenuContent | Array |  |  | Контент для меню в попапе; TODO @teryaew: rewrite |
| root | String | `'/'`  |  | Корневая ссылка |
| menu | Node |  |  | Содержимое меню в шапке |
| help | Node |  |  | Содержимое элемента справки |
| user | Node |  |  | Содержимое элемента пользователя |
| support | Node |  |  | Содержимое элмента контактов поддержки |
| topContent | Node |  |  | Произвольный контент над логотипом и меню |
| fixed | Boolean | `false`  |  | Управление возможностью фиксирования шапки к верхнему краю окна |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onLogoClick | Function |  |  | Обработчик события клика по логотипу Альфа-Банк |
| onTitleClick | Function |  |  | Обработчик события клика по названию |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



