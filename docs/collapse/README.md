# Collapse

Компонент "подката": позволяет спрятать кусок текста за ссылку "Еще...".

```javascript
import Collapse from 'arui-feather/src/collapse/collapse';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| isExpanded | Boolean |  |  | Управление состоянием expand/collapse компонента |
| collapsedLabel | String | `<FormattedMessage<br>    id='collapse.collapsedLabel'<br>    defaultMessage='Expand'<br>/>`  |  | Текст ссылки в expand состоянии |
| expandedLabel | String | `<FormattedMessage<br>    id='collapse.expandedLabel'<br>    defaultMessage='Collapse'<br>/>`  |  | Текст ссылки в collapse состоянии |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onExpandedChange | Function |  |  | Обработчик смены состояния expand/collapse |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



