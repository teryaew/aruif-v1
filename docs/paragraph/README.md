# Paragraph

Компонент параграфа текста.

```javascript
import Paragraph from 'arui-feather/src/paragraph/paragraph';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| view | [ViewEnum](#ViewEnum) |  |  | Тип параграфа |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ViewEnum"></a>ViewEnum

 * `'lead'`
 * `'normal'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



