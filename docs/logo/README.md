# Logo

Компонент логотипа. Может отбражаться как в виде знака, так и в виде знак + написание.

```javascript
import Logo from 'arui-feather/src/logo/logo';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| view | [ViewEnum](#ViewEnum) |  |  | Тип логотипа |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ViewEnum"></a>ViewEnum

 * `'default'`
 * `'full'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



