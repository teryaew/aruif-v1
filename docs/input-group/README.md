# InputGroup

Компонент группы полей для текстового ввода.

```javascript
import InputGroup from 'arui-feather/src/input-group/input-group';
```

## Примеры


```
import 'Input' from 'arui-feather/src/input/input';
import 'InputGroup' from 'arui-feather/src/input-group/input-group';

// Группа полей для ввода
<InputGroup>
   <Input />
   <Input />
   <Input />
</InputGroup>

// Группа полей для ввода, растягивающаяся на всю ширину
<InputGroup width='available'>
   <Input />
   <Input />
   <Input />
</InputGroup>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| width | [WidthEnum](#WidthEnum) |  |  | Управление возможностью компонента занимать всю ширину родителя |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="WidthEnum"></a>WidthEnum

 * `'default'`
 * `'available'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



