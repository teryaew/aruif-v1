# Page

Компонент страницы.
Как правило является корневым компонентов страницы.
Обычно используется совместно с компонентами `Header`, `Footer`
и компонентами `AppTitle`, `AppMenu` и `AppContent`.

```javascript
import Page from 'arui-feather/src/page/page';
```

## Примеры


```javascript
import Page from 'arui-feather/src/page/page';
import Header from 'arui-feather/src/header/header';
import Footer from 'arui-feather/src/footer/footer';

import AppTitle from 'arui-feather/src/app-title/app-title';
import AppMenu from 'arui-feather/src/app-menu/app-menu';
import AppContent from 'arui-feather/src/app-content/app-content';

import Heading from 'arui-feather/src/heading/heading';
import Menu from 'arui-feather/src/menu/menu';
import Paragraph from 'arui-feather/src/paragraph/paragraph';

<Page header={ <Header /> } footer={ <Footer /> }>
    <AppTitle>
        <Heading>Заголовок страницы</Heading>
    </AppTitle>
    <AppMenu>
        <Menu />
    </AppMenu>
    <AppContent>
        <Paragraph>Контент страницы...</Paragraph>
    </AppContent>
</Page>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| view | [ViewEnum](#ViewEnum) |  |  | Вид |
| header | Node |  |  | Шапка страницы |
| footer | Node |  |  | Футер страницы |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ViewEnum"></a>ViewEnum

 * `'default'`
 * `'text'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



