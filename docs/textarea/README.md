# Textarea

Компонент многострочного текстового ввода.

```javascript
import Textarea from 'arui-feather/src/textarea/textarea';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| fixedLabel | Boolean |  |  | Фиксированное позиционирование лейбла |
| label | Node |  |  | Лейбл для поля |
| tip | Node |  |  | Подсказка под полем |
| width | [WidthEnum](#WidthEnum) | `'default'`  |  | Управление возможностью компонента занимать всю ширину родителя |
| autocomplete | Boolean | `true`  |  | Управление автозаполнением компонента |
| disabled | Boolean | `false`  |  | Управление возможностью изменения значения компонента |
| autosize | Boolean | `true`  |  | Управление возможностью подстраивать высоту компонента под высоту текста |
| maxLength | Number |  |  | Максимальное число символов |
| id | String |  |  | Уникальный id блока |
| error | Node |  |  | Отображение ошибки |
| name | String |  |  | Уникальное имя блока |
| value | String |  |  | Содержимое поля ввода, указанное по умолчанию |
| tabIndex | Number |  |  | Последовательность перехода между контролами при нажатии на Tab |
| placeholder | String |  |  | Подсказка |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| resize | [ResizeEnum](#ResizeEnum) |  |  | Управление возможностью изменения размеров компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| onBlur | Function |  |  | Обработчик снятия фокуса c поля |
| onChange | Function |  |  | Обработчик изменения значения 'value' |
| onFocus | Function |  |  | Обработчик фокуса поля |
| onHeightChange | Function |  |  | Обработчик события изменения высоты компонента со значением параметра "autosize" = true |
| onPaste | Function |  |  | Обработчик события вставки текста в поле |





## Публичные методы
| Метод  | Описание |
| ------ | -------- |
| focus() | Устанавливает фокус на поле ввода. |
| blur() | Снимает фокус с поля ввода. |
| scrollTo() | Скроллит страницу до поля ввода. |





## Типы






### <a id="WidthEnum"></a>WidthEnum

 * `'default'`
 * `'available'`


### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ResizeEnum"></a>ResizeEnum

 * `'both'`
 * `'horizontal'`
 * `'vertical'`
 * `'none'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



