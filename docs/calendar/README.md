# Calendar

Компонент календаря.

```javascript
import Calendar from 'arui-feather/src/calendar/calendar';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| value | deprecatedType(Moment, Type.number, 'Calendar.selectedFrom should be a timestamp') |  |  | Выбранная дата, в формате unix timestamp |
| selectedFrom | deprecatedType(Moment, Type.number, 'Calendar.selectedFrom should be a timestamp') | `null`  |  | Левая граница диапазона дат, в формате unix timestamp |
| selectedTo | deprecatedType(Moment, Type.number, 'Calendar.selectedTo should be a timestamp') | `null`  |  | Правая граница диапазона дат, в формате unix timestamp |
| earlierLimit | deprecatedType(Moment, Type.number, 'Calendar.earlierLimit should be a timestamp') |  |  | Левая граница дат, возможных для выбора, в формате unix timestamp |
| laterLimit | deprecatedType(Moment, Type.number, 'Calendar.laterLimit should be a timestamp') |  |  | Правая граница дат, возможных для выбора, в формате unix timestamp |
| month | deprecatedType(Moment, Type.number, 'Calendar.month should be a timestamp') |  |  | Месяц, который будет отрендерен, в формате unix timestamp |
| onValueChange | Function |  |  | Обработчик смены даты |
| onMonthChange | Function |  |  | Обработчик смены месяца |
| outputFormat | String | `'DD.MM.YYYY'`  |  | Тип форматирования даты при выводе |
| months | Array.<String> | `['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',<br>    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']`  |  | Список названий месяцев |
| weekdays | Array.<String> | `['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']`  |  | Список названий дней недели |
| offDays | deprecatedType( Type.arrayOf(Moment), Type.arrayOf(Type.number), 'Calendar.offDays should be a sorted array of timestamps' ) | `[]`  |  | Список выходных дней в виде unix timestamp, отсортированный по возрастанию |
| showArrows | Boolean | `true`  |  | Отображение стрелок навигации по месяцам |
| isKeyboard | Boolean | `true`  |  | Возможность управления календарём с клавиатуры |
| error | Node |  |  | Сообщение об ошибке |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onKeyDown | Function |  |  | Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте |
| onKeyUp | Function |  |  | Обработчик события отжатия на клавишу клавиатуры в момент, когда фокус находится на компоненте |
| onFocus | Function |  |  | Обработчик фокуса |
| onBlur | Function |  |  | Обработчик снятия фокуса |





## Публичные методы
| Метод  | Описание |
| ------ | -------- |
| focus() | Устанавливает фокус на календарь. |
| blur() | Убирает фокус с календаря. |





## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



