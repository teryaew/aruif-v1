# Footer

Компонент подвала сайта.
Обычно используется совместно с компонентом `Page`.

```javascript
import Footer from 'arui-feather/src/footer/footer';
```

## Примеры


```javascript
import Page from 'arui-feather/src/page/page';
import Header from 'arui-feather/src/header/header';
import Footer from 'arui-feather/src/footer/footer';

<Page header={ <Header /> } footer={ <Footer /> }>
    Контент страницы...
</Page>
```



## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| locale | String |  |  | Локализация компонента |
| menu | Node |  |  | Меню в подвале |
| additional | Node | `<FormattedMessage<br>    id='footer.additional'<br>    defaultMessage='Made in Alfa-Laboratory'<br>/>`  |  | Дополнительный текст |
| social | Node |  |  | Содержимое блока соц. сетей |
| showSocial | Boolean | `true`  |  | Отображение блока соц. сетей |
| copyright | Node |  |  | Содержимое блока копирайта |
| showYears | Boolean | `false`  |  | Отображение годов в копирайте |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



