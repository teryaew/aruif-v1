# Copyright

Компонент копирайта: отображает данные о лицензии Альфа-Банка.

```javascript
import Copyright from 'arui-feather/src/copyright/copyright';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| className | any |  |  | Дополнительный класс |
| locale | String | `'en'`  |  | Локализация компонента |
| showYears | Boolean | `false`  |  | Отображение годов |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |







## Типы






### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



