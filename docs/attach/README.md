# Attach

Компонент прикрепления файлов

```javascript
import Attach from 'arui-feather/src/attach/attach';
```




## Props


| Prop  | Тип  | По-умолчанию | Обязательный | Описание |
| ----- | ---- | ------------ | ------------ |----------|
| value | String |  |  | Содержимое поля ввода, указанное по умолчанию |
| name | String |  |  | Уникальное имя блока |
| tabIndex | Number |  |  | Последовательность перехода между контролами при нажатии на Tab |
| locale | String |  |  | Локализация компонента |
| noFileText | Node\|String | `<FormattedMessage<br>    id='attach.noFileText'<br>    defaultMessage='No file'<br>/>`  |  | Текст для случая, когда файл не загружен |
| buttonContent | Node | `<FormattedMessage<br>    id='attach.buttonContent'<br>    defaultMessage='Choose a file'<br>/>`  |  | Содержимое кнопки для выбора файла |
| buttonProps | Object | `{}`  |  | Свойства для кнопки |
| disabled | Boolean | `false`  |  | Управление возможностью изменения значения компонента |
| size | [SizeEnum](#SizeEnum) | `'m'`  |  | Размер компонента |
| theme | [ThemeEnum](#ThemeEnum) |  |  | Тема компонента |
| className | any |  |  | Дополнительный класс |
| onClick | Function |  |  | Обработчик клика по компоненту кнопки |
| onChange | Function |  |  | Обработчик изменения значения 'value' |
| onClearClick | Function |  |  | Обработчик клика по крестику, сбрасываещему значение 'value' |
| onFocus | Function |  |  | Обработчик фокуса компонента |
| onBlur | Function |  |  | Обработчик снятия фокуса компонента |
| onMouseEnter | Function |  |  | Обработчик события наведения курсора на кнопку |
| onMouseLeave | Function |  |  | Обработчик события снятия курсора с кнопки |





## Публичные методы
| Метод  | Описание |
| ------ | -------- |
| focus() |  |
| blur() |  |





## Типы






### <a id="SizeEnum"></a>SizeEnum

 * `'s'`
 * `'m'`
 * `'l'`
 * `'xl'`


### <a id="ThemeEnum"></a>ThemeEnum

 * `'alfa-on-color'`
 * `'alfa-on-white'`



