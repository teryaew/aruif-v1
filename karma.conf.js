const path = require('path');
const IS_BENCHMARK = process.env.BENCHMARK ? true : false;
const IS_MOBILE = process.env.MOBILE ? true : false;

function getTestsGlobs(tests, postfix) {
    return tests.split(',').map(function (testName) {
        return './src/' + testName + '/*-' + postfix + '.{js,jsx}';
    });
}

module.exports = function (config) {
    var webpackConfig = require('./webpack.config.template');
    webpackConfig.devtool = 'inline-source-map';

    var cfg = {
        browsers: ['PhantomJS_Desktop'],

        singleRun: true,

        plugins: [
            require('karma-webpack'),
            require('karma-chrome-launcher'),
            require('karma-phantomjs-launcher'),
            require('karma-sourcemap-loader')
        ],

        webpack: webpackConfig,

        webpackMiddleware: {
            noInfo: true,
            quiet: true
        },

        customLaunchers: {
            PhantomJS_Desktop: {
                base: 'PhantomJS',
                options: {
                    viewportSize: {
                        width: 1280,
                        height: 100
                    }
                }
            }
        },

        browserNoActivityTimeout: 20000
    };

    if (IS_MOBILE) {
        cfg.browsers = ['MobileSafari'];
        cfg.plugins.push(
            require('karma-ios-simulator-launcher')
        );
    }

    if (!IS_BENCHMARK) {
        webpackConfig.module.loaders.push({
            test: /\.jsx$/,
            loader: 'isparta',
            include: path.resolve('src'),
        });

        var testsFiles = !process.env.TESTS
            ? ['./src/**/*-test.js?(x)']
            : getTestsGlobs(process.env.TESTS, 'test');

        testsFiles.unshift('./src/polyfills.js');
        testsFiles.unshift('./karma-warnings.js');

        Object.assign(cfg, {
            frameworks: ['mocha', 'chai-spies', 'chai-dom', 'chai'],
            reporters: ['mocha', 'coverage', 'junit'],
            preprocessors: {
                './src/**/*': ['webpack', 'sourcemap'],
            },
            files: testsFiles,
            coverageReporter: {
                check: {
                    global: {
                        statements: 86,
                        branches: 80,
                        functions: 95,
                        lines: 40,
                    },
                },
            },
            junitReporter: {
                outputFile: 'test-results.xml',
                useBrowserName: false
            }
        });

        cfg.plugins.push(
            require('karma-mocha'),
            require('karma-chai'),
            require('karma-chai-spies'),
            require('karma-chai-dom'),
            require('karma-mocha-reporter'),
            require('karma-junit-reporter'),
            require('karma-coverage')
        );
    } else {
        var benchFiles = !process.env.TESTS
            ? ['./src/**/*-benchmark.js?(x)']
            : getTestsGlobs(process.env.TESTS, 'benchmark');

        benchFiles.unshift('./src/polyfills.js');

        Object.assign(cfg, {
            frameworks: ['benchmark'],
            reporters: ['benchmark'],
            preprocessors: {
                './src/**/*': ['webpack', 'sourcemap'],
            },
            files: benchFiles
        });

        cfg.plugins.push(
            require('karma-benchmark'),
            require('karma-benchmark-reporter')
        );
    }

    config.set(cfg);
};
