const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const WEBPACK_CONFIG_TEMPLATE = require('./webpack.config.template.js');
const WEBPACK_CONFIG_TEMPLATE_DEVELOPMENT = require('./webpack.config.template.development.js');
const WEBPACK_CONFIG_TEMPLATE_PRODUCTION = require('./webpack.config.template.production.js');

const IS_PRODUCTION = (process.env.NODE_ENV === 'production');
const COMPONENTS_TO_BUILD = process.env.COMPONENTS
    ? process.env.COMPONENTS.toLowerCase().replace(/\s/g, '').split(',')
    : [];
const APP_ROOT = './demo';
const POLYFILLS = './src/polyfills.js';
const TEMPLATE = path.join(
    APP_ROOT,
    IS_PRODUCTION ? 'index-production.html' : 'index-development.html'
);

const DEMO_COMPONENTS = fs.readdirSync(APP_ROOT)
    .filter(item => fs.statSync(path.resolve(APP_ROOT, item)).isDirectory())
    .filter(item => !COMPONENTS_TO_BUILD.length || COMPONENTS_TO_BUILD.find(component => component === item));

DEMO_COMPONENTS.forEach(item => {
    const indexTarget = path.resolve(APP_ROOT, 'index.jsx');
    const resultDir = path.resolve(__dirname, '.build', 'prebuild', item);
    const result = fs.readFileSync(indexTarget, 'utf8')
        .replace(/.\/demo/g, path.resolve(APP_ROOT, item, 'demo'))
        .replace(/..\/src/g, path.resolve(__dirname, 'src'));

    mkdirp.sync(resultDir);
    fs.writeFileSync(path.resolve(resultDir, 'index.jsx'), result, 'utf8');
});

const ENTRIES = Object.assign(
    {
        '.': [
            path.resolve(__dirname, POLYFILLS),
            path.resolve(APP_ROOT, 'index.jsx')
        ]
    },
    DEMO_COMPONENTS.reduce((result, item) => {
        result[item] = [
            path.resolve(__dirname, POLYFILLS),
            path.resolve(__dirname, '.build', 'prebuild', item, 'index.jsx')
        ];
        return result;
    }, {})
);

const webpackConfig = Object.assign(
    {
        entry: ENTRIES,
        output: {
            path: path.resolve(__dirname, '.build', 'dist'),
            publicPath: '../',
            filename: '[name]/index.js'
        }
    },
    WEBPACK_CONFIG_TEMPLATE
);

Object.assign(
    webpackConfig.module.loaders.find(loader => loader.loader === 'babel'),
    {
        include: [
            path.resolve(__dirname, 'demo'),
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, '.build', 'prebuild')
        ]
    }
);

webpackConfig.plugins.push(
    new CopyWebpackPlugin(
        [{ from: TEMPLATE, to: './index.html' }]
            .concat(DEMO_COMPONENTS.map(component => (
                { from: TEMPLATE, to: path.resolve(component, './index.html') }
            )))
    )
);

webpackConfig.plugins.push(
    new webpack.DefinePlugin({
        'process.COMPONENTS': JSON.stringify(DEMO_COMPONENTS.sort()),
        'process.HOT_LOADER': process.env.HOT_LOADER,
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
);

webpackConfig.plugins.push(
    new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'vendor.js',
        minChunks: module => /node_modules/.test(module.resource)
    })
);

if (IS_PRODUCTION) {
    webpackConfig.module.loaders.forEach(loader => {
        const prodLoader = WEBPACK_CONFIG_TEMPLATE_PRODUCTION.module.loaders
            .find(prodLoader => loader.test.toString() === prodLoader.test.toString());

        if (prodLoader) {
            Object.assign(loader, prodLoader);
        }
    });
    WEBPACK_CONFIG_TEMPLATE_PRODUCTION.plugins.forEach(plugin => {
        webpackConfig.plugins.push(plugin);
    });
} else {
    Object.assign(webpackConfig, WEBPACK_CONFIG_TEMPLATE_DEVELOPMENT);
}

module.exports = webpackConfig;
