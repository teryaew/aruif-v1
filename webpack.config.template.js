const path = require('path');
const webpack = require('webpack');

const MQ = require('./src/mq/mq.json');

module.exports = {
    resolve: {
        root: [
            path.join(__dirname, 'node_modules')
        ],
        extensions: ['', '.js', '.jsx', '.web.js', '.webpack.js']
    },
    module: {
        loaders: [
            {
                test: /\.json?$/,
                loader: 'json-loader'
            },
            {
                test: /\.jsx?$/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    plugins: ['transform-decorators-legacy'],
                    presets: ['react', 'es2015', 'stage-0'],
                    env: {
                        production: {
                            plugins: [
                                'transform-react-remove-prop-types',
                                'transform-react-constant-elements',
                                'transform-react-inline-elements'
                            ]
                        }
                    }
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader?sourceMap!postcss-loader'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?mimetype=application/font-woff'
            },
            {
                test: /\.(ttf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?mimetype=application/octet-stream'
            },
            {
                test: /\.(eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(jpe?g)$/i,
                loader: 'url-loader?mimetype=image/jpeg'
            },
            {
                test: /\.(png)$/i,
                loader: 'url-loader?mimetype=image/png'
            },
            {
                test: /\.(gif)$/i,
                loader: 'url-loader?mimetype=image/gif'
            },
            {
                test: /\.(svg)$/i,
                loader: 'url-loader?mimetype=image/svg+xml'
            }
        ]
    },
    postcss: function () {
        return [
            require('postcss-import')(),
            require('postcss-url')({
                url: 'rebase'
            }),
            require('postcss-mixins'),
            require('postcss-for'),
            require('postcss-each'),
            require('postcss-simple-vars')({
                variables: {
                    gridMaxWidth: '1000px',
                    gridGutter: '10px',
                    gridFlex: 'flex'
                }
            }),
            require('postcss-custom-media')({
                extensions: MQ
            }),
            require('postcss-custom-properties'),
            require('postcss-calc'),
            require('postcss-nested'),
            require('autoprefixer')({
                browsers: [
                    'last 2 versions',
                    'ie >= 9',
                    'android >= 4',
                    'ios >= 8'
                ]
            })
        ];
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react'
        }),
        new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(ru|en-gb)$/)
    ]
};
