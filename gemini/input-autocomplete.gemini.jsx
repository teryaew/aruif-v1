import GeminiBox from '../gemini-utils/gemini-box/gemini-box';
import InputAutocomplete from '../src/input-autocomplete/input-autocomplete';

const NAME = 'input-autocomplete';
const THEMES = ['alfa-on-color', 'alfa-on-white'];
const SIZES = process.env.ALL_SIZES ? ['s', 'm', 'l', 'xl'] : ['m'];

const OPTIONS = [
    {
        value: 1,
        text: 'Facebook'
    },
    {
        value: 2,
        text: 'Twitter'
    },
    {
        value: 3,
        text: 'LinkedIn'
    },
    {
        value: 4,
        text: 'Sina Weibo'
    },
    {
        value: 5,
        text: 'Pinterest'
    },
    {
        value: 6,
        text: 'VKontakte'
    },
    {
        value: 7,
        text: 'Instagram'
    },
    {
        value: 8,
        text: 'Tumblr'
    },
    {
        value: 9,
        text: 'Flickr'
    },
    {
        value: 10,
        text: 'Odnoklassniki'
    }
];

const PROP_SETS = [
    { placeholder: 'Input', opened: true, options: OPTIONS },
    { placeholder: 'Input', disabled: true }
];

geminiReact.suite(NAME, function () {
    THEMES.forEach((theme) => {
        let themeSelector = `${NAME}_theme_${theme}`;

        SIZES.forEach((size) => {
            let sizeSelector = `${NAME}_size_${size}`;

            PROP_SETS.forEach((set, index) => {
                let selector = `${themeSelector}.${sizeSelector}.${NAME}_prop-set_${index + 1}`;

                geminiReact.suite(selector, function (suite) {
                    let props = { theme, size, ...set };
                    let template = (
                        <GeminiBox theme={ theme }>
                            <InputAutocomplete { ...props } />
                        </GeminiBox>
                    );

                    if (set.disabled) {
                        suite
                            .render(template)
                            .capture('plain');
                    } else {
                        suite
                            .setExtraCaptureElements(['.popup'])
                            .render(template)
                            .capture('plain')
                            .capture('hovered', function (actions) {
                                actions.mouseMove(this.renderedComponent);
                            })
                            .capture('pressed', function (actions) {
                                actions.mouseDown(this.renderedComponent);
                            })
                            .capture('clicked', function (actions) {
                                actions.mouseUp(this.renderedComponent);
                            })
                            .capture('focused-hard', function (actions) {
                                actions.focus(this.renderedComponent);
                            });
                    }
                });
            });
        });
    });
});
