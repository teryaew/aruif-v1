import FeatherComponent from '../src/feather/feather';

import cn from '../src/cn';
require('./demo-section.css');

/**
 * @param {String} [theme] "alfa-on-color", "alfa-on-white"
 * @param {String} [className]
 */
@cn('demo-section')
class DemoSection extends FeatherComponent {
    render(cn) {
        return (
            <div className={ cn }>{ this.props.children }</div>
        );
    }
}

export default DemoSection;
