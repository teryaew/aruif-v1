import FeatherComponent from '../../src/feather/feather';

import Page from '../../src/page/page';
import Footer from '../../src/footer/footer';
import Header from '../../src/header/header';

import AppTitle from '../../src/app-title/app-title';
import AppMenu from '../../src/app-menu/app-menu';
import AppContent from '../../src/app-content/app-content';

import Heading from '../../src/heading/heading';
import Menu from '../../src/menu/menu';
import Paragraph from '../../src/paragraph/paragraph';

require('../../src/main.css');

class Demo extends FeatherComponent {
    render() {
        return (
            <Page header={ <Header /> } footer={ <Footer /> }>
                <AppTitle>
                    <Heading>Заголовок страницы</Heading>
                </AppTitle>
                <AppMenu>
                    <Menu
                        view='horizontal'
                        content={ [
                            { content: 'Раздел 1', value: 'section1' },
                            { content: 'Раздел 2', value: 'section2' }
                        ] }
                    />
                </AppMenu>
                <AppContent>
                    <Paragraph>Контент страницы...</Paragraph>
                </AppContent>
            </Page>
        );
    }
}

export default Demo;
