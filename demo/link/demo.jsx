import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Link from '../../src/link/link';
import ThemeProvider from '../../src/theme-provider/theme-provider';

require('./demo.css');

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div>
                                <span className='layout'>
                                    <Link size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='xl'>Ссылка</Link>
                                </span>
                            </div>
                            <div>
                                <span className='layout'>
                                    <Link disabled={ true } size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='xl'>Ссылка</Link>
                                </span>
                            </div>
                            <div>
                                <span className='layout'>
                                    <Link pseudo={ true } size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='xl'>Ссылка</Link>
                                </span>
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <div>
                                <span className='layout'>
                                    <Link size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link size='xl'>Ссылка</Link>
                                </span>
                            </div>
                            <div>
                                <span className='layout'>
                                    <Link disabled={ true } size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link disabled={ true } size='xl'>Ссылка</Link>
                                </span>
                            </div>
                            <div>
                                <span className='layout'>
                                    <Link pseudo={ true } size='s'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='m'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='l'>Ссылка</Link>
                                </span>
                                <span className='layout'>
                                    <Link pseudo={ true } size='xl'>Ссылка</Link>
                                </span>
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
