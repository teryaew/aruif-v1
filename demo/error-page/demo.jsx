import FeatherComponent from '../../src/feather/feather';

import ErrorPage from '../../src/error-page/error-page';
import Header from '../../src/header/header';

require('../../src/main.css');

class Demo extends FeatherComponent {
    render() {
        return (
            <ErrorPage
                returnUrl='/login'
                header={ <Header /> }
            />
        );
    }
}

export default Demo;
