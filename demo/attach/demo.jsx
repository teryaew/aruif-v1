import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Icon from '../../src/icon/icon';
import Attach from '../../src/attach/attach';
import ThemeProvider from '../../src/theme-provider/theme-provider';
import { autobind } from 'core-decorators';

require('./demo.css');

class Demo extends FeatherComponent {
    state = {
        value: ''
    };

    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div className='layout'>
                                <Attach
                                    size='s'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='m'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='l'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                    disabled={ true }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <div className='layout'>
                                <Attach
                                    size='s'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='m'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='l'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                    disabled={ true }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <div className='layout'>
                                <Attach
                                    size='s'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='s' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='m'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='m' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='l'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='l' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='xl' format='pdf' />
                                    } }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div className='layout'>
                                <Attach
                                    size='s'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='s' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='m'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='m' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='l'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='l' format='pdf' />
                                    } }
                                />
                            </div>
                            <div className='layout'>
                                <Attach
                                    size='xl'
                                    buttonContent='Please, choose a file'
                                    noFileText='file in pdf format'
                                    buttonProps={ {
                                        pseudo: true,
                                        icon: <Icon size='xl' format='pdf' />
                                    } }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <div className='layout'>
                                <Attach
                                    size='s'
                                    buttonContent={ this.state.value ? 'Choose another file' : 'Choose a file' }
                                    onChange={ this.handleChange }
                                    onClearClick={ this.handleClearClick }
                                    value={ this.state.value }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }

    @autobind
    handleChange(value) {
        this.setState({
            value
        });
    }

    @autobind
    handleClearClick() {
        this.setState({
            value: ''
        });
    }
}

export default Demo;
