import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Spin from '../../src/spin/spin';
import Button from '../../src/button/button';
import ThemeProvider from '../../src/theme-provider/theme-provider';

require('./demo.css');

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            { this.renderSpins() }
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            { this.renderSpins() }
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }

    renderSpins() {
        return ['s', 'm', 'l', 'xl'].map(size => (
            <span key={ size } className='layout'>
                <Button
                    icon={
                        <Spin
                            size={ size }
                            visible={ true }
                        />
                    }
                    size={ size }
                >Кнопка</Button>
            </span>
        ));
    }
}

export default Demo;
