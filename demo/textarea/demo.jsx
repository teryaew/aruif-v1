import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Textarea from '../../src/textarea/textarea';
import ThemeProvider from '../../src/theme-provider/theme-provider';

import cn from '../../src/cn';

require('./demo.css');

@cn('demo')
class Demo extends FeatherComponent {
    render(cn) {
        return (
            <div className={ cn }>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Textarea placeholder='Textarea...' />
                        </div>
                    </ThemeProvider>

                </DemoSection>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Textarea
                                error='something went wrong'
                                placeholder='Textarea...'
                            />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Textarea
                                autosize={ true }
                                placeholder='Textarea autosize...'
                            />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Textarea placeholder='Textarea...' />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Textarea
                                error='something went wrong'
                                placeholder='Textarea...'
                            />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Textarea
                                autosize={ true }
                                placeholder='Textarea autosize...'
                            />
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
