import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Plate from '../../src/plate/plate';
import Paragraph from '../../src/paragraph/paragraph';
import Link from '../../src/link/link';
import ThemeProvider from '../../src/theme-provider/theme-provider';
import { LOREM_IPSUM } from '../../src/vars';

/* eslint no-alert: 0 */
class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Plate>
                                <Paragraph>
                                    { LOREM_IPSUM.slice(0, 3) }
                                </Paragraph>
                                <Link>Plate Link</Link>
                            </Plate>
                            <Plate hasCloser={ true } onCloserClick={ function () { alert('plate close'); } }>
                                <Paragraph>
                                    { LOREM_IPSUM.slice(0, 3) }
                                </Paragraph>
                                <Link>Plate Link</Link>
                            </Plate>
                            <Plate isFlat={ true }>
                                <Paragraph>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore
                                    magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </Paragraph>
                                <Link>Plate Link</Link>
                            </Plate>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Plate>
                                <ThemeProvider theme='alfa-on-white'>
                                    <div>
                                        <Paragraph>
                                            { LOREM_IPSUM.slice(0, 3) }
                                        </Paragraph>
                                        <Link>Plate Link</Link>
                                    </div>
                                </ThemeProvider>
                            </Plate>
                            <Plate hasCloser={ true } onCloserClick={ function () { alert('plate close'); } }>
                                <ThemeProvider theme='alfa-on-white'>
                                    <div>
                                        <Paragraph>
                                            { LOREM_IPSUM.slice(0, 3) }
                                        </Paragraph>
                                        <Link>Plate Link</Link>
                                    </div>
                                </ThemeProvider>
                            </Plate>
                            <Plate isFlat={ true } >
                                    <Paragraph>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco
                                        laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in
                                        reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur.
                                        Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia
                                        deserunt mollit anim id est laborum.
                                    </Paragraph>
                                    <Link>Plate Link</Link>
                            </Plate>
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
