import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Logo from '../../src/logo/logo';
import ThemeProvider from '../../src/theme-provider/theme-provider';

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Logo size='s' />
                            <Logo size='m' />
                            <Logo size='l' />
                            <Logo size='xl' />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Logo size='s' />
                            <Logo size='m' />
                            <Logo size='l' />
                            <Logo size='xl' />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Logo size='s' view='full' />
                            <Logo size='m' view='full' />
                            <Logo size='l' view='full' />
                            <Logo size='xl' view='full' />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <Logo size='s' view='full' />
                            <Logo size='m' view='full' />
                            <Logo size='l' view='full' />
                            <Logo size='xl' view='full' />
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
