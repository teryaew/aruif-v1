import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Copyright from '../../src/copyright/copyright';
import ThemeProvider from '../../src/theme-provider/theme-provider';

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <Copyright />
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <Copyright />
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
