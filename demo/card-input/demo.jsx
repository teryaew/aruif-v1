import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import CardInput from '../../src/card-input/card-input';
import ThemeProvider from '../../src/theme-provider/theme-provider';

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <CardInput />
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <CardInput />
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }
}

export default Demo;
