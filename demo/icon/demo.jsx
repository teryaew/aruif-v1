import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Icon from '../../src/icon/icon';
import ThemeProvider from '../../src/theme-provider/theme-provider';

require('./demo.css');

const ACTION_ICON = ['check', 'error', 'fail', 'ok', 'ok_filled', 'repeat', 'down', 'left', 'right', 'up'];
const BANK_ICON = ['244', '256', '285', '351', '404', '439', '1309', '1415', '1490', '1516', '2377', '2449',
    '3001', '3308', '4267', '4924', '5030', '5475', '6415', '7311', '7686', '7687', '8967', '9908', '10223'];
const CARD_ICON = ['maestro', 'belkart', 'mastercard', 'visa', 'visaelectron'];
const CURRENCY_ICON = ['rub', 'chf', 'eur', 'gbp', 'jpy', 'usd'];
const FORMAT_ICON = ['1c', 'csv', 'default', 'doc', 'pdf', 'png', 'ppt', 'sketch', 'svg', 'txt', 'xls', 'xml'];
const TOOL_ICON = ['close', 'calendar', 'email', 'help', 'helpfilled', 'info', 'payment_rounded_plus',
    'payment_plus', 'printer', 'search'];
const NETWORK_ICON = ['vk', 'facebook', 'twitter'];
const USER_ICON = ['body', 'logout'];
const CATEGORY_ICON = ['appliances', 'auto', 'books_movies', 'business', 'charity',
    'dress', 'education', 'entertainment', 'family', 'finance', 'forgot',
    'gasoline', 'gibdd_fines', 'health', 'hobby', 'housekeeping', 'investments',
    'loans', 'medicine', 'mobile_internet', 'mortgage', 'other', 'person',
    'pets', 'rent', 'repairs', 'restaurants', 'shopping', 'tax_fines',
    'transport', 'travel', 'user'];

class Demo extends FeatherComponent {
    render() {
        return (
            <div>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div className='layer'>
                                { ACTION_ICON.map(icon => this.renderIcons('action', icon)) }
                            </div>
                            <div className='layer'>
                                { BANK_ICON.map(icon => this.renderIcons('bank', icon)) }
                            </div>
                            <div className='layer'>
                                { CARD_ICON.map(icon => this.renderIcons('card', icon)) }
                            </div>
                            <div className='layer'>
                                { CURRENCY_ICON.map(icon => this.renderIcons('currency', icon)) }
                            </div>
                            <div className='layer'>
                                { TOOL_ICON.map(icon => this.renderIcons('tool', icon)) }
                            </div>
                            <div className='layer'>
                                { FORMAT_ICON.map(icon => this.renderIcons('format', icon)) }
                            </div>
                            <div className='layer'>
                                { NETWORK_ICON.map(icon => this.renderIcons('network', icon)) }
                            </div>
                            <div className='layer'>
                                { USER_ICON.map(icon => this.renderIcons('user', icon)) }
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-color'>
                        <div>
                            <div>
                                { ACTION_ICON.map(icon => this.renderIcons('action', icon)) }
                            </div>
                            <div>
                                { BANK_ICON.map(icon => this.renderIcons('bank', icon)) }
                            </div>
                            <div>
                                { CARD_ICON.map(icon => this.renderIcons('card', icon)) }
                            </div>
                            <div>
                                { CURRENCY_ICON.map(icon => this.renderIcons('currency', icon)) }
                            </div>
                            <div>
                                { TOOL_ICON.map(icon => this.renderIcons('tool', icon)) }
                            </div>
                            <div>
                                { FORMAT_ICON.map(icon => this.renderIcons('format', icon)) }
                            </div>
                            <div>
                                { NETWORK_ICON.map(icon => this.renderIcons('network', icon)) }
                            </div>
                            <div>
                                { USER_ICON.map(icon => this.renderIcons('user', icon)) }
                            </div>
                            <div>
                                { CATEGORY_ICON.map(icon => this.renderIcons('category', icon)) }
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-white'>
                    <ThemeProvider theme='alfa-on-colored'>
                        <div>
                            <div>
                                { ['error', 'ok'].map(icon => this.renderIcons('action', icon)) }
                            </div>
                            <div>
                                { BANK_ICON.map(icon => this.renderIcons('bank', icon)) }
                            </div>
                            <div>
                                { CARD_ICON.map(icon => this.renderIcons('card', icon)) }
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }

    renderIcons(propName, propValue) {
        let props = ['s', 'm', 'l', 'xl', 'xxl'].map(size => (
            {
                size,
                [propName]: propValue
            }
        ));

        return (
            <div>
                { props.map(propsItem => <Icon { ...propsItem } />) }
            </div>
        );
    }
}

export default Demo;
