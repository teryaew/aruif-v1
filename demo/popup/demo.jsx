import ReactDOM from 'react-dom';

import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import Popup from '../../src/popup/popup';
import Button from '../../src/button/button';
import Paragraph from '../../src/paragraph/paragraph';
import PopupContainerProvider from '../../src/popup-container-provider/popup-container-provider';
import ThemeProvider from '../../src/theme-provider/theme-provider';
import { LOREM_IPSUM } from '../../src/vars';

import cn from '../../src/cn';
require('./demo.css');

@cn('popup-demo')
class Demo extends FeatherComponent {
    state = {
        popup1: false,
        popup2: false,
        popup3: false,
        popup4: false,
        popup5: false,
        popup6: false,
        popup7: false,
        popup8: false
    };

    componentDidMount() {
        this.refs.popup1.setTarget(ReactDOM.findDOMNode(this.refs.target1));
        this.refs.popup2.setTarget(ReactDOM.findDOMNode(this.refs.target2));
        this.refs.popup3.setTarget(ReactDOM.findDOMNode(this.refs.target3));
        this.refs.popup4.setPosition(500, 400);
        this.refs.popup5.setTarget(ReactDOM.findDOMNode(this.refs.target5));
        this.refs.popup6.setTarget(ReactDOM.findDOMNode(this.refs.target6));
        this.refs.popup7.setTarget(ReactDOM.findDOMNode(this.refs.target7));
        this.refs.popup8.setTarget(ReactDOM.findDOMNode(this.refs.target8));
    }

    render(cn) {
        return (
            <div className={ cn }>
                <div className={ cn('main') }>
                    <DemoSection theme='alfa-on-color'>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <Button
                                    ref='target1'
                                    size='s'
                                    onClick={ () => { this.setState({ popup1: !this.state.popup1 }); } }
                                >
                                    Click me
                                </Button>
                                <Popup
                                    directions={ ['top-center', 'bottom-center'] }
                                    ref='popup1'
                                    size='s'
                                    type='tooltip'
                                    visible={ this.state.popup1 }
                                >
                                    Popup on top or bottom
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-white'>
                        <ThemeProvider theme='alfa-on-color'>
                            <div>
                                <Button
                                    ref='target2'
                                    size='m'
                                    onClick={ () => { this.setState({ popup2: !this.state.popup2 }); } }
                                >
                                    Click me
                                </Button>
                                <Popup
                                    directions={ ['right-center', 'right-top', 'right-bottom'] }
                                    ref='popup2'
                                    size='m'
                                    type='tooltip'
                                    mainOffset={ 13 }
                                    visible={ this.state.popup2 }
                                    invalid={ true }
                                >
                                    Invalid popup
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-color'>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <Button
                                    ref='target3'
                                    size='l'
                                    onMouseEnter={ () => { this.setState({ popup3: true }); } }
                                    onMouseLeave={ () => { this.setState({ popup3: false }); } }
                                >
                                    Hover me
                                </Button>
                                <Popup
                                    ref='popup3'
                                    directions={ ['right-center', 'right-top', 'right-bottom'] }
                                    size='l'
                                    mainOffset={ 0 }
                                    type='tooltip'
                                    visible={ this.state.popup3 }
                                    onMouseLeave={ () => { this.setState({ popup3: false }); } }
                                    onMouseEnter={ () => { this.setState({ popup3: true }); } }
                                >
                                    Popup
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-white'>
                        <ThemeProvider theme='alfa-on-color'>
                            <div>
                                <Button
                                    ref='target4'
                                    size='xl'
                                    onClick={ () => { this.setState({ popup4: !this.state.popup4 }); } }
                                >
                                    Show popup at position(500px, 400px)
                                </Button>
                                <Popup
                                    ref='popup4'
                                    size='xl'
                                    visible={ this.state.popup4 }
                                    target='position'
                                >
                                    Popup with position 500px 400px
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-color'>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <Button
                                    ref='target5'
                                    size='m'
                                    onMouseEnter={ () => { this.setState({ popup5: true }); } }
                                    onMouseLeave={ () => { this.setState({ popup5: false }); } }
                                >
                                    Hover me
                                </Button>
                                <Popup
                                    ref='popup5'
                                    height='available'
                                    visible={ this.state.popup5 }
                                    onMouseLeave={ () => { this.setState({ popup5: false }); } }
                                    onMouseEnter={ () => { this.setState({ popup5: true }); } }
                                >
                                    Popup with available height
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-white'>
                        <ThemeProvider theme='alfa-on-color'>
                            <div>
                                <Button
                                    ref='target6'
                                    size='m'
                                    onMouseEnter={ () => { this.setState({ popup6: true }); } }
                                    onMouseLeave={ () => { this.setState({ popup6: false }); } }
                                >
                                    Hover me
                                </Button>
                                <Popup
                                    ref='popup6'
                                    height='available'
                                    directions={ ['right-center', 'right-top', 'right-bottom'] }
                                    visible={ this.state.popup6 }
                                    onMouseLeave={ () => { this.setState({ popup6: false }); } }
                                    onMouseEnter={ () => { this.setState({ popup6: true }); } }
                                >
                                    Popup with available height
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                    <DemoSection theme='alfa-on-color'>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <Button
                                    ref='target7'
                                    size='m'
                                    onClick={ () => { this.setState({ popup7: !this.state.popup7 }); } }
                                >
                                    Click me
                                </Button>
                                <Popup
                                    ref='popup7'
                                    autoclosable={ true }
                                    visible={ this.state.popup7 }
                                    onClickOutside={ () => { this.setState({ popup7: false }); } }
                                >
                                    Popup with autoclos able="true"
                                </Popup>
                            </div>
                        </ThemeProvider>
                    </DemoSection>
                </div>
                <PopupContainerProvider className={ cn('aside') }>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <Paragraph>
                                { LOREM_IPSUM.slice(0, 3) }
                            </Paragraph>
                            <DemoSection theme='alfa-on-white'>
                                <ThemeProvider theme='alfa-on-color'>
                                    <div>
                                        <Button
                                            ref='target8'
                                            size='s'
                                            onClick={ () => { this.setState({ popup8: !this.state.popup8 }); } }
                                        >
                                            Click me
                                        </Button>
                                        <Popup
                                            directions={ ['bottom-left'] }
                                            ref='popup8'
                                            size='s'
                                            type='tooltip'
                                            visible={ this.state.popup8 }
                                        >
                                            Popup with two lines of text in
                                            custom scrollable fixed container renders just fine
                                        </Popup>
                                    </div>
                                </ThemeProvider>
                            </DemoSection>
                            <Paragraph>
                                { LOREM_IPSUM.slice(0, 3) }
                            </Paragraph>
                        </div>
                    </ThemeProvider>
                </PopupContainerProvider>
            </div>
        );
    }
}

export default Demo;
