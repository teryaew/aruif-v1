import { autobind } from 'core-decorators';

import FeatherComponent from '../../src/feather/feather';
import DemoSection from '../demo-section';
import InputAutocomplete from '../../src/input-autocomplete/input-autocomplete';
import Label from '../../src/label/label';
import ThemeProvider from '../../src/theme-provider/theme-provider';

import cn from '../../src/cn';

require('./demo.css');

const OPTIONS_1 = [
    {
        value: 1,
        text: 'Facebook'
    },
    {
        value: 2,
        text: 'Twitter'
    },
    {
        value: 3,
        text: 'LinkedIn'
    },
    {
        value: 4,
        text: 'Sina Weibo'
    },
    {
        value: 5,
        text: 'Pinterest'
    },
    {
        value: 6,
        text: 'VKontakte'
    },
    {
        value: 7,
        text: 'Instagram'
    },
    {
        value: 8,
        text: 'Tumblr'
    },
    {
        value: 9,
        text: 'Flickr'
    },
    {
        value: 10,
        text: 'Odnoklassniki'
    },
    {
        value: 11,
        text: 'Renren'
    },
    {
        value: 12,
        text: 'douban'
    },
    {
        value: 13,
        text: 'LiveJournal'
    },
    {
        value: 14,
        text: 'DeviantArt'
    },
    {
        value: 15,
        text: 'StumbleUpon'
    },
    {
        value: 16,
        text: 'Myspace'
    },
    {
        value: 17,
        text: 'Yelp, Inc.'
    },
    {
        value: 18,
        text: 'Taringa!'
    },
    {
        value: 19,
        text: 'mixi'
    },
    {
        value: 20,
        text: 'XING'
    }
];

const OPTIONS_2 = [
    {
        value: 1,
        text: 'VKontakte',
        description: <Label size='l'>вариант - VKontakte</Label>
    },
    {
        value: 2,
        text: 'Facebook',
        description: <Label size='l'>вариант - Facebook</Label>
    },
    {
        value: 3,
        text: 'Twitter',
        description: <Label size='l'>вариант - Twitter</Label>
    }
];

@cn('demo')
class Demo extends FeatherComponent {
    state = {
        value: '',
        valueWithDescription: ''
    };

    render(cn) {
        return (
            <div className={ cn }>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div className={ cn('layout') }>
                                <InputAutocomplete
                                    size='m'
                                    value={ this.state.value }
                                    onChange={ this.handleChange }
                                    onItemSelect={ this.handleItemSelect }
                                    placeholder='Input...'
                                    options={ this.getFilteredOptions(OPTIONS_1, this.state.value) }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
                <DemoSection theme='alfa-on-color'>
                    <ThemeProvider theme='alfa-on-white'>
                        <div>
                            <div className={ cn('layout') }>
                                <InputAutocomplete
                                    size='l'
                                    value={ this.state.valueWithDescription }
                                    onChange={ this.handleChangeValueWithDescription }
                                    onItemSelect={ this.handleItemSelectValueWithDescription }
                                    placeholder='Input...'
                                    options={ this.getFilteredOptions(OPTIONS_2, this.state.valueWithDescription) }
                                />
                            </div>
                        </div>
                    </ThemeProvider>
                </DemoSection>
            </div>
        );
    }

    @autobind
    handleChange(value) {
        this.setState({
            value
        });
    }

    @autobind
    handleChangeValueWithDescription(value) {
        this.setState({
            valueWithDescription: value
        });
    }

    @autobind
    handleItemSelect(item) {
        this.setState({
            value: item.text
        });
    }

    @autobind
    handleItemSelectValueWithDescription(item) {
        this.setState({
            valueWithDescription: item.text
        });
    }

    getFilteredOptions(optionsList, value) {
        let filteredOptions = [];

        if (!value) {
            return filteredOptions;
        }

        optionsList.forEach((option) => {
            if (option.type === 'group') {
                let groupOptions = this.getFilteredOptions(option.content, value);

                if (groupOptions.length) {
                    filteredOptions.push(
                        Object.assign(
                            {},
                            option,
                            {
                                content: groupOptions
                            }
                        )
                    );
                }
            } else if (option.text.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
                filteredOptions.push(option);
            }
        });

        return filteredOptions;
    }
}

export default Demo;
