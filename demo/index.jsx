import querystring from 'querystring';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import RedBox from 'redbox-react';

import Demo from './demo';
import I18n from '../src/i18n/i18n';

const IS_PRODUCTION = process.env.NODE_ENV === 'production';
const HOT_LOADER = !!process.HOT_LOADER;

const locale = querystring.parse(window.parent.location.search).locale;
const template = (
    <I18n locale={ locale }>
        <Demo />
    </I18n>
);

// alert(locale);

if (!IS_PRODUCTION && HOT_LOADER) {
    ReactDOM.render(
        <AppContainer errorReporter={ RedBox }>
            { template }
        </AppContainer>,
        document.getElementById('react-app')
    );

    if (module.hot) {
        module.hot.accept('./demo', () => {
            ReactDOM.render(
                <AppContainer errorReporter={ RedBox }>
                    { template }
                </AppContainer>,
                document.getElementById('react-app')
            );
        });
    }
} else {
    ReactDOM.render(template, document.getElementById('react-app'));
}
