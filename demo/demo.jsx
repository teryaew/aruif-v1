import querystring from 'querystring';

import FeatherComponent from '../src/feather/feather';
import Heading from '../src/heading/heading';
import Label from '../src/label/label';
import Select from '../src/select/select';
import ThemeProvider from '../src/theme-provider/theme-provider';

import cn from '../src/cn';
require('./demo.css');

@cn('demo')
class Demo extends FeatherComponent {
    state = {
        component: this.getQueryStringObject().component,
        componentsList: process.COMPONENTS || [],
        locale: this.getQueryStringObject().locale || 'en'
    };

    render(cn) {
        return (
            <div className={ cn }>
                <div className={ cn('layout') }>
                    {
                        this.state.component &&
                        <iframe
                            className={ cn('iframe') }
                            src={ `${this.state.component}/index.html` }
                            width='100%'
                            height='100%'
                            frameBorder='0'
                        />
                    }
                </div>
                <div className={ cn('menu') }>
                    <div className={ cn('menu-desktop') }>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <Heading size='l'>ARUI Feather</Heading>
                                <div className={ cn('menu-desktop-item') }>
                                    { this.renderLocaleSelect() }
                                </div>
                                <ul className={ cn('menu-items') }>
                                    { this.state.componentsList.map(component =>
                                        <li
                                            key={ component }
                                            className={ cn('menu-item') }
                                            onClick={ () => {
                                                this.setState({ component });
                                            } }
                                        >
                                            <a
                                                href={ '?' + querystring.stringify(
                                                    { component, locale: this.state.locale }
                                                ) }
                                            >
                                                { component }
                                            </a>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </ThemeProvider>
                    </div>
                    <div className={ cn('menu-mobile') }>
                        <ThemeProvider theme='alfa-on-white'>
                            <div>
                                <div className={ cn('menu-mobile-item') }>
                                    <Label size='m'>ARUI Feather</Label>
                                </div>
                                <div className={ cn('menu-mobile-item') }>
                                    <Select
                                        size='s'
                                        mode='radio-check'
                                        placeholder='Выберите компонент'
                                        checkedItems={ [this.state.component] }
                                        options={
                                            this.state.componentsList.map(component =>
                                                ({
                                                    value: component,
                                                    text: component
                                                })
                                            )
                                        }
                                        onOptionCheck={ (items) => {
                                            this.setState({ component: items[0] });
                                            window.location.href = '?' + querystring.stringify(
                                                { component: items[0], locale: this.state.locale }
                                            );
                                        } }
                                    />
                                </div>
                                <div className={ cn('menu-mobile-item') }>
                                    { this.renderLocaleSelect() }
                                </div>
                            </div>
                        </ThemeProvider>
                    </div>
                </div>
            </div>
        );
    }

    renderLocaleSelect() {
        return (
            <Select
                size='s'
                mode='radio-check'
                placeholder='Выберите язык'
                checkedItems={ [this.state.locale] }
                options={
                    [
                        {
                            text: 'English',
                            value: 'en'
                        },
                        {
                            text: 'Русский',
                            value: 'ru'
                        }
                    ].map(item =>
                        ({
                            value: item.value,
                            text: item.text
                        })
                    )
                }
                onOptionCheck={ (items) => {
                    this.setState({ locale: items[0] });
                    window.location.href = '?' + querystring.stringify(
                        { component: this.state.component, locale: items[0] }
                    );
                } }
            />
        );
    }

    getQueryStringObject() {
        return querystring.parse(window.location.search.replace('?', ''));
    }
}

export default Demo;
